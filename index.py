import dash
from flask import Flask
# from flask_caching import Cache

# Objeto do server em Flask
server = Flask(__name__)

# dash_app é o objeto Dash setado
dash_app = dash.Dash(
    # Arquivo principal
    __name__,

    # Server Flask
    server=server,

    # Suprime as exceções dos callbacks
    suppress_callback_exceptions=True, 
    
    # Comando que torna a estrutura geral resposiva
    meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}],
    
    # Frase que aparece quando a página está atualizando
    update_title="Cassy está prevendo..."
)

# Favicon que está na pasta assets, o próprio Dash reconhece a pasta
dash_app._favicon = "logo-trinus_triangulo.png"

# Título que aparece na aba do navegador
dash_app.title = "Cassy"

# Esse trecho seria para criar o cache usando o sistema de arquivos do SO ou o Redis
# cache = Cache(
#     dash_app.server,
#     config={
#         'CACHE_TYPE': 'filesystem',
#         'CACHE_DIR': 'cache'
#     }
# )