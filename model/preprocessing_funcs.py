import pandas as pd

# Lista com todoas as colunas
n_cols = ['VENDAS ACUMULADAS TOTAIS', 'CANCELAMENTO TOTAL', 'VGV LÍQUIDO ACUMULADO', '(+) RECEITA BRUTA OPERACIONAL',
           '(=) CUSTOS OPERACIONAIS', '(=) DESPESAS OPERACIONAIS', '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', 'FLUXO DE CAIXA FII']

# Lista somente com as colunas que precisam ser acumuladas
var_cols = ['CANCELAMENTO TOTAL', '(+) RECEITA BRUTA OPERACIONAL', '(=) CUSTOS OPERACIONAIS', '(=) DESPESAS OPERACIONAIS',
    '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', 'FLUXO DE CAIXA FII']

def extrai_data(lista, emp):
    """Função que pega o mês e o ano data data de referência para gerar um tipo de chave, por exemplo: 2101 Solange.

    Args:
        lista (list): lista com as datas de referência dos PxR de um empreendimento
        emp (string): variável com o nome do empreendimento

    Returns:
        aux (list): retorna uma lista com as chaves de cada PxR de um empreendimento, no formato: 2101 Solange, 2102 Solange...
    """

    aux = []
    for d in lista:
        d = str(d).split('T')[0].split('-')
        d1 = d[0][2:] + d[1] + ' ' + emp
        aux.append(d1)

    return sorted(aux)

def separa_previsto_realizado(empr, empreendimentos):
    """Função que separa os PxR previstos do realizado.

    Args:
        empr (dict): dicionário com todos os empreendimentos
        empreendimentos (list): lista com os nomes de todos empreendimentos

    Returns:
        dict_empr (dict): retorna um novo dicionário com os PxR separados, o dicionário possui o formato:
            dict = {
                'nome do empreendimento': {
                    'realizado': DataFrame do último PxR,
                    'previstos': {
                        '2101 Solange': DataFrame do PxR 01/01/2021,
                        '2102 Solange': DataFrame do PxR 01/02/2021,
                        ...
                    }
                }
            }
    """

    dict_empr = {}

    # Cria o dicionário para os empreendimentos
    for empreendimento in empreendimentos:
        dict_empr[empreendimento] = {
            'realizado': None,
            'previstos': {}
        }

    # Separando em previsto e realizado
    for empreendimento in empreendimentos:
        # Lista das chaves dos PxR, por exemplo: 2101 Solange, 2102 Solange...
        lista_arq = list(empr[empreendimento].keys())

        # Colocando o último PxR na chave do realizado
        dict_empr[empreendimento]['realizado'] = empr[empreendimento][lista_arq[-1]]
        
        # Colocando os demais PxR nas chaves do previsto
        for i in range(len(lista_arq)-1):
            dict_empr[empreendimento]['previstos'][lista_arq[i]] = empr[empreendimento][lista_arq[i]]

    return dict_empr

def acumulando_dados(empr, lista_emp):
    """Função que acumula as variáveis.

    Args:
        empr (dict): dicionário com os PxR de cada empreendimento
        lista_emp (list): lista dos empreendimentos que possuem a quantidade mínima de PxR para as análises

    Returns:
        empr_aux (dict): retorna um novo dicionário com as variáveis acumuladas
    """

    empr_aux = empr
    
    # Percorre a lista de empreendimentos
    for empreendimento in lista_emp:
        # Para cada empreendimento retorna uma lista com o nome do PXR
        lista_pxr = list(empr[empreendimento])

        # Pega cada PXR de um empreendimento
        for i in range(len(lista_pxr)):
            # Armazena o PXR analisado
            pxr = empr[empreendimento][lista_pxr[i]]

            # Acumulando as variáveis
            pxr['VENDAS ACUMULADAS TOTAIS'] = pxr['VENDAS ACUMULADAS TOTAIS'].cumsum()
            pxr['CANCELAMENTO TOTAL'] = pxr['CANCELAMENTO TOTAL'].cumsum()
            pxr['VGV LÍQUIDO ACUMULADO'] = pxr['VGV LÍQUIDO ACUMULADO'].cumsum()
            pxr['(+) RECEITA BRUTA OPERACIONAL'] = pxr['(+) RECEITA BRUTA OPERACIONAL'].cumsum()
            pxr['(=) CUSTOS OPERACIONAIS'] = pxr['(=) CUSTOS OPERACIONAIS'].cumsum()
            pxr['(=) DESPESAS OPERACIONAIS'] = pxr['(=) DESPESAS OPERACIONAIS'].cumsum()
            pxr['(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO'] = pxr['(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO'].cumsum()
            pxr['FLUXO DE CAIXA FII'] = pxr['FLUXO DE CAIXA FII'].cumsum()

            # Retorna o PXR com variáveis acumuladas
            empr_aux[empreendimento][lista_pxr[i]] = pxr

    return empr_aux

def separa_pxr_acumulado(empr, empr_aux, lista_emp):
    """Função que separa os PxR acumulados previstos do realizado.

    Args:
        empr (dict): dicionário com todos os empreendimentos
        empr_aux (dict): dicionário com os PxR
        lista_emp (list): lista com os nomes de todos empreendimentos

    Returns:
        dict_empr2 (dict): retorna um novo dicionário com os PxR separados, o dicionário possui o formato:
            dict = {
                'nome do empreendimento': {
                    'realizado': DataFrame do último PxR,
                    'previstos': {
                        '2101 Solange': DataFrame do PxR 01/01/2021,
                        '2102 Solange': DataFrame do PxR 01/02/2021,
                        ...
                    }
                }
            }
    """

    dict_empr2 = {}

    # Cria o dicionário para os empreendimentos
    for empreendimento in lista_emp:
        dict_empr2[empreendimento] = {
            'realizado': None,
            'previstos': {}
        }

    # Separando em Previsto e realizado
    for empreendimento in lista_emp:
        # Lista das chaves dos PxR, por exemplo: 2101 Solange, 2102 Solange...
        lista_arq = list(empr[empreendimento].keys())

        # Colocando o último PxR na chave do realizado
        dict_empr2[empreendimento]['realizado'] = empr_aux[empreendimento][lista_arq[-1]]
        
        # Colocando os demais PxR nas chaves do previsto
        for i in range(len(lista_arq)-1):
            dict_empr2[empreendimento]['previstos'][lista_arq[i]] = empr_aux[empreendimento][lista_arq[i]]

    return dict_empr2

def indicadores(pxr):
    """Função que criar os indicadores de meses, por exemplo: M1, M2,..., M_n.

    Args:
        pxr (DataFrame): DataFrame de um PxR

    Returns:
        pxr (DataFrame): retorna o mesmo PxR, mas com a coluna dos indicadores de meses
    """

    # Gera uma lista com os indicadores
    ind = ['M' + f'{i+1}' for i in range(pxr.shape[0])]
    
    # Insere a lista como a primeira coluna do DataFrame
    pxr.insert(0, "INDICADORES", ind, True)
    
    return pxr

def filtragem(pxr, nome_atual_pxr, nome_ultimo_pxr, op):
    """Função que faz a filtragem dos PxR com base nas datas.

    Args:
        pxr (DataFrame): DataFrame de um PxR
        nome_atual_pxr (string): chave do PxR em questão, por exemeplo: 2101 Solange
        nome_ultimo_pxr (string): chave do PxR realizado, por exemplo: 2201 Solange
        op (int): flag que sinaliza se o PxR é o realizado ou previsto

    Returns:
        pxr (DataFrame): retorna o DataFrame filtrado
    """

    mes_atual = int(nome_atual_pxr.split()[0][2:])
    ano_atual = int(nome_atual_pxr.split()[0][:2])
    mes_ultimo = int(nome_ultimo_pxr.split()[0][2:])
    ano_ultimo = int(nome_ultimo_pxr.split()[0][:2])

    if mes_atual < 10:
        data_atual = f"20{ano_atual}-0{mes_atual}-01"
    else:
        data_atual = f"20{ano_atual}-{mes_atual}-01"
    
    if mes_ultimo < 10:
        data_ultima = f"20{ano_ultimo}-0{mes_ultimo}-01"
    else:
        data_ultima = f"20{ano_ultimo}-{mes_ultimo}-01"

    if op == 1:
        filtro_mes = (pxr.DATA >= data_atual) & (pxr.DATA < data_ultima)
    else:
        filtro_mes = (pxr.DATA >= data_atual)

    # Faz o filtro com base nos meses
    pxr = pxr[filtro_mes]

    # Chama a função para criar os indicadores de meses
    pxr = indicadores(pxr)
    
    # Reseta a coluna de índice
    pxr.reset_index(inplace=True, drop=True)

    return pxr

def filtragem_realizado(pxr, nome_atual_pxr, nome_ultimo_pxr, op):
    """Função que faz a filtragem do realizado com base nas datas.

    Args:
        pxr (DataFrame): DataFrame de um PxR
        nome_atual_pxr (string): chave do PxR em questão, por exemeplo: 2101 Solange
        nome_ultimo_pxr (string): chave do PxR realizado, por exemplo: 2201 Solange
        op (int): flag que sinaliza se o PxR é o realizado ou previsto

    Returns:
        pxr (DataFrame): retorna o DataFrame filtrado
    """

    mes_atual = int(nome_atual_pxr.split()[0][2:])
    ano_atual = int(nome_atual_pxr.split()[0][:2])
    mes_ultimo = int(nome_ultimo_pxr.split()[0][2:])
    ano_ultimo = int(nome_ultimo_pxr.split()[0][:2])

    if mes_atual < 10:
        data_atual = f"20{ano_atual}-0{mes_atual}-01"
    else:
        data_atual = f"20{ano_atual}-{mes_atual}-01"

    if mes_ultimo < 10:
        data_ultima = f"20{ano_ultimo}-0{mes_ultimo}-01"
    else:
        data_ultima = f"20{ano_ultimo}-{mes_ultimo}-01"
    
    if op == 1:
        filtro_mes = (pxr.DATA >= data_atual) & (pxr.DATA < data_ultima)
        pxr = pxr[filtro_mes]
        pxr = indicadores(pxr)
        pxr.reset_index(inplace=True, drop=True)
    else:
        pass

    return pxr

def preparando_dados(lista_emp, empr, dict_empr, dict_filtrado, op):
    """Função que prepara os dados para filtragem.

    Args:
        lista_emp (list): lista com os nomes de todos empreendimentos com o número mínimo de PxR para análises
        empr (dict): dicionário com todos os PxR's
        dict_empr (dict): dicionário com os PxR's separados
        dict_filtrado (dict): dicionário que irá armazenar os dados filtrados
        op (int): flag que indica se o PxR é previsto ou realizado

    Returns:
        dict_filtrado (dict): retorna um dicionário com todos os PxR's de cada empreendimento filtrado
    """
    # Colocando todos os PXR em uma lista
    for emp in lista_emp:

        # Criando os indicadores M's para o realizado de cada empreendimento
        lista_arq = list(empr[emp].keys())
        a = lista_arq[-1][:2]
        ano = f"20{a}"
        data = f"{int(ano)}-12-01"

        for i in range(len(lista_arq)-1):
            prev = dict_empr[emp]['previstos'][lista_arq[i]]
            prev = prev[(prev.DATA >= '2021-01-01') & (prev.DATA <= data)]
            prev = filtragem(prev, lista_arq[i], lista_arq[-1], op)
            dict_filtrado[emp]['previstos'][lista_arq[i]] = prev
        
        real = dict_empr[emp]['realizado']
        real = real[(real.DATA >= '2021-01-01') & (real.DATA <= data)]
        if op == 1:
            real = filtragem_realizado(real, lista_arq[0], lista_arq[-1], op)
        dict_filtrado[emp]['realizado'] = real.reset_index(drop=True)

    return dict_filtrado

def indicadores_realizado(pxr):
    """Função que cria os indicadores para o realizado

    Args:
        pxr (DataFrame): DataFrame do realizado

    Returns:
        pxr (DataFrame): retorna o DataFrame do realizado com a coluna de indicadores de meses
    """

    ind = ['M' + f'{i+1}' for i in range(pxr.shape[0])]
    
    pxr.insert(0, "INDICADORES", ind, True)
    
    return pxr

def preparando_dados_realizado(lista_emp, dict_empr, aux):
    """Função que prepara os dados do realizado para chamar os indicadores de meses.

    Args:
        lista_emp (list): lista com os nomes dos empreendimentos
        dict_empr (dict): dicionário com os PxR's separados
        aux (dict): dicionário auxiliar

    Returns:
        aux (dict): retorna o dicionário auxiliar
    """
    # Colocando todos os PXR em uma lista
    for emp in lista_emp:            
        real = dict_empr[emp]['realizado']
        real = indicadores_realizado(real)
        aux[emp]['realizado'] = real.reset_index(drop=True)
        
    return aux

def calcula_erro(dicionario_emp, emp):
    """Função que calcula a taxa de erro,  e = (P - R) / R.

    Args:
        dicionario_emp (dict): dicionário que com os PxR filtrados e separados.
        emp (string): nome do empreendimento

    Returns:
        dicionario (dict): returna um novo dicionário com a taxa de erro calculada
    """
    # Pega uma lista com chaves dos previstos, por exemplo: 2101 Solange, 2102 Solange...
    lista_pxr = list(dicionario_emp['previstos'].keys())

    # Cria o dicionário que vai guardar os dados calculados
    dicionario = {'realizado': None, 'previstos': {}}

    # Armazena o realizado
    dicionario['realizado'] = dicionario_emp['realizado']
    
    # Pega cada PxR de um empreendimento
    for pxr in lista_pxr:
        # Extraindo o último número do arquivo PXR (Por exemplo: 2101, extrai o último número)
        j = int(pxr.split()[0][2:])

        if emp in ['Golden Laghetto', 'Max Serra Dourada', 'Residencial Braviello', 'Vitta Novo Mundo']:
            k = pxr.split()[0]

            if k == '2104':
                d = dicionario_emp['previstos'][pxr]

                lista = list()

                for ind in d.index.tolist():
                    if int(ind) % 2 == 0:
                        lista.append(ind)

                dicionario_emp['previstos'][pxr] = d.loc[lista].reset_index(drop=True)
        
        if j < 10:
            data = f"2021-0{j}-01"
        else:
            data = f"2021-{j}-01"

        # Cria um dicionário temporário
        df = {}
        df['INDICADORES'] = dicionario_emp['previstos'][pxr]['INDICADORES']
        df['ID_GUARDADOS'] = dicionario_emp['previstos'][pxr]['ID_GUARDADOS']
        df['EMPREENDIMENTO'] = dicionario_emp['previstos'][pxr]['EMPREENDIMENTO']
        df['NATUREZA'] = dicionario_emp['previstos'][pxr]['NATUREZA']
        df['DATA_REFERENCIA'] = dicionario_emp['previstos'][pxr]['DATA_REFERENCIA']
        df['DATA'] = dicionario_emp['previstos'][pxr]['DATA']

        for coluna in n_cols:
            df[coluna] = []

        # Faz um filtro do range de data do realizado
        realizado = dicionario_emp['realizado'][dicionario_emp['realizado'].DATA >= data].reset_index(drop=True)

        # Percorre por cada coluna do PxR
        for coluna in n_cols:
            # Pega uma lista com os valores realizados
            r = list(realizado[coluna].values)

            # Pega uma lista com os valores previstos
            p = list(dicionario_emp['previstos'][pxr][coluna].values)

            # Percorre as listas
            for i in range(dicionario_emp['previstos'][pxr].shape[0]):
                # Calcula a taxa de erro
                if int(r[i]) != 0:
                    result = (p[i] - r[i]) / r[i]
                else:
                    result = 0
                        
                df[coluna].append(result)

        dicionario['previstos'][pxr] = pd.DataFrame(df)

    return dicionario