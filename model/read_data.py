import time
import datetime as dt
import pandas as pd
import pyodbc
from model.preprocessing_funcs import *
#from index import cache
import warnings
warnings.filterwarnings("ignore")

#start_time = time.time()

n_cols = ['VENDAS ACUMULADAS TOTAIS', 'CANCELAMENTO TOTAL', 'VGV LÍQUIDO ACUMULADO', '(+) RECEITA BRUTA OPERACIONAL',
    '(=) CUSTOS OPERACIONAIS', '(=) DESPESAS OPERACIONAIS', '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', 'FLUXO DE CAIXA FII']

# Faz a conexão com DataBricks
#print("Fazendo conexão com DataBricks...")
conn = pyodbc.connect("DSN=DatabricksTrinus", autocommit=True)

# Fazer a consulta da tabela de PxR
#print("Puxando os dados do DataBricks...")
#df = pd.read_sql(f"SELECT * FROM pxr_v1.indicadores_d2_cassy", conn)

#@cache.memoize(timeout=20)
def load_data():
    #return pd.read_excel("D:\\Cassy\\model\\indicadores_d2.xlsx")
    return pd.read_sql(f"SELECT * FROM pxr_v1.indicadores_d2_cassy", conn)

df = load_data()

# Renomear as colunas da tabela
#print("Renomeando as colunas")
df.rename(
    columns={
        'id_guardados': 'ID_GUARDADOS',
        'empreendimento': 'EMPREENDIMENTO',
        'natureza': 'NATUREZA',
        'data_referencia': 'DATA_REFERENCIA',
        'data': 'DATA',
        'vendas_acumuladas_totais': 'VENDAS ACUMULADAS TOTAIS',
        'cancelamento_total': 'CANCELAMENTO TOTAL',
        'vgv_liquido_acumulado': 'VGV LÍQUIDO ACUMULADO',
        'mais_receita_bruta_operacional': '(+) RECEITA BRUTA OPERACIONAL',
        'igual_custos_operacionais': '(=) CUSTOS OPERACIONAIS',
        'igual_despesas_operacionais': '(=) DESPESAS OPERACIONAIS',
        'igual_fluxo_de_caixa_operacional_ajustado': '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO',
        'fluxo_de_caixa_fii': 'FLUXO DE CAIXA FII'
    },
    inplace=True
)

# Ordenando por empreendimento, data de referência e data
df.sort_values(by=['EMPREENDIMENTO', 'DATA_REFERENCIA', 'DATA'], ascending=True, inplace=True)
df.reset_index(drop=True, inplace=True)

# Pegando a lista de todos os empreendimentos
lista_de_empreendimentos = sorted(df.EMPREENDIMENTO.unique().tolist())

# Cria os dicionários dos dados
empr, empr_realizado = {}, {}
for emp in lista_de_empreendimentos:
    empr[emp] = {}
    empr_realizado[emp] = {}

# Faz o separação dos dados por empreendimento
for emp in lista_de_empreendimentos:
    # Filtra por empreenidmento
    aux_emp = df[df.EMPREENDIMENTO == emp]

    # Pega a data de referência
    data = sorted(aux_emp.DATA_REFERENCIA.unique())

    # Extrai mês e ano, vai ficar: 2101 Solange, 2102 Solange...
    data2 = extrai_data(data, emp)

    for i in range(len(data)):
        aux = aux_emp[aux_emp.DATA_REFERENCIA == data[i]].reset_index(drop=True)

        empr[emp][data2[i]] = aux
        empr_realizado[emp][data2[i]] = aux

#print("Pegando os empreendimentos da tabela...")
dict_empr = separa_previsto_realizado(empr, lista_de_empreendimentos)
dict_empr_realizado = separa_previsto_realizado(empr_realizado, lista_de_empreendimentos)

lista_emp = []
for empreendimento in lista_de_empreendimentos:
    if len(empr[empreendimento]) >= 4:
        lista_emp.append(empreendimento)

tipologias = {}
for emp in lista_emp:
    t = dict_empr[emp]['realizado']['NATUREZA'].unique()[0]
    tipologias[emp] = t

#print("Acumulando as variáveis...")
empr_aux = acumulando_dados(empr, lista_emp)
empr_aux_realizado = acumulando_dados(empr_realizado, lista_emp)

# Sepera os previstos do realizado acumulado
dict_empr2 = separa_pxr_acumulado(empr, empr_aux, lista_emp)
dict_empr3 = separa_pxr_acumulado(empr_realizado, empr_aux_realizado, lista_emp)

# Cria os dicionários para preparar os dados
dict_tratado = {}
for empreendimento in lista_emp:
    dict_tratado[empreendimento] = {
        'realizado': None,
        'previstos': {}
    }

dict_tratado_aux = {}
for empreendimento in lista_emp:
    dict_tratado_aux[empreendimento] = {
       'realizado': None,
       'previstos': {}
    }

dict_tratado_realizado = {}
for empreendimento in lista_emp:
    dict_tratado_realizado[empreendimento] = {
        'realizado': None,
        'previstos': {}
    }

dict_tratado_aux_realizado = {}
for empreendimento in lista_emp:
    dict_tratado_aux_realizado[empreendimento] = {
        'realizado': None,
        'previstos': {}
    }

#print("Preparando os dados...")
# Prepara os dados para fazer as filtragens
dict_tratado = preparando_dados(lista_emp, empr_aux, dict_empr2, dict_tratado, 1)
dict_tratado_aux = preparando_dados(lista_emp, empr_aux, dict_empr2, dict_tratado_aux, 0)

dict_tratado_realizado = preparando_dados(lista_emp, empr_aux_realizado, dict_empr3, dict_tratado_realizado, 1)
dict_tratado_aux_realizado = preparando_dados_realizado(lista_emp, dict_empr3, dict_tratado_aux_realizado)

# Cria os dicionários salvando os realizados
pxr_realizados = {}
for emp in lista_emp:
    pxr_realizados[emp] = dict_tratado[emp]['realizado'].reset_index(drop=True)

pxr_realizados_aux = {}
for emp in lista_emp:
    pxr_realizados_aux[emp] = dict_tratado_aux[emp]['realizado'].reset_index(drop=True)

pxr_realizados2 = {}
for emp in lista_emp:
    pxr_realizados2[emp] = dict_tratado_realizado[emp]['realizado'].reset_index(drop=True)

pxr_realizados_extend = {}
for emp in lista_emp:
    pxr_realizados_extend[emp] = dict_tratado_aux_realizado[emp]['realizado'].reset_index(drop=True)

for emp in lista_emp:
    pxr_realizados_aux[emp] = indicadores(pxr_realizados_aux[emp])

#print("Calculando a taxa de erro...")
# Calcula os erros
erro_pxr = {}
for emp in lista_emp:
    dict = calcula_erro(dict_tratado[emp], emp)
    erro_pxr[emp] = dict

erro_pxr_aux = {}
for emp in lista_emp:
    dict = calcula_erro(dict_tratado_aux[emp], emp)
    erro_pxr_aux[emp] = dict

def query_dfs():
    """Função que empilha os erros dos empreendimentos

    Returns:
        dfs (DataFrame): retorna o DataFrame empilhado com todos os empreenidmentos
    """
    
    # Pegando o primeiro PXR para empilhar
    dfs = erro_pxr['Solange Bairro Planejado']['previstos']['2102 Solange Bairro Planejado'] 

    for emp in lista_emp:
        if emp == 'Solange Bairro Planejado':
            lista = list(erro_pxr[emp]['previstos'].keys())
            for i in range(1, len(lista)):
                dfs = pd.concat([dfs, erro_pxr[emp]['previstos'][lista[i]]], axis=0)
        else:
            for pxr in list(erro_pxr[emp]['previstos'].keys()):
                dfs = pd.concat([dfs, erro_pxr[emp]['previstos'][pxr]], axis=0)
    dfs = dfs.reset_index(drop=True)

    return dfs

_ = query_dfs()

#end_time = time.time()

#seconds = end_time - start_time

#print()
#print(f"Tempo de execução: {dt.timedelta(seconds=seconds)}")
