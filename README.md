# Análise Estatística PXR #

Projeto sobre avaliação de capacidade de previsão financeira (PxR) e assertividade de previsibilidade versus mês de previsão.

### Análises Gráficas ###

* Previsto vs Realizado: esta análise tem como finalidade apresentar o previsto contra o realizado sobre cada mês previsto, por exemplo, todos os M'i;
* Erro percentual: esta análise tem como objetivo mostrar o erro percentual calculado entre o que foi previsto e o que foi realizado ao longo dos meses M'i:
* Análise do realizado com bandas de erro: esta análise mostra o realizado até o mês de referência, além de projetar bandas de erro;
* Clusterização: esta análise tem como objetivo clusterizar os dados de acordo com alguns fatores, tais como tipologia, cidade, estado, gerente de projeto, etc;
* Backtesting: a análise de backtest tem como objetivo olhar para os dados do passado e entender o que aconteceu e, assim, auxiliar nas estratégias futuras. Esta análise dispõe de uma faixa de erro dinâmica ao longo dos meses. O método para calcular esta faixa de erro é conhecido como bootstrap.
