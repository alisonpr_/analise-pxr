# Objeto dash_app que veio do arquivo index.py
from index import dash_app

# Imports da bilioteca Dash
import dash
from dash import dcc
from dash import html
from dash import Input, Output
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc

# Imports dos arquivos da pasta controller
from controller.grafico_realizado_bandas import opcao_grafico_bandas
from controller.graficos_backtesting import opcao_empreendimento
from controller.graficos_erro_tipologia import alerta_tipologia, opcao_grafico_erro
from controller.graficos_erro_ativos import opcao_grafico_asset, alerta_ativo
from controller.table import return_table, return_table_typololy
from controller.indicadores import indicadores_tipologia, indicadores_ativos

# Imports das constantes da pasta utils
from utils.constantes import *
from utils.constantes import alerta

# Imports dos arquivos da pasta view
from view.view_main_layout import layout
from view.view_graph_error import *
from view.view_graph_bands import *
from view.view_graph_backtest import *
from view.view_graph_backtest_ci import *
from view.view_dropdown_confidance import *
from view.view_typology import *
from view.view_graph_error_assets import *
from view.view_card_error_typology import *
from view.view_card_error_assets import *
from view.view_table_error import *
from view.view_home_page import *

# Import da biblioteca time
import time

# Import da biblioteca warnings que esconde qualquer aviso ou alerta
import warnings
warnings.filterwarnings("ignore")

# Aplicando o layout criado em view.view_main_layout
dash_app.layout = layout

# Não lembro pra que isso
app = dash_app.server

###################################### CALLBACKS DOS LOADINGS (SPINNER) ######################################

############################# Os Spinners de página aparecem ao trocar de página #############################

# Spinner da home page
@dash_app.callback(
    Output('loading-output', 'children'),
    [Input('url', 'pathname')]
)
def spinner_home_page(pathname):
    """Função de loading da home page.

    Args:
        pathname (object): pathname é o nome da página, o input é obrigatório em callbacks

    Returns:
        none: a função não irá retornar nada, ela apenas receber o nome da página para gerar o loading
    """

    time.sleep(2)
    return None

# Spinner da página de erros por tipologia
@dash_app.callback(
    Output('loading-output-1', 'children'),
    [Input('url', 'pathname')]
)
def spinner_page_typology(pathname):
    """Função de loading da página de erros por tipologia.

    Args:
        pathname (object): pathname é o nome da página, o input é obrigatório em callbacks

    Returns:
        none: a função não irá retornar nada, ela apenas receber o nome da página para gerar o loading
    """

    time.sleep(2)
    return None

# Spinner da página de erros por ativo
@dash_app.callback(
    Output('loading-output-2', 'children'),
    [Input('url', 'pathname')]
)
def spinner_page_asset(pathname):
    """Função de loading da página de erros por ativos.

    Args:
        pathname (object): pathname é o nome da página, o input é obrigatório em callbacks

    Returns:
        none: a função não irá retornar nada, ela apenas receber o nome da página para gerar o loading
    """

    time.sleep(2)
    return None

# Spinner da página de backtesting
@dash_app.callback(
    Output('loading-output-3', 'children'),
    [Input('url', 'pathname')]
)
def spinner_page_backtesting(pathname):
    """Função de loading da página de backtesting.

    Args:
        pathname (object): pathname é o nome da página, o input é obrigatório em callbacks

    Returns:
        none: a função não irá retornar nada, ela apenas receber o nome da página para gerar o loading
    """

    time.sleep(2)
    return None

# Spinner da página do realizado com bandas
@dash_app.callback(
    Output('loading-output-4', 'children'),
    [Input('url', 'pathname')]
)
def spinner_page_realizado(pathname):
    """Função de loading da página do realizado com bandas.

    Args:
        pathname (object): pathname é o nome da página, o input é obrigatório em callbacks

    Returns:
        none: a função não irá retornar nada, ela apenas receber o nome da página para gerar o loading
    """

    time.sleep(2)
    return None

############################# Os Spinners das tabelas quando se troca o input de meses #############################

# Spinner da tabela de indicadores de erros por ativos
@dash_app.callback(
    Output('loading-output-table-asset', 'children'),
    [Input('input-meses-table-asset', 'value')]
)
def spinner_table_asset(value):
    """Função de loading para o input de meses da tabela de indicadores por ativos.

    Args:
        value (int): a variável value recebe o número de meses

    Returns:
        none: a função não irá retornar nada, pois ela somente vai gerar o loading assim que o input mudar
    """

    time.sleep(3)
    return None

# Spinner da tabela de indicadores de erros por tipologia
@dash_app.callback(
    Output('loading-output-table-typology', 'children'),
    [Input('input-meses-table-typology', 'value')]
)
def spinner_table_typology(value):
    """Função de loading para o input de meses da tabela de indicadores por tipologia.

    Args:
        value (int): a variável value recebe o número de meses

    Returns:
        none: a função não irá retornar nada, pois ela somente vai gerar o loading assim que o input mudar
    """

    time.sleep(2)
    return None

############################# Os Spinners dos seletores de tipologia e/ou ativos #############################

# Spinner do dropdown da página de tipologia
@dash_app.callback(
    Output('loading-output-typology', 'children'),
    [Input('tipologia', 'value')]
)
def spinner_dropdown_typology(value):
    """Função que gera o loading quando muda a tipologia para visiualizar os gráficos na página de tipologia.

    Args:
        value (string): recebe a tipologia informada

    Returns:
        none: a função não irá retornar nada, pois ela somente vai gerar o loading assim que o input mudar
    """

    time.sleep(1)
    return None

# Spinner do dropdown da página de ativos
@dash_app.callback(
    Output('loading-output-asset-2', 'children'),
    [Input('empreendimento3', 'value')]
)
def spinner_dropdown_asset(value):
    """Função que gera o loading quando muda o empreendimento para visiualizar os gráficos na página de ativos.

    Args:
        value (string): recebe o empreendimento informado

    Returns:
        none: a função não irá retornar nada, pois ela somente vai gerar o loading assim que o input mudar
    """

    time.sleep(1)
    return None

# Spinner do dropdown da página de backtesting sem intervalo de confiança
@dash_app.callback(
    Output('loading-output-asset-3', 'children'),
    [Input('empreendimento2', 'value')]
)
def spinner_dropdown_backtesting(value):
    """Função que gera o loading quando muda o empreendimento para visiualizar os gráficos na página de backtesting sem intervalo.

    Args:
        value (string): recebe o empreendimento informado

    Returns:
        none: a função não irá retornar nada, pois ela somente vai gerar o loading assim que o input mudar
    """

    time.sleep(1)
    return None

# Spinner do dropdown da página de backtesting com intervalo de confiança
@dash_app.callback(
    Output('loading-output-asset-31', 'children'),
    [Input('empreendimento4', 'value')]
)
def spinner_dropdown_backtesting_ci(value):
    """Função que gera o loading quando muda o empreendimento para visiualizar os gráficos na página de backtesting com intervalo.

    Args:
        value (string): recebe o empreendimento informado

    Returns:
        none: a função não irá retornar nada, pois ela somente vai gerar o loading assim que o input mudar
    """

    time.sleep(1)
    return None

# Spinner do input de intervalo de confiança da página de backtesting com intervalo de confiança
@dash_app.callback(
    Output('loading-output-asset-32', 'children'),
    [Input('intervalo-confianca', 'value')]
)
def spinner_interval_confidence(value):
    """Função que gera o loading quando muda o intervalo de confiança para visiualizar os gráficos na página de backtesting.

    Args:
        value (float): recebe o intervalo de confiança

    Returns:
        none: a função não irá retornar nada, pois ela somente vai gerar o loading assim que o input mudar
    """

    time.sleep(1)
    return None

# Spinner do dropdown da página do realizado com bandas
@dash_app.callback(
    Output('loading-output-asset-4', 'children'),
    [Input('empreendimento', 'value')]
)
def spinner_dropdown_realizado(value):
    """Função que gera o loading quando muda o empreendimento para visiualizar os gráficos na página do realizado com bandas.

    Args:
        value (string): recebe o empreendimento informado

    Returns:
        none: a função não irá retornar nada, pois ela somente vai gerar o loading assim que o input mudar
    """

    time.sleep(1)
    return None

# Spinner do input de meses da página do realizado com intervalo de confiança
@dash_app.callback(
    Output('loading-output-41', 'children'),
    [Input('input-data', 'value')]
)
def spinner_dropdown_realizado(value):
    """Função que gera o loading quando muda o input de meses para visiualizar os gráficos na página do realizado com bandas.

    Args:
        value (int): recebe o input de meses

    Returns:
        none: a função não irá retornar nada, pois ela somente vai gerar o loading assim que o input mudar
    """

    time.sleep(1)
    return None

###################################### ESTRUTURA DE SELEÇÃO DE PÁGINAS ######################################

@dash_app.callback(
    Output("page-content", "children"),
    [Input("url", "pathname")]
)
def render_page_content(pathname):
    """Função que renderiza e retorna páginas.

    Args:
        pathname (object): link da página, por exemplo: /errorAssets

    Returns:
        object: retorna uma Div com uma página de acordo com a estrutura de seleção
    """

    # Home page da Cassy retorna uma Div completa
    if pathname == home_page_location:
        return html.Div(
            id="page-home",
            children=[
                # Título da página
                html.H2('O que é o projeto?', style=TEXT_STYLE),

                # Spinner da página
                dcc.Loading(
                    id="loading",
                    className="loading",
                    type="cube",
                    children=html.Div(id="loading-output"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=True,
                ),

                # O html.Hr coloca uma linha como divisória
                html.Hr(className="hrBody"),
                
                # A primeira linha da página contém uma descrição do projeto que está nessa estrutura
                first_row_hp,
                
                html.Hr(className="hrBody"),
                
                # A segunda linha da página contém a tabela com a relação de PxR por empreendimentos e alguns cards
                second_row_hp,
                
                html.Hr(className="hrBody")
            ]
        )

    # Página de erros por tipologia também retorna uma Div completa
    elif pathname == error_page_location:
        return html.Div(
            id="page-erro",
            children=[
                # Título da página
                html.H2 ('Análise de erro por tipologia', style=TEXT_STYLE),

                # Spinner da página
                dcc.Loading(
                    id="loading-1",
                    className="loading",
                    type="cube",
                    children=html.Div(id="loading-output-1"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=True,
                ),

                # Spinner da tabela de indicadores
                dcc.Loading(
                    id="loading-table1",
                    className="loader",
                    children=html.Div(id="loading-output-table-typology"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=False,
                ),

                # O html.Hr coloca uma linha como divisória
                html.Hr(className="hrBody"),

                # Linha da página que contém os inputs que controlam a tabela
                tipologia_table_row,
                
                html.Hr(className="hrBody"),
                
                # Div que recebe o output de alerta quando uma tipologia sai da tabela
                html.Div(id='output-alert-table-tipologia'),
                
                # Linha da página que contém a tabela de indicadores por tipologia
                content_table_tipologia,
                
                # Spinner do input de tipologia
                dcc.Loading(
                    id="loading-typology",
                    className="loader",
                    children=html.Div(id="loading-output-typology"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=False,
                ),

                html.Hr(className="hrBody"),
                
                # Linha da página do dropdown para selecionar a tipologia
                tipologia_row,
                
                # Div que recebe o output de alerta quando um empreendimento não apresenta mais meses
                html.Div(id='output-alert-tipologia'),
                
                html.Hr(className="hrBody"),
                
                # Primeira linha que apresenta os cards com indicador de erros e desvio padrão
                card_first_row_erro,
                
                # Primeira linha de gráficos
                content_first_row_erro,
                
                html.Hr(className="hrBody"),
                
                # Segunda linha que apresenta os cards com indicador de erros e desvio padrão
                card_second_row_erro,
                
                # Segunda linha de gráficos
                content_second_row_erro,
                
                html.Hr(className="hrBody"),
                
                # Terceira linha que apresenta os cards com indicador de erros e desvio padrão
                card_third_row_erro,
                
                # Terceira linha de gráficos
                content_third_row_erro,
                
                html.Hr(className="hrBody"),
                
                # Quarta linha que apresenta os cards com indicador de erros e desvio padrão
                card_fourth_row_erro,
                
                # Quarta linha de gráficos
                content_fourth_row_erro,
                
                html.Hr(className="hrBody")
            ]
        )
    
    # Página de erros por ativos também retorna uma Div completa
    elif pathname == error_page_assets_location:
        return html.Div(
            id="page-asset",
            children=[
                # Título da página
                html.H2 ('Análise de erro por ativo', style=TEXT_STYLE),

                # Spinner da página
                dcc.Loading(
                    id="loading-2",
                    className="loading",
                    type="cube",
                    children=html.Div(id="loading-output-2"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=True,
                ),

                # Spinner da tabela
                dcc.Loading(
                    id="loading-table2",
                    className="loader",
                    children=html.Div(id="loading-output-table-asset"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=False,
                ),
                
                # O html.Hr coloca uma linha como divisória
                html.Hr(className="hrBody"),

                # Linha da página que apresenta o filtro de tipologia e o input de meses
                ativos_table_row,
                
                html.Hr(className="hrBody"),
                
                #html.Div(id='output-alert-table-ativo'),
                
                # Linha da página que apresenta a tabela de indicadores por ativos
                content_table,
                
                # Spinner da troca de empreendimentos
                dcc.Loading(
                    id="loading-asset-2",
                    className="loader",
                    children=html.Div(id="loading-output-asset-2"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=False,
                ),
                
                html.Hr(className="hrBody"),
                
                # Campo que seleciona o empreendimento e o input de meses
                dropdown_confianca_row3,
                
                # Div que retorna o output de alerta caso um empreendimento esteja fora do escopo de meses informado
                html.Div(id='output-alert-ativo'),
                
                html.Hr(className="hrBody"),
                
                # Primeira linha que apresenta os cards de indicador de erros e desvio padrão
                card_first_row_erro_asset,
                
                # Primeira linha de gráficos
                content_first_row_erro_asset,
                
                html.Hr(className="hrBody"),
                
                # Segunda linha que apresenta os cards de indicador de erros e desvio padrão
                card_second_row_erro_asset,
                
                # Segunda linha de gráficos
                content_second_row_erro_asset,
                
                html.Hr(className="hrBody"),
                
                # Terceira linha que apresenta os cards de indicador de erros e desvio padrão
                card_third_row_erro_asset,
                
                # Terceira linha de gráficos
                content_third_row_erro_asset,
                
                html.Hr(className="hrBody"),
                
                # Quarta linha que apresenta os cards de indicador de erros e desvio padrão
                card_fourth_row_erro_asset,
                
                # Quarta linha de gráficos
                content_fourth_row_erro_asset,
                
                html.Hr(className="hrBody")
            ]
        )

    # Página de backtesting também retorna uma Div completa
    elif pathname == backtesting_location:
        return html.Div(
            id="page-backtest",
            children=[
                # Título da página
                html.H2('Análise de backtesting', style=TEXT_STYLE),

                html.Hr(className="hrBody"),

                # Spinner do intervalo de confiança
                dcc.Loading(
                    id="loading-asset-3",
                    className="loader",
                    children=html.Div(id="loading-output-asset-3"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=False,
                ),

                # Spinner da troca de empreendimentos na página sem intervalo de confiança
                dcc.Loading(
                    id="loading-asset-3",
                    className="loader",
                    children=html.Div(id="loading-output-asset-31"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=False,
                ),

                # Spinner da troca de empreendimentos na página com intervalo de confiança
                dcc.Loading(
                    id="loading-asset-3",
                    className="loader",
                    children=html.Div(id="loading-output-asset-32"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=False,
                ),

                # Spinner da página
                dcc.Loading(
                    id="loading-3",
                    #className="loader",
                    type="cube",
                    children=html.Div(id="loading-output-3"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=True,
                ),

                # Cria as abas da página
                dbc.Tabs(
                    class_name="nav nav-tabs",
                    children=[
                        # Aba que apresenta os gráficos sem intervalo de confiança
                        dbc.Tab(
                            id="tab-backtest-SI",
                            
                            class_name="nav-link active",
                            
                            children=[
                                # Linha que mostra a opção de trocar o empreendimento e o botão info
                                dropdown_confianca_row2SI,
                                
                                html.Hr(className="hrBody"), 
                                
                                # Primeira linha com gráficos
                                content_first_row,
                                
                                html.Hr(className="hrBody"),
                                
                                # Segunda linha com gráficos
                                content_second_row,
                                
                                html.Hr(className="hrBody"), 
                                
                                # Terceira linha com gráficos
                                content_third_row,
                                
                                html.Hr(className="hrBody"),
                                
                                # Quarta linha com gráficos
                                content_fourth_row,
                                
                                html.Hr(className="hrBody")
                            ],

                            # Nome que aparece na aba
                            label="Backtesting S.I"
                        ),

                        # Aba que apresenta os gráficos com intervalo de confiança
                        dbc.Tab(
                            id="tab-backtest-CI",
                            
                            class_name="nav-link active",
                            
                            children=[
                                # Linha que mostra a opção de trocar o empreendimento, o intervalo de confiança e o botão info
                                dropdown_confianca_row2CI,
                                
                                html.Hr(className="hrBody"), 
                                
                                # Primeira linha com gráficos
                                content_first_row_ci,
                                
                                html.Hr(className="hrBody"),
                                
                                # Segunda linha com gráficos
                                content_second_row_ci,
                                
                                html.Hr(className="hrBody"), 
                                
                                # Terceira linha com gráficos
                                content_third_row_ci,
                                
                                html.Hr(className="hrBody"),
                                
                                # Quarta linha com gráficos
                                content_fourth_row_ci,
                                
                                html.Hr(className="hrBody")
                            ],

                            # Nome que aparece na aba
                            label="Backtesting C.I"
                        )
                    ]
                ),

                # Tooltip da primeira aba
                dbc.Tooltip("Backtesting sem Intervalo de Confiança.", target="tab-backtest-SI", style={'font-family': 'Inter'}),

                # Tooltip da segunda aba
                dbc.Tooltip("Backtesting com Intervalo de Confiança.", target="tab-backtest-CI", style={'font-family': 'Inter'}),
            ]
        )

    # Página do realizado com bandas de erros também retorna uma Div completa
    elif pathname == error_bands_location:
        return html.Div(
            id="page-bandas",
            children=[
                # Título da página
                html.H2('Análise do realizado', style=TEXT_STYLE),

                # Spinner da troca de empreendimento
                dcc.Loading(
                    id="loading-asset-4",
                    className="loader",
                    children=html.Div(id="loading-output-asset-4"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=False,
                ),

                # Spinner da página
                dcc.Loading(
                    id="loading-4",
                    className="loading",
                    type="cube",
                    children=html.Div(id="loading-output-4"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=True,
                ),

                # Spinner do input de meses
                dcc.Loading(
                    id="loading-4",
                    className="loader",
                    children=html.Div(id="loading-output-41"),
                    color="#242e48",
                    loading_state={"component_name": "Carregando", "is_loading": True, "prop_name": None},
                    fullscreen=False,
                ),

                html.Hr(className="hrBody"),
                
                # Linha que mostra a opção de trocar empreendimento, o input de meses e o botão info
                dropdown_confianca_row,
                
                html.Hr(className="hrBody"),
                
                # Primeira linha com gráficos
                content_first_row_bandas,
                
                html.Hr(className="hrBody"),
                
                # Segunda linha com gráficos
                content_second_row_bandas,
                
                html.Hr(className="hrBody"), 
                
                # Terceira linha com gráficos
                content_third_row_bandas,
                
                html.Hr(className="hrBody"),
                
                # Quarta linha com gráficos
                content_fourth_row_bandas,
                
                html.Hr(className="hrBody")
            ]
        )

# Os callbacks abaixo fazem parte do controller do MVC, ele se comunica com as views e retorna ao usuário informações dos indicadores
###################################### CALLBACKS DOS INDICADORES POR TIPOLOGIA ######################################

@dash_app.callback(
    Output('card-ind-error-vat-typololy', 'children'),
    Output('card-std-error-vat-typololy', 'children'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')]
)
def update_indicator_typology_vat(tipologia_value, value):
    """Função dos indicadores para VENDAS ACUMULADAS TOTAIS da página de erros por tipologia.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_tipologia(tipologia_value, 'VENDAS ACUMULADAS TOTAIS', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-ct-typololy', 'children'),
    Output('card-std-error-ct-typololy', 'children'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')]
)
def update_indicator_typology_ct(tipologia_value, value):
    """Função dos indicadores para CANCELAMENTO TOTAL da página de erros por tipologia.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_tipologia(tipologia_value, 'CANCELAMENTO TOTAL', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-vgvla-typololy', 'children'),
    Output('card-std-error-vgvla-typololy', 'children'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')]
)
def update_indicator_typology_vgvla(tipologia_value, value):
    """Função dos indicadores para VGV LÍQUIDO ACUMULADO da página de erros por tipologia.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_tipologia(tipologia_value, 'VGV LÍQUIDO ACUMULADO', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-rbo-typololy', 'children'),
    Output('card-std-error-rbo-typololy', 'children'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')]
)
def update_indicator_typology_rbo(tipologia_value, value):
    """Função dos indicadores para RECEITA BRUTA OPERACIONAL da página de erros por tipologia.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_tipologia(tipologia_value, '(+) RECEITA BRUTA OPERACIONAL', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-co-typololy', 'children'),
    Output('card-std-error-co-typololy', 'children'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')]
)
def update_indicator_typology_co(tipologia_value, value):
    """Função dos indicadores CUSTOS OPERACIONAIS da página de erros por tipologia.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_tipologia(tipologia_value, '(=) CUSTOS OPERACIONAIS', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-do-typololy', 'children'),
    Output('card-std-error-do-typololy', 'children'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')]
)
def update_indicator_typology_do(tipologia_value, value):
    """Função dos indicadores para DESPESAS OPERACIONAIS da página de erros por tipologia.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_tipologia(tipologia_value, '(=) DESPESAS OPERACIONAIS', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-fcoa-typololy', 'children'),
    Output('card-std-error-fcoa-typololy', 'children'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')]
)
def update_indicator_typology_fcoa(tipologia_value, value):
    """Função dos indicadores para FLUXO DE CAIXA OPERACIONAL AJUSTADO da página de erros por tipologia.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_tipologia(tipologia_value, '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-fcfii-typololy', 'children'),
    Output('card-std-error-fcfii-typololy', 'children'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')]
)
def update_indicator_typology_fcfii(tipologia_value, value):
    """Função dos indicadores para FLUXO DE CAIXA FII da página de erros por tipologia.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_tipologia(tipologia_value, 'FLUXO DE CAIXA FII', value)
    return ind, std

###################################### CALLBACKS DOS INDICADORES POR EMPREENDIMENTOS ######################################

@dash_app.callback(
    Output('card-ind-error-vat-asset', 'children'),
    Output('card-std-error-vat-asset', 'children'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')]
)
def update_indicator_asset_vat(ativo_value, value):
    """Função dos indicadores para VENDAS ACUMULADAS TOTAIS da página de erros por ativos.

    Args:
        ativo_value (string): input do empreendimento (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_ativos(ativo_value, 'VENDAS ACUMULADAS TOTAIS', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-ct-asset', 'children'),
    Output('card-std-error-ct-asset', 'children'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')]
)
def update_indicator_asset_ct(ativo_value, value):
    """Função dos indicadores para CANCELAMENTO TOTAL da página de erros por ativos.

    Args:
        ativo_value (string): input do empreendimento (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_ativos(ativo_value, 'CANCELAMENTO TOTAL', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-vgvla-asset', 'children'),
    Output('card-std-error-vgvla-asset', 'children'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')]
)
def update_indicator_asset_vgvla(ativo_value, value):
    """Função dos indicadores para VGV LÍQUIDO ACUMULADO da página de erros por ativos.

    Args:
        ativo_value (string): input do empreendimento (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_ativos(ativo_value, 'VGV LÍQUIDO ACUMULADO', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-rbo-asset', 'children'),
    Output('card-std-error-rbo-asset', 'children'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')]
)
def update_indicator_asset_rbo(ativo_value, value):
    """Função dos indicadores para RECEITA BRUTA OPERACIONAL da página de erros por ativos.

    Args:
        ativo_value (string): input do empreendimento (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_ativos(ativo_value, '(+) RECEITA BRUTA OPERACIONAL', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-co-asset', 'children'),
    Output('card-std-error-co-asset', 'children'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')]
)
def update_indicator_asset_co(ativo_value, value):
    """Função dos indicadores para CUSTOS OPERACIONAIS da página de erros por ativos.

    Args:
        ativo_value (string): input do empreendimento (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_ativos(ativo_value, '(=) CUSTOS OPERACIONAIS', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-do-asset', 'children'),
    Output('card-std-error-do-asset', 'children'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')]
)
def update_indicator_asset_do(ativo_value, value):
    """Função dos indicadores para DESPESAS OPERACIONAIS da página de erros por ativos.

    Args:
        ativo_value (string): input do empreendimento (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_ativos(ativo_value, '(=) DESPESAS OPERACIONAIS', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-fcoa-asset', 'children'),
    Output('card-std-error-fcoa-asset', 'children'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')]
)
def update_indicator_asset_fcoa(ativo_value, value):
    """Função dos indicadores para FLUXO DE CAIXA OPERACIONAL AJUSTADO da página de erros por ativos.

    Args:
        ativo_value (string): input do empreendimento (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_ativos(ativo_value, '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', value)
    return ind, std

@dash_app.callback(
    Output('card-ind-error-fcfii-asset', 'children'),
    Output('card-std-error-fcfii-asset', 'children'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')]
)
def update_indicator_asset_fcfii(ativo_value, value):
    """Função dos indicadores para FLUXO DE CAIXA FII da página de erros por ativos.

    Args:
        ativo_value (string): input do empreendimento (default: Todos)
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador ponderado calculado com base no input de meses (default: 5)
        std (float): retorna o valor do desvio padrão ponderado calculado com base no input de meses (default: 5)
    """

    ind, std = indicadores_ativos(ativo_value, 'FLUXO DE CAIXA FII', value)
    return ind, std

# Os callbacks abaixo fazem parte do controller do MVC, ele se comunica com as views e retorna ao usuário os gráficos

###################################### CALLBACKS DO GRÁFICO DE ERRO POR TIPOLOGIA ######################################

@dash_app.callback(
    Output('input-meses', 'value'),
    Output('output-alert-tipologia', 'children'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')])
def callback_circular_tipologia(tip, value):
    """Função que gera o callbak circular para tipologia, o callback circular nada mais é do que retornar o input de meses para o defult
    ao trocar a tipologia. Essa função corresponde somente aos gráficos.

    Args:
        tip (string): input da tipologia (default: Todos)
        value (int): input dos meses (default: 5)

    Returns:
        value (int): retorna o valor default após trocar de tipologia
        alert (object): retorna um objeto dbc.Alert() informando qual tipologia está fora do input de meses informado
    """

    # O callback_context contém um dicionário com o histórico de ações do callback
    ctx = dash.callback_context

    # Pegando apenas a string do input tipologia
    trigger_id = ctx.triggered[0]["prop_id"].split(".")[0]

    # Função que vai retornar um sinal de alerta caso o input seja maior que a quantidade máxima de M's que uma tipologia tem
    alerta = alerta_tipologia(tip, value)
    
    # Se a variável conter a string 'tipologia', é porque trocou de tipologia, então retornar o input de meses para default
    if trigger_id == "tipologia":
        value = 5
    
    # Se verdadeiro constrói o alerta
    if alerta == True:
        alert = dbc.Alert(
            f"A tipologia {tip.upper()} só vai até M{value-1}.",
            color="danger",
            dismissable=True,
            is_open=True,
            duration=3000)

        value = value - 1
    else:
        alert = None

    return value, alert

@dash_app.callback(
    Output('table_error_typology', 'children'),
    Output('output-alert-table-tipologia', 'children'),
    [Input('input-meses-table-typology', 'value')])
def update_table_error_typology(value):
    """Função que retorna a tabela de indicadores de erros por tipologia e o alerta.

    Args:
        value (int): input de meses

    Returns:
        tabela (object): retorna a tabela de indicadores
        alert (object): retorna o alerta caso o input de meses seja maior que o número de M's das tipologias
    """

    # Função que chama a tabela
    tabela = return_table_typololy(value)

    # Se o objeto alerta.aux_tipologia for diferente de vazio, então há um alerta da tabela
    if alerta.aux_tipologia != None:
        alert = dbc.Alert(
            f"A tipologia {alerta.aux_tipologia.upper()} saiu da análise, pois ele só vai até M{value-1}.",
            color="danger",
            dismissable=True,
            is_open=True,
            fade=False,
            duration=3000)

        alerta.aux_tipologia = None
    else:
        alert = None
    
    return tabela, alert

@dash_app.callback(
    Output('graph_1_erro', 'figure'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')])
def update_graph_1_erro(tipologia_value, value):
    """Função que retorna o gráfico de VENDAS ACUMULADAS TOTAIS.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input dos meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    graph = opcao_grafico_erro(tipologia_value, 'VENDAS ACUMULADAS TOTAIS', value)

    return graph

@dash_app.callback(
    Output('graph_2_erro', 'figure'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')])
def update_graph_2_erro(tipologia_value, value):
    """Função que retorna o gráfico de CANCELAMENTO TOTAL.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input dos meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    graph = opcao_grafico_erro(tipologia_value, 'CANCELAMENTO TOTAL', value)
    return graph

@dash_app.callback(
    Output('graph_3_erro', 'figure'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')])
def update_graph_3_erro(tipologia_value, value):
    """Função que retorna o gráfico de VGV LÍQUIDO ACUMULADO.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input dos meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    graph = opcao_grafico_erro(tipologia_value, 'VGV LÍQUIDO ACUMULADO', value)
    return graph

@dash_app.callback(
    Output('graph_4_erro', 'figure'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')])
def update_graph_4_erro(tipologia_value, value):
    """Função que retorna o gráfico de RECEITA BRUTA OPERACIONAL.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input dos meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    graph = opcao_grafico_erro(tipologia_value, '(+) RECEITA BRUTA OPERACIONAL', value)
    return graph

@dash_app.callback(
    Output('graph_5_erro', 'figure'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')])
def update_graph_5_erro(tipologia_value, value):
    """Função que retorna o gráfico de CUSTOS OPERACIONAIS.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input dos meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    graph = opcao_grafico_erro(tipologia_value, '(=) CUSTOS OPERACIONAIS', value)
    return graph

@dash_app.callback(
    Output('graph_6_erro', 'figure'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')])
def update_graph_6_erro(tipologia_value, value):
    """Função que retorna o gráfico de DESPESAS OPERACIONAIS.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input dos meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    graph = opcao_grafico_erro(tipologia_value, '(=) DESPESAS OPERACIONAIS', value)
    return graph

@dash_app.callback(
    Output('graph_7_erro', 'figure'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')])
def update_graph_7_erro(tipologia_value, value):
    """Função que retorna o gráfico de FLUXO DE CAIXA OPERACIONAL AJUSTADO.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input dos meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    graph = opcao_grafico_erro(tipologia_value, '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', value)
    return graph

@dash_app.callback(
    Output('graph_8_erro', 'figure'),
    [Input('tipologia', 'value'), Input('input-meses', 'value')])
def update_graph_8_erro(tipologia_value, value):
    """Função que retorna o gráfico de FLUXO DE CAIXA FII.

    Args:
        tipologia_value (string): input da tipologia (default: Todos)
        value (int): input dos meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    graph = opcao_grafico_erro(tipologia_value, 'FLUXO DE CAIXA FII', value)
    return graph

###################################### CALLBACKS DO GRÁFICO DE ERRO POR ATIVOS ######################################

@dash_app.callback(
    Output('input-meses-ativos', 'value'),
    Output('output-alert-ativo', 'children'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')])
def callback_circular_ativos(empreendimento, value):
    """Função que gera o callbak circular para ativos, o callback circular nada mais é do que retornar o input de meses para o defult
    ao trocar o empreendimento. Essa função corresponde somente aos gráficos.

    Args:
        empreendimento (string): input de empreendimento (default: AR Lotes Jardim Europa, ordem alfabética)
        value (int): input de meses (default: 5)

    Returns:
        value (int): retorna o número default para a caxinha
        alert (object): retorna o objeto dbc.Alert() informando que o input de meses é maior que o número de M's dos empreendimentos
    """
    
    # Retorna um dicionário histórico do callback
    ctx = dash.callback_context

    # Pegando o id do input
    trigger_id = ctx.triggered[0]["prop_id"].split(".")[0]

    # Função que retorna um sinal, caso o input de meses seja maior que o número de M's dos empreendimentos
    alerta = alerta_ativo(empreendimento, value)
    
    # Se a variável conter a string 'empreendimento3', é porque trocou do ativo, então retornar o input de meses para default
    if trigger_id == "empreendimento3":
        value = 5
    
    # Se houver um sinal de alerta, então criar o alerta
    if alerta == True:
        alert = dbc.Alert(
            f"O ativo {empreendimento.upper()} só vai até M{value-1}",
            color="danger",
            dismissable=True,
            is_open=True,
            duration=3000)

        value = value - 1
    else:
        alert = None

    return value, alert

@dash_app.callback(
    Output('table_error', 'children'),
    Output('input-meses-table-asset', 'value'),
    #Output('output-alert-table-ativo', 'children'),
    [Input('input-meses-table-asset', 'value'), Input('tipologia3', 'value')])
def update_table_error_asset(value, tipologia):
    """Função que retorna a tabela de indicadores por ativo e o valor default do input de meses.

    Args:
        value (int): input de meses (default: 5)
        tipologia (string): input de tipologia, por causa do filtro

    Returns:
        tabela (object): retorna a tabela
        value (int): retorna o input de meses default
    """

    # Histórico de callbacks
    ctx = dash.callback_context

    # Pega o id do input de tipologia
    trigger_id = ctx.triggered[0]["prop_id"].split(".")[0]

    # Retorna o input de meses para o default
    if trigger_id == "tipologia3":
        value = 5

    # Retorna a tabela
    tabela = return_table(value, tipologia)

    # Cria o alerta, mas o output do alerta está desativado
    if alerta.aux_ativos != None:
        alert = dbc.Alert(
            f"O(s) empreendimento(s) {alerta.lista_aux} saiu/saíram da análise, pois ele(s) só vai/vão até M{value-1}.",
            color="danger",
            dismissable=True,
            is_open=True,
            fade=False,
            duration=5000)

        alerta.aux_ativos = None
        alerta.lista_aux = ''
    else:
        alert = None

    return tabela, value

@dash_app.callback(
    Output('graph_1_asset', 'figure'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')])
def update_graph_1_asset(dropdown_value, value):
    """Função que retorna o gráfico de VENDAS ACUMULADAS TOTAIS da página de erros por ativos.

    Args:
        dropdown_value (string): input do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_asset(dropdown_value, 'VENDAS ACUMULADAS TOTAIS', value)

@dash_app.callback(
    Output('graph_2_asset', 'figure'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')])
def update_graph_2_asset(dropdown_value, value):
    """Função que retorna o gráfico de CANCELAMENTO TOTAL da página de erros por ativos.

    Args:
        dropdown_value (string): input do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_asset(dropdown_value, 'CANCELAMENTO TOTAL', value)

@dash_app.callback(
    Output('graph_3_asset', 'figure'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')])
def update_graph_3_asset(dropdown_value, value):
    """Função que retorna o gráfico de VGV LÍQUIDO ACUMULADO da página de erros por ativos.

    Args:
        dropdown_value (string): input do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_asset(dropdown_value, 'VGV LÍQUIDO ACUMULADO', value)

@dash_app.callback(
    Output('graph_4_asset', 'figure'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')])
def update_graph_4_asset(dropdown_value, value):
    """Função que retorna o gráfico de RECEITA BRUTA OPERACIONAL da página de erros por ativos.

    Args:
        dropdown_value (string): input do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_asset(dropdown_value, '(+) RECEITA BRUTA OPERACIONAL', value)

@dash_app.callback(
    Output('graph_5_asset', 'figure'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')])
def update_graph_5_asset(dropdown_value, value):
    """Função que retorna o gráfico de CUSTOS OPERACIONAIS da página de erros por ativos.

    Args:
        dropdown_value (string): input do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_asset(dropdown_value, '(=) CUSTOS OPERACIONAIS', value)

@dash_app.callback(
    Output('graph_6_asset', 'figure'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')])
def update_graph_6_asset(dropdown_value, value):
    """Função que retorna o gráfico de DESPESAS OPERACIONAIS da página de erros por ativos.

    Args:
        dropdown_value (string): input do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_asset(dropdown_value, '(=) DESPESAS OPERACIONAIS', value)

@dash_app.callback(
    Output('graph_7_asset', 'figure'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')])
def update_graph_7_asset(dropdown_value, value):
    """Função que retorna o gráfico de FLUXO DE CAIXA OPERACIONAL AJUSTADO da página de erros por ativos.

    Args:
        dropdown_value (string): input do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_asset(dropdown_value, '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', value)

@dash_app.callback(
    Output('graph_8_asset', 'figure'),
    [Input('empreendimento3', 'value'), Input('input-meses-ativos', 'value')])
def update_graph_8_asset(dropdown_value, value):
    """Função que retorna o gráfico de FLUXO DE CAIXA FII da página de erros por ativos.

    Args:
        dropdown_value (string): input do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_asset(dropdown_value, 'FLUXO DE CAIXA FII', value)

###################################### CALLBACKS DO GRÁFICO DO REALIZADO COM BANDAS ######################################

@dash_app.callback(
    Output('input-data', 'value'),
    [Input('empreendimento', 'value'), Input('input-data', 'value')])
def callback_circular_realizado(dropdown, value):
    """Função de callback circular para o input de meses da página do realizado com bandas.

    Args:
        dropdown (string): input do empreendimento (default: AR Lotes Jardim Europa, ordem alfabética)
        value (int): input de meses (default: 10)

    Returns:
        value (int): retorna o input de meses default
    """
    
    # Histórico de callbacks
    ctx = dash.callback_context
    
    # Pega o id 'empreendimento'
    trigger_id = ctx.triggered[0]["prop_id"].split(".")[0]
    
    # Se for igual é porque trocou de empreendimento
    if trigger_id == "empreendimento":
        value = 10

    return value

@dash_app.callback(
    Output('graph_1_bandas', 'figure'),
    [Input('empreendimento', 'value'), Input('input-data', 'value')])
def update_graph_1_bandas(dropdown_value, value):
    """Função que retorna o gráfico de VENDAS ACUMULADAS TOTAIS da página do realizado com bandas.

    Args:
        dropdown_value (string): input de empreendimento (default: AR Lotes Jardim Europa)
        value (int): input de meses (default: 10)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_bandas(dropdown_value, 'VENDAS ACUMULADAS TOTAIS', input_data=value)

@dash_app.callback(
    Output('graph_2_bandas', 'figure'),
    [Input('empreendimento', 'value'), Input('input-data', 'value')])
def update_graph_2_bandas(dropdown_value, value):
    """Função que retorna o gráfico de CANCELAMENTO TOTAL da página do realizado com bandas.

    Args:
        dropdown_value (string): input de empreendimento (default: AR Lotes Jardim Europa)
        value (int): input de meses (default: 10)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_bandas(dropdown_value, 'CANCELAMENTO TOTAL', input_data=value)

@dash_app.callback(
    Output('graph_3_bandas', 'figure'),
    [Input('empreendimento', 'value'), Input('input-data', 'value')])
def update_graph_3_bandas(dropdown_value, value):
    """Função que retorna o gráfico de VGV LÍQUIDO ACUMULADO da página do realizado com bandas.

    Args:
        dropdown_value (string): input de empreendimento (default: AR Lotes Jardim Europa)
        value (int): input de meses (default: 10)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_bandas(dropdown_value, 'VGV LÍQUIDO ACUMULADO', input_data=value)

@dash_app.callback(
    Output('graph_4_bandas', 'figure'),
    [Input('empreendimento', 'value'), Input('input-data', 'value')])
def update_graph_4_bandas(dropdown_value, value):
    """Função que retorna o gráfico de RECEITA BRUTA OPERACIONAL da página do realizado com bandas.

    Args:
        dropdown_value (string): input de empreendimento (default: AR Lotes Jardim Europa)
        value (int): input de meses (default: 10)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_bandas(dropdown_value, '(+) RECEITA BRUTA OPERACIONAL', input_data=value)

@dash_app.callback(
    Output('graph_5_bandas', 'figure'),
    [Input('empreendimento', 'value'), Input('input-data', 'value')])
def update_graph_5_bandas(dropdown_value, value):
    """Função que retorna o gráfico de CUSTOS OPERACIONAIS da página do realizado com bandas.

    Args:
        dropdown_value (string): input de empreendimento (default: AR Lotes Jardim Europa)
        value (int): input de meses (default: 10)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_bandas(dropdown_value, '(=) CUSTOS OPERACIONAIS', input_data=value)

@dash_app.callback(
    Output('graph_6_bandas', 'figure'),
    [Input('empreendimento', 'value'), Input('input-data', 'value')])
def update_graph_6_bandas(dropdown_value, value):
    """Função que retorna o gráfico de DESPESAS OPERACIONAIS da página do realizado com bandas.

    Args:
        dropdown_value (string): input de empreendimento (default: AR Lotes Jardim Europa)
        value (int): input de meses (default: 10)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_bandas(dropdown_value, '(=) DESPESAS OPERACIONAIS', input_data=value)

@dash_app.callback(
    Output('graph_7_bandas', 'figure'),
    [Input('empreendimento', 'value'), Input('input-data', 'value')])
def update_graph_7_bandas(dropdown_value, value):
    """Função que retorna o gráfico de FLUXO DE CAIXA OPERACIONAL AJUSTADO da página do realizado com bandas.

    Args:
        dropdown_value (string): input de empreendimento (default: AR Lotes Jardim Europa)
        value (int): input de meses (default: 10)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_bandas(dropdown_value, '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', input_data=value)

@dash_app.callback(
    Output('graph_8_bandas', 'figure'),
    [Input('empreendimento', 'value'), Input('input-data', 'value')])
def update_graph_8_bandas(dropdown_value, value):
    """Função que retorna o gráfico de FLUXO DE CAIXA FII da página do realizado com bandas.

    Args:
        dropdown_value (string): input de empreendimento (default: AR Lotes Jardim Europa)
        value (int): input de meses (default: 10)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_grafico_bandas(dropdown_value, 'FLUXO DE CAIXA FII', input_data=value)

###################################### CALLBACKS DO GRÁFICO DE BACKTESTING SEM INTERVALO DE CONFIANÇA ######################################

@dash_app.callback(
    Output('graph_1', 'figure'),
    [Input('empreendimento2', 'value')])
def update_graph_1(dropdown_value):
    """Função que retorna o gráfico de VENDAS ACUMULADAS TOTAIS do backtesting sem intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, 'VENDAS ACUMULADAS TOTAIS', confianca=True)

@dash_app.callback(
    Output('graph_2', 'figure'),
    [Input('empreendimento2', 'value')])
def update_graph_2(dropdown_value):
    """Função que retorna o gráfico de CANCELAMENTO TOTAL do backtesting sem intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, 'CANCELAMENTO TOTAL', confianca=True)

@dash_app.callback(
    Output('graph_3', 'figure'),
    [Input('empreendimento2', 'value')])
def update_graph_3(dropdown_value):
    """Função que retorna o gráfico de VGV LÍQUIDO ACUMULADO do backtesting sem intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, 'VGV LÍQUIDO ACUMULADO', confianca=True)

@dash_app.callback(
    Output('graph_4', 'figure'),
    [Input('empreendimento2', 'value')])
def update_graph_4(dropdown_value):
    """Função que retorna o gráfico de RECEITA BRUTA OPERACIONAL do backtesting sem intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, '(+) RECEITA BRUTA OPERACIONAL', confianca=True)

@dash_app.callback(
    Output('graph_5', 'figure'),
    [Input('empreendimento2', 'value')])
def update_graph_5(dropdown_value):
    """Função que retorna o gráfico de CUSTOS OPERACIONAIS do backtesting sem intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, '(=) CUSTOS OPERACIONAIS', confianca=True)

@dash_app.callback(
    Output('graph_6', 'figure'),
    [Input('empreendimento2', 'value')])
def update_graph_6(dropdown_value):
    """Função que retorna o gráfico de DESPESAS OPERACIONAIS do backtesting sem intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, '(=) DESPESAS OPERACIONAIS', confianca=True)

@dash_app.callback(
    Output('graph_7', 'figure'),
    [Input('empreendimento2', 'value')])
def update_graph_7(dropdown_value):
    """Função que retorna o gráfico de FLUXO DE CAIXA OPERACIONAL AJUSTADO do backtesting sem intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', confianca=True)

@dash_app.callback(
    Output('graph_8', 'figure'),
    [Input('empreendimento2', 'value')])
def update_graph_8(dropdown_value):
    """Função que retorna o gráfico de FLUXO DE CAIXA FII do backtesting sem intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, 'FLUXO DE CAIXA FII', confianca=True)

###################################### CALLBACKS DO GRÁFICO DE BACKTESTING COM INERVALO DE CONFIANÇA ######################################

@dash_app.callback(
    Output('intervalo-confianca', 'value'),
    [Input('empreendimento4', 'value'), Input('intervalo-confianca', 'value')])
def callback_circular_backtesting(dropdown, value):
    """Função de callback circular do intervalo de confiança, ela vai retorna ao default sempre que mudar de empreendimento.

    Args:
        dropdown (string): input de empreendimento (default: AR Lotes Jardim Europa, ordem alfabética)
        value (float): input do intervalo de confiança (default: 0.95)

    Returns:
        value (float): retorna o default do intervalo de confiança
    """

    # Histórico do callback
    ctx = dash.callback_context

    # Pega o id 'empreendimento4'
    trigger_id = ctx.triggered[0]["prop_id"].split(".")[0]
    
    # Se for verdadeira, coloca o default da variável
    if trigger_id == "empreendimento4":
        value = 0.95

    return value

@dash_app.callback(
    Output('graph_1_ci', 'figure'),
    [Input('empreendimento4', 'value'), Input('intervalo-confianca', 'value')])
def update_graph_1_ci(dropdown_value, value):
    """Função que retorna o gráfico de VENDAS ACUMULADAS TOTAIS da página de backtesting com intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)
        value (float): input do intervalo de confiança (default: 0.95)

    Returns:
        return (object): retorna o gráfico
    """
    
    return opcao_empreendimento(dropdown_value, 'VENDAS ACUMULADAS TOTAIS', confianca=value)

@dash_app.callback(
    Output('graph_2_ci', 'figure'),
    [Input('empreendimento4', 'value'), Input('intervalo-confianca', 'value')])
def update_graph_2_ci(dropdown_value, value):
    """Função que retorna o gráfico de CANCELAMENTO TOTAL da página de backtesting com intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)
        value (float): input do intervalo de confiança (default: 0.95)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, 'CANCELAMENTO TOTAL', confianca=value)

@dash_app.callback(
    Output('graph_3_ci', 'figure'),
    [Input('empreendimento4', 'value'), Input('intervalo-confianca', 'value')])
def update_graph_3_ci(dropdown_value, value):
    """Função que retorna o gráfico de VGV LÍQUIDO ACUMULADO da página de backtesting com intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)
        value (float): input do intervalo de confiança (default: 0.95)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, 'VGV LÍQUIDO ACUMULADO', confianca=value)

@dash_app.callback(
    Output('graph_4_ci', 'figure'),
    [Input('empreendimento4', 'value'), Input('intervalo-confianca', 'value')])
def update_graph_4_ci(dropdown_value, value):
    """Função que retorna o gráfico de RECEITA BRUTA OPERACIONAL da página de backtesting com intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)
        value (float): input do intervalo de confiança (default: 0.95)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, '(+) RECEITA BRUTA OPERACIONAL', confianca=value)

@dash_app.callback(
    Output('graph_5_ci', 'figure'),
    [Input('empreendimento4', 'value'), Input('intervalo-confianca', 'value')])
def update_graph_5_ci(dropdown_value, value):
    """Função que retorna o gráfico de CUSTOS OPERACIONAIS da página de backtesting com intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)
        value (float): input do intervalo de confiança (default: 0.95)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, '(=) CUSTOS OPERACIONAIS', confianca=value)

@dash_app.callback(
    Output('graph_6_ci', 'figure'),
    [Input('empreendimento4', 'value'), Input('intervalo-confianca', 'value')])
def update_graph_6_ci(dropdown_value, value):
    """Função que retorna o gráfico de DESPESAS OPERACIONAIS da página de backtesting com intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)
        value (float): input do intervalo de confiança (default: 0.95)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, '(=) DESPESAS OPERACIONAIS', confianca=value)

@dash_app.callback(
    Output('graph_7_ci', 'figure'),
    [Input('empreendimento4', 'value'), Input('intervalo-confianca', 'value')])
def update_graph_7_ci(dropdown_value, value):
    """Função que retorna o gráfico de FLUXO DE CAIXA OPERACIONAL AJUSTADO da página de backtesting com intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)
        value (float): input do intervalo de confiança (default: 0.95)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', confianca=value)

@dash_app.callback(
    Output('graph_8_ci', 'figure'),
    [Input('empreendimento4', 'value'), Input('intervalo-confianca', 'value')])
def update_graph_8_ci(dropdown_value, value):
    """Função que retorna o gráfico de FLUXO DE CAIXA FII da página de backtesting com intervalo de confiança.

    Args:
        dropdown_value (string): input do empreendimento (default: AR Lotes Jardim Europa)
        value (float): input do intervalo de confiança (default: 0.95)

    Returns:
        return (object): retorna o gráfico
    """

    return opcao_empreendimento(dropdown_value, 'FLUXO DE CAIXA FII', confianca=value)

################################################################################################################

# Roda o aplicativo
if __name__ == '__main__':
    #dash_app.run_server(host='0.0.0.0', port=80)
    dash_app.run_server(host='0.0.0.0', port=80, use_reloader=False)