# Link das páginas
home_page_location = "/"
error_page_location = "/errorPageTypology"
error_page_assets_location = "/errorAssets"
error_bands_location = "/erroBandas"
backtesting_location = "/backtesting"

# Define o estilo e argumentos para o sidebar
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "20%",
    "padding": "20px 10px",
    "background-color": "#242e48",
    "z-index": 1,
    "overflow-x": "hidden",
    "transition": "all 0.5s",
}

# Define o estilo e argumentos para a content page
CONTENT_STYLE = {
    "margin-left": "25%",
    "margin-right": "5%",
    "padding": "20px 10px",
    "transition": "margin-left .5s",
    "background-color": "#FFFFFF"
}

# Define o estilo textual
TEXT_STYLE = {
    'textAlign': 'center',
    'color': '#242e48',
    'fontWeight': 'bold',
    'font-family': 'Inter'
}

# Define o estilo de subtítulos
TEXT_SUBTITLE = {
    'textAlign': 'left',
    'color': '#242e48',
    'fontWeight': 'bold',
    'font-family': 'Inter'
}

# Define os estilos dos cards
CARD_TEXT_STYLE = {
    'textAlign': 'center',
    'color': '#0074D9',
    'fontWeight': 'bold',
    'font-family': 'Inter'
}

# Define o posicionamento da imagem do sidebar
IMAGEM_SIDEBAR = {
    'bottom': 0,
    'left': '15%',
    'margin-left': '-40px',
    'position': 'absolute',
    'width': 'auto',
    'height': '80px',
}

# Define cores para os textos
colors = {
    'background': '#9fb2cd', #7791B8
    'text': '#FFFFFF',
    'fontWeight': 'bold',
    'font-family': 'Inter'
}

# Define a configuração de sombra
color_shadow = {
    'color_botton': '#242e48',
    'color_graph': '#DCE3EC',
    'color_card': '#DCE3EC'
}

# Define a classe e objeto para gerar alertas
class AlertaTipologia:
    def __init__(self) -> None:
        self.list_tipologia = []
        self.aux_tipologia = None
        self.list_ativos = []
        self.lista_aux = ''
        self.aux_ativos = None
        self.anterior = 4

alerta = AlertaTipologia()