import plotly.graph_objects as go
import pandas as pd
from model.read_data import *
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import summary_table
import warnings
warnings.filterwarnings("ignore")

pd.set_option('display.max_columns', None)

def grafico_final(variavel, realizado, realizado_aux, erros_pred_realizado):
    """Função que gera a figura dos gráficos do realizado com bandas.

    Args:
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        realizado (DataFrame): dataframe do realizado 
        realizado_aux (DataFrame): dataframe que corresponde ao previsto
        erros_pred_realizado (DataFrame): dataframe dos erros da regressão linear

    Returns:
        fig (object): retorna figura do gráfico
    """
    
    df = pd.DataFrame(None)

    # Coloca em absoluto as variáveis que não são Fluxo de Caixa
    if variavel not in ['(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO', 'FLUXO DE CAIXA FII']:
        realizado[variavel] = abs(realizado[variavel])
        df['DATA'] = realizado_aux['DATA']
        df['Indicadores'] = erros_pred_realizado['INDICADORES'].values
        df[variavel] = abs(realizado_aux[variavel])
        df['Erro'] = abs(erros_pred_realizado[variavel].round(4).values)
        erro = df['Erro'].values
        erro[0] = 0.0001
        df['Erro'] = erro
        df['Lim_Inf'] = df[variavel] - (df[variavel] * df['Erro'])
        df['Lim_Sup'] = df[variavel] + (df[variavel] * df['Erro'])
        df['Lim_Inf'] = df['Lim_Inf']
        df['Lim_Sup'] = df['Lim_Sup']
        sup_name = "Banda Superior"
        inf_name = "Banda Inferior"
    else:
        df['DATA'] = realizado_aux['DATA']
        df['Indicadores'] = erros_pred_realizado['INDICADORES'].values
        df[variavel] = realizado_aux[variavel]
        df['Erro'] = abs(erros_pred_realizado[variavel].round(4).values)
        erro = df['Erro'].values
        erro[0] = 0.001
        df['Erro'] = erro
        df['Lim_Inf'] = df[variavel] - (abs(df[variavel] * df['Erro']))
        df['Lim_Sup'] = df[variavel] + (abs(df[variavel] * df['Erro']))
        df['Lim_Inf'] = df['Lim_Inf']
        df['Lim_Sup'] = df['Lim_Sup']
        sup_name = "Banda Superior"
        inf_name = "Banda Inferior"

    df = df.reset_index(drop=True)

    fig = go.Figure()
    
    fig.add_trace(go.Scatter(
        x=df['DATA'], 
        y=df[variavel],
        name="Previsto", 
        mode='lines',
        line={"width":3},
        marker={"color":'#E54D8D', "size": 3}
    ))
    fig.add_trace(go.Scatter(
        x=realizado['DATA'], 
        y=realizado[variavel], 
        name="Realizado", 
        mode='lines',
        line={"width":3},
        marker={"color":'#242e48', "size": 3}
    ))
    fig.add_trace(go.Scatter(
        x=realizado_aux['DATA'], 
        y=df['Lim_Inf'],
        name=inf_name,
        mode='lines',
        fillcolor='rgba(60, 78, 120, 0.3)',
        #line_color='rgba(68, 68, 68, 0.3)',
        line={'width':0},
        showlegend=False
    ))
    fig.add_trace(go.Scatter(
        x=realizado_aux['DATA'], 
        y=df['Lim_Sup'], 
        name=sup_name, 
        marker= {'color':"#444"},
        mode='lines',
        fill='tonexty',
        fillcolor='rgba(60, 78, 120, 0.3)', 
        #line_color='rgba(68, 68, 68, 0.3)',
        line={'width':0},
        showlegend=False
    ))

    fig.update_yaxes(showgrid=True, zeroline=False, rangemode="tozero", color="#242e48")
    fig.update_xaxes(showgrid=False, zeroline=False, zerolinewidth=2, color="#242e48")

    # Coloca o título do eixo y e organiza os nomes das variáveis para o gráfico
    if variavel == "CANCELAMENTO TOTAL" or variavel == "VENDAS ACUMULADAS TOTAIS":
        titulo_eixo_y = "Quantidade"
        var = variavel.lower().capitalize()
    elif variavel == "VGV LÍQUIDO ACUMULADO":
        titulo_eixo_y = "R$"
        var = variavel[:3] + variavel[3:].lower()
    elif variavel == "FLUXO DE CAIXA FII":
        titulo_eixo_y = "R$"
        var = variavel[:14].capitalize() + variavel[14:]
    else:
        titulo_eixo_y = "R$"
        s = variavel.lower()
        var = s[4].upper() + s[5:]

    if variavel == "CANCELAMENTO TOTAL" or variavel == "VENDAS ACUMULADAS TOTAIS":
        fig.update_layout(
            title={"text":var, "font": {"color": "#242e48", "family": "Inter"}},
            font_family="Inter",
            title_font_family="Inter",
            xaxis_title="Meses",
            yaxis_title=titulo_eixo_y,
            autosize=True,
            paper_bgcolor="#DCE3EC",
            plot_bgcolor="#DCE3EC",
            legend={"font": {'color': "#242e48", "family": "Inter"}},
        )
    else:
        fig.update_layout(
            title={"text":var, "font": {"color": "#242e48", "family": "Inter"}},
            font_family="Inter",
            title_font_family="Inter",
            xaxis_title="Meses",
            yaxis_title=titulo_eixo_y,
            autosize=True,
            paper_bgcolor="#DCE3EC", #BAC8DB
            plot_bgcolor="#DCE3EC",
            legend={"font": {'color': "#242e48", "family": "Inter"}},
        )

    return fig

def empilha_erro_de_um_empreendimento(empreendimento):
    """Função que empilha os erros calculados de um empreendimento.

    Args:
        empreendimento (string): nome do empreendimento

    Returns:
        dfs_erro_por_empreendimento (DataFrame): retorna um novo dataframe com os dados empilhados
    """

    lista_pxr = list(erro_pxr_aux[empreendimento]['previstos'].keys())

    dfs_erro_por_empreendimento = erro_pxr_aux[empreendimento]['previstos'][lista_pxr[0]] # Atribui o primeiro pxr

    for i in range(1, len(lista_pxr)):
        dfs_erro_por_empreendimento = pd.concat([dfs_erro_por_empreendimento, erro_pxr_aux[empreendimento]['previstos'][lista_pxr[i]]], axis=0)

    dfs_erro_por_empreendimento = dfs_erro_por_empreendimento.reset_index(drop=True)

    return dfs_erro_por_empreendimento

def replace_meses(preditos):
    """Função que retorna os indicadores para M1, M2,..., M_n.

    Args:
        preditos (DataFrame): DataFrame da regressão linear

    Returns:
        preditos: retorna o mesmo dataframe, mas com os indicadores trocados
    """

    dict_replace = {}
    
    for i in range(preditos.shape[0]):
        dict_replace[i+1] = f"M{i+1}"

    preditos['INDICADORES'].replace(dict_replace, inplace=True)

    return preditos

def regressao(empreendimento, input_data, df):
    """Função que faz a regressão linear.

    Args:
        empreendimento (string): nome do empreendimento
        input_data (int): input de meses fornecido pelo usuário
        df (DataFrame): dataframe com as médias dos M_i calculados

    Returns:
        preditos (DataFrame): retorna um novo dataframe com a regressão linear
    """
    # Pega a chave do PxR do realizado, por exemplo: 2202 Solange
    nome_pxr_realizado = list(empr[empreendimento].keys())[-1]

    # Extrai o mês e ano da chave
    mes = int(nome_pxr_realizado.split()[0][2:])
    ano = int(nome_pxr_realizado.split()[0][:2])
    
    # Constrói a data do realizado
    if ano == 21:
        data1 = f'2021-{mes}-01'
    else:
        data1 = f'2022-{mes}-01'
    
    # Pega o realizado ad aeternum
    realizado = pxr_realizados_extend[empreendimento]

    # Faz a filtragem pegando os dados a partir da data de referência, o que corresponde ao previsto
    realizado = realizado[realizado.DATA >= data1].reset_index(drop=True)

    num_meses = input_data

    # Faz uma nova filtragem de acordo com o input de meses
    realizado = realizado[realizado.index < num_meses]
    
    # Constrói o dataframe e instancia as datas e os indicadores de meses
    preditos = pd.DataFrame()
    preditos['DATA'] = realizado.DATA
    preditos['INDICADORES'] = [i for i in range(1, num_meses+1)]

    # Para cada variável do PxR
    for var in n_cols:
        X = df['INDICADORES'].values.reshape(-1,1) 
        y = df[var].values

        X = sm.add_constant(X)
        res = sm.OLS(y, X).fit()
        
        st, data, ss2 = summary_table(res, alpha=0.05)
        
        preds = pd.DataFrame.from_records(data, columns=[s.replace('\n', ' ') for s in ss2]).sort_values(by='Obs')
        
        y_pred = res.predict(sm.add_constant(preditos['INDICADORES']))

        preditos[var] = sorted(y_pred)
        preditos[var] = preditos[var].round(4)

    preditos = replace_meses(preditos)

    return preditos.reset_index(drop=True)

def media_ms(df):
    """Função que faz as médias para M1, M2,..., M_n.

    Args:
        df (DataFrame): DataFrame com os dados empilhados

    Returns:
        df_aux (DataFrame): retorna um novo DataFrame com as médias calculadas
    """

    # Tranformas os indicadores M1, M2,..., M_n para 1, 2,..., 3
    df.INDICADORES = df.INDICADORES.apply(lambda x: int(x[1:]))
    
    # Cria o novo dataframe e instancia os indicadores de meses
    df_aux = pd.DataFrame()
    df_aux['INDICADORES'] = list(df.INDICADORES.unique())

    # Percorre cada coluna fazendo as médias
    for var in n_cols:
        media = []

        # Faz o filtro por cada M_i
        for m in list(df.INDICADORES.unique()):
            dfm = df[df.INDICADORES == m]

            if dfm.shape[0] == 1:
                media.append(round(abs(dfm[var].values[0]), 4))
            else:
                media.append(dfm[var].abs().mean().round(4))

        df_aux[var] = media

    return df_aux

def plateau(alist):
    """Função destinada a encontrar o platô das variáveis.

    Args:
        alist (list): lista com os valores das variáveis

    Returns:
        flag (bool): retorna uma flag indicando se há platô ou não
        value (int ou float): retorna o valor do platô, podendo ser inteiro ou float
    """
    # Variáveis
    flag = False
    value = None

    # Loop que percorre a lista
    for i in range(1, len(alist)-1):
        # Verifica se pelo menos o mesmo número se repete em três posições seguidas
        if alist[i] == alist[i-1] == alist[i+1]:
            flag = True
            value = alist[i]

    return value, flag

def opcao_grafico_bandas(empreendimento, variavel, input_data=10):
    """Função que é chamada em app.py e retorna o gráfico de cada variável.

    Args:
        empreendimento (string): nome do empreendimento, por exemplo: Solange
        variavel (string): variável informada, por exemplo: Vendas Acumuladas Totais
        input_data (int, optional): input de meses (default: 10)

    Returns:
        fig (object): retorna a figura do gráfico gerado
    """

    # Empilha todos os erros de um empreendimento
    erro_por_empr_empilhado = empilha_erro_de_um_empreendimento(empreendimento)
    
    # Faz as médias de M1, M2,..., M_n
    df = media_ms(erro_por_empr_empilhado)

    # Pega o realizado
    realizado = pxr_realizados_extend[empreendimento]

    # Pega a chave do realizado, por exemplo: 2202 Solange
    nome_pxr_realizado = list(empr[empreendimento].keys())[-1] # O último é o realizado

    # Pega a tabela com a regressão calculada
    epred = regressao(empreendimento, input_data, df)

    # Extrai da chave anterior o ano e o mês
    ano = int(nome_pxr_realizado.split()[0][:2])
    mes = int(nome_pxr_realizado.split()[0][2:])

    # Constrói a data do realizado de acordo com ano e mês
    if ano == 21:
        if mes > 9:
            data = f"2021-{mes}-01"
        else:
            data = f"2021-0{mes}-01"
    else:
        if mes > 9:
            data = f"2022-{mes}-01"
        else:
            data = f"2022-0{mes}-01"

    # Filtra o realizado, pegando os dados com data maior ou igual a data de referência do realizado
    realizado_aux = realizado[realizado.DATA >= data].reset_index(drop=True)

    # Filtra o realizado com base no input de meses informado pelo usuário, por exemplo: 10
    realizado_aux = realizado_aux[realizado_aux.index < input_data]

    # Recontrói os indicadores M1, M2,..., M_n
    realizado_aux.INDICADORES = [f"M{i}" for i in range(1, realizado_aux.shape[0]+1)]
    
    # Filtro um novo realizado, pegando os dados com data menor ou igual a data de referência do realizado
    realizado = realizado[realizado.DATA <= data]
    
    # Chama a função que vai identificar se tem o platô
    value, flag = plateau(list(realizado_aux[variavel]))
    
    # Se existir o platô o códido entra no if
    if flag:
        # Pegando uma lista com as posições onde está o número platô
        lista_pos = list(realizado_aux[realizado_aux[variavel] == value].index)
        
        # Pega a primeira posição que começa o platô
        pos = lista_pos[0]

        # Faz um filtro do realizado deixando pelo menos 3 posições a mais do platô
        real_aux = realizado_aux[realizado_aux.index <= (pos+3)]

        # Faz o mesmo com a regressão
        epred_aux = epred[epred.index <= (pos+3)]

        # Posições maiores ou iguais a do platô recebem erro 0
        epred_aux.loc[epred_aux.index >= pos, variavel] = 0.0000
    else:
        # Caso não exista platô segue o dados normalmente
        real_aux = realizado_aux.copy()
        epred_aux = epred.copy()
    
    # Chama a função que retorna a figura do gráfico
    fig = grafico_final(variavel, realizado, real_aux, epred_aux)

    return fig