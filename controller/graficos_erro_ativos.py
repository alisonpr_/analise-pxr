import plotly.graph_objects as go
import pandas as pd
from model.read_data import *
import numpy as np
import warnings
warnings.filterwarnings("ignore")

pd.set_option('display.max_columns', None)

def erro_medio_positivo_negativo(df_filtrado, variavel, meses):
    """Função que faz as médias do erros pessimista, otimista e geral.

    Args:
        df_filtrado (DataFrame): dataframe filtrado de acordo com cada M_i
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        meses (list): lista com os indicadores de meses: M1, M2,..., M_n

    Returns:
        media (list): retorna uma lista com as médias de cada M_i
        media_pos (list): retorna uma lista com as médias otimistas de cada M_i
        media_neg (list): retorna uma lista com as médias pessimistas de cada M_i
    """

    # Cria as listas
    media, media_pos, media_neg = [], [], []

    # Loop que faz as médias baseando-se no filtro
    for m in meses:
        # Filtragem
        df_m = df_filtrado[df_filtrado['INDICADORES'] == m]

        # Fazendo a média geral
        if df_m.shape[0] == 1:
            media.append(abs(df_m[variavel].values[0]))
        elif df_m.shape[0] == 0:
            media.append(0)
        else:
            media.append(df_m[variavel].abs().mean())

        # Filtrando os valores positivos
        pos_m = df_m[df_m[variavel] > 0]

        # Filtrando os valores negativos
        neg_m = df_m[df_m[variavel] < 0]

        # Fazendo a média otimista
        if pos_m.shape[0] == 1:
            media_pos.append(abs(pos_m[variavel].values[0]))
        elif pos_m.shape[0] == 0:
            media_pos.append(0)
        else:
            media_pos.append(pos_m[variavel].abs().mean())

        # Fazendo a média pessimista
        if neg_m.shape[0] == 1:
            media_neg.append(abs(neg_m[variavel].values[0]))
        elif neg_m.shape[0] == 0:
            media_neg.append(0)
        else:
            media_neg.append(neg_m[variavel].abs().mean())

    return media, media_pos, media_neg

def grafico_erro_asset(df_empreendimento, variavel, meses):
    """Função que retorna a figura do gráfico.

    Args:
        df_empreendimento (DataFrame): dataframe com os erros empilhados
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        meses (list): lista com os indicadores de meses

    Returns:
        fig (object): retorna a figura do gráfico
    """

    # Filtra o dataframe de erros apenas com os indicadores e a variável
    df_filtrado = df_empreendimento[['INDICADORES', variavel]]

    # Chama a função que gera as médias
    m, p, n = erro_medio_positivo_negativo(df_filtrado, variavel, meses)

    df = pd.DataFrame(None)

    df['Indicadores'] = meses
    df['Erro inferior'] = np.array(n) * 100
    df['Erro médio'] = np.array(m) * 100
    df['Erro superior'] = np.array(p) * 100

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=df['Indicadores'], 
        y=df['Erro superior'].round(3),
        name='Erro otimista',
        marker={'color': '#14B5BD', 'size': 8, 'symbol': 'x'},
        showlegend=True
    ))

    fig.add_trace(go.Scatter(
        x=df['Indicadores'], 
        y=df['Erro médio'].round(3), 
        name='Erro médio',
        marker={'color': '#242e48', 'size': 8, 'symbol': 'circle'},
        showlegend=True
    ))

    fig.add_trace(go.Scatter(
        x=df['Indicadores'], 
        y=df['Erro inferior'].round(3), 
        name='Erro pessimista',
        marker={'color': '#E54D8D', 'size': 8, 'symbol': 'square'},
        showlegend=True 
    ))
    
    fig.update_yaxes(showgrid=True, zeroline=False, zerolinewidth=2, rangemode="tozero", color="#242e48", ticksuffix="%")
    fig.update_xaxes(showgrid=False, zeroline=False, zerolinewidth=2, color="#242e48")

    if variavel == "CANCELAMENTO TOTAL" or variavel == "VENDAS ACUMULADAS TOTAIS":
        var = variavel.lower().capitalize()

    elif variavel == "VGV LÍQUIDO ACUMULADO":
        var = variavel[:3] + variavel[3:].lower()
        
    elif variavel == "FLUXO DE CAIXA FII":
        var = variavel[:14].capitalize() + variavel[14:]

    else:
        s = variavel.lower()
        var = s[4].upper() + s[5:]

    if variavel == "CANCELAMENTO TOTAL" or variavel == "VENDAS ACUMULADAS TOTAIS":
        fig.update_layout(
            title={"text":var, "font": {"color": "#242e48", "family": "Inter"}},
            font_family="Inter",
            title_font_family="Inter",
            xaxis_title="Meses",
            autosize=True,
            paper_bgcolor="#DCE3EC",
            plot_bgcolor="#DCE3EC",
            legend={"font": {'color': "#242e48", "family": "Inter"}},
        )
    else:
        fig.update_layout(
            title={"text":var, "font": {"color": "#242e48", "family": "Inter"}},
            font_family="Inter",
            title_font_family="Inter",
            xaxis_title="Meses",
            autosize=True,
            paper_bgcolor="#DCE3EC", #BAC8DB
            plot_bgcolor="#DCE3EC",
            legend={"font": {'color': "#242e48", "family": "Inter"}},
        )
    
    return fig

def empilha_erro_asset(empreendimento):
    """Função que empilha os erros calculados de um empreendimento.

    Args:
        empreendimento (string): nome do empreendimento

    Returns:
        dfs_erro_por_empreendimento (DataFrame): retorna um novo dataframe com os dados empilhados
    """

    lista_pxr = list(erro_pxr[empreendimento]['previstos'].keys())

    dfs_erro_por_empreendimento = erro_pxr[empreendimento]['previstos'][lista_pxr[0]] # Atribui o primeiro pxr

    for i in range(1, len(lista_pxr)):
        dfs_erro_por_empreendimento = pd.concat([dfs_erro_por_empreendimento, erro_pxr[empreendimento]['previstos'][lista_pxr[i]]], axis=0)

    dfs_erro_por_empreendimento = dfs_erro_por_empreendimento.reset_index(drop=True)

    return dfs_erro_por_empreendimento

def opcao_grafico_asset(empreendimento, variavel, value):
    """Função que é chamada em app.py e retorna o gráfico de cada variável. 

    Args:
        empreendimento (string): nome do empreendimento
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        value (int): input de meses fornecido (default: 5)

    Returns:
        fig (object): retorna a figura do gráfico gerado
    """

    erro_por_empr_empilhado = empilha_erro_asset(empreendimento)

    maximo_meses = len(list(erro_por_empr_empilhado['INDICADORES'].unique()))

    if value > maximo_meses:
        meses = [f'M{i}' for i in range(1, maximo_meses+1)]

    elif value <= maximo_meses:
        meses = [f'M{i}' for i in range(1, value+1)]
    
    return grafico_erro_asset(erro_por_empr_empilhado, variavel, meses)

def alerta_ativo(empreendimento, value):
    """Função que gera um alerta caso o empreendimento não corresponda com a quantidade de meses informado.

    Args:
        empreendimento (string): nome do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        alerta (bool): retorna um valor booleano indicando o alerta
    """
    
    ativo = empilha_erro_asset(empreendimento)

    maximo_meses = len(list(ativo.INDICADORES.unique()))

    if value > maximo_meses:
        alerta = True
    elif value <= maximo_meses:
        alerta = False

    return alerta