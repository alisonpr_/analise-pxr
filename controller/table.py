import pandas as pd
from model.read_data import n_cols, lista_emp, tipologias
from model.read_data import query_dfs, erro_pxr
from utils.constantes import alerta
import dash_table as dt
from dash.dash_table import FormatTemplate
import numpy as np
import warnings
warnings.filterwarnings("ignore")

dfs = query_dfs()

# Dicionário para as colunas do dataframe de empreendimentos
columns1 = {"Empreendimento": "Empreendimento", "Vendas Acumuladas Totais": "Vendas Acumuladas Totais (%)", "Cancelamento Total": "Cancelamento Total (%)",
            "VGV Líquido Acumulado": "VGV Líquido Acumulado (%)", "Fluxo de Caixa FII": "Fluxo de Caixa FII (%)", "Receita Bruta Operacional": "Receita Bruta Operacional (%)",
            "Custos Operacionais": "Custos Operacionais (%)", "Despesas Operacionais": "Despesas Operacionais (%)",
            "Fluxo de Caixa Operacional Ajustado": "Fluxo de Caixa Operacional Ajustado (%)"}

# Dicionário para as colunas do dataframe de tipologias
columns2 = {"Tipologia": "Tipologias", "Vendas Acumuladas Totais": "Vendas Acumuladas Totais (%)", "Cancelamento Total": "Cancelamento Total (%)",
            "VGV Líquido Acumulado": "VGV Líquido Acumulado (%)", "Fluxo de Caixa FII": "Fluxo de Caixa FII (%)", "Receita Bruta Operacional": "Receita Bruta Operacional (%)",
            "Custos Operacionais": "Custos Operacionais (%)", "Despesas Operacionais": "Despesas Operacionais (%)",
            "Fluxo de Caixa Operacional Ajustado": "Fluxo de Caixa Operacional Ajustado (%)"}

# Objeto que coloca os valores em percentual com duas casas decimais
percentage = FormatTemplate.percentage(2)

def dicionarios(opcao_analise):
    """Função que retorna o dicionário para construir o dataframe por ativos ou tipologia.

    Args:
        opcao_analise (string): opção de análise, se é por empreendimentos ou tipologia

    Returns:
        dicionario (dict): retorna o dicionário baseado na opção de análise
        lista_tipologias (list): retorna a lista de tipologias
    """

    lista_tipologias = dfs.NATUREZA.unique().tolist()
    lista_tipologias.insert(0, "Todos")

    if opcao_analise == "empreendimentos":
        dicionario = {
            opcao_analise: [],
            "tip": [],

            "vatOtimista": [],
            "vatGeral": [],
            "vatPessimista": [],

            "ctOtimista": [],
            "ctGeral": [],
            "ctPessimista": [],

            "vgvlaOtimista": [],
            "vgvlaGeral": [],
            "vgvlaPessimista": [],

            "rboOtimista": [],
            "rboGeral": [],
            "rboPessimista": [],

            "coOtimista": [],
            "coGeral": [],
            "coPessimista": [],

            "doOtimista": [],
            "doGeral": [],
            "doPessimista": [],

            "fcoaOtimista": [],
            "fcoaGeral": [],
            "fcoaPessimista": [],

            "fcfiiOtimista": [],
            "fcfiiGeral": [],
            "fcfiiPessimista": [],
        }
    else:
        dicionario = {
            opcao_analise: [],

            "vatOtimista": [],
            "vatGeral": [],
            "vatPessimista": [],

            "ctOtimista": [],
            "ctGeral": [],
            "ctPessimista": [],

            "vgvlaOtimista": [],
            "vgvlaGeral": [],
            "vgvlaPessimista": [],

            "rboOtimista": [],
            "rboGeral": [],
            "rboPessimista": [],

            "coOtimista": [],
            "coGeral": [],
            "coPessimista": [],

            "doOtimista": [],
            "doGeral": [],
            "doPessimista": [],

            "fcoaOtimista": [],
            "fcoaGeral": [],
            "fcoaPessimista": [],

            "fcfiiOtimista": [],
            "fcfiiGeral": [],
            "fcfiiPessimista": [],
        }

    return dicionario, lista_tipologias

def filtra_tipologia(tipologia, dfs):
    """Função que filtra o dataframe geral por tipologia.

    Args:
        tipologia (string): nome da tipologia, por exemplo: Loteamento aberto
        dfs (DataFrame): dataframe empilhado com todos os empreendimentos

    Returns:
        df (DataFrame): retorna o dataframe filtrado pela tipologia
    """

    df = dfs[dfs['NATUREZA'] == tipologia]
    
    return df

def separa_erros(df_filtrado, variavel, meses):
    """Função que faz as médias do erros pessimista, otimista e geral.

    Args:
        df_filtrado (DataFrame): dataframe filtrado de acordo com cada M_i
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        meses (list): lista com os indicadores de meses: M1, M2,..., M_n

    Returns:
        media (list): retorna uma lista com as médias de cada M_i
        media_pos (list): retorna uma lista com as médias otimistas de cada M_i
        media_neg (list): retorna uma lista com as médias pessimistas de cada M_i
    """

    ind_geral, ind_pessimista, ind_otimista = [], [], []

    for m in meses:
        df_m = df_filtrado[df_filtrado['INDICADORES'] == m]
       
        if df_m.shape[0] == 1:
            ind_geral.append(abs(df_m[variavel].values[0]))
        elif df_m.shape[0] == 0:
            ind_geral.append(0)
        else:
            ind_geral.append(df_m[variavel].abs().mean())

        dfp = df_m[df_m[variavel] < 0] # Pessimista
        dfo = df_m[df_m[variavel] > 0] # Otimista

        # Pessimista
        if dfp.shape[0] == 1:
            ind_pessimista.append(abs(dfp[variavel].values[0]))
        elif dfp.shape[0] == 0:
            ind_pessimista.append(0)
        else:
            ind_pessimista.append(dfp[variavel].abs().mean())

        # Otimista
        if dfo.shape[0] == 1:
            ind_otimista.append(abs(dfo[variavel].values[0]))
        elif dfo.shape[0] == 0:
            ind_otimista.append(0)
        else:
            ind_otimista.append(dfo[variavel].abs().mean())

    return ind_geral, ind_pessimista, ind_otimista

def indicador_de_erros(df, tipo_indicador, size):
    """Função que calcula os indicadores de erros.

    Args:
        df (DataFrame): dataframe do indicador
        tipo_indicador (string): indica se é o indicador geral, pessimista ou otimista
        size (int): quantidade de M_i

    Returns:
        indicador (float): retorna o valor do indicador calculado
    """

    pesos = []
    k = size
    dados = list(df[tipo_indicador].values)

    indicador = 0
    for i in range(k):
        pesos.append(k-i)
        indicador += dados[i] * (k-i)

    indicador = indicador / sum(pesos)

    return indicador

def gera_indicador(df_tipologia, variavel, meses):
    """Função que gera o indicador geral para as tabelas.

    Args:
        df_tipologia (DataFrame): dataframe filtrado pela tipologia
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        meses (list): lista com os M1, M2,..., M_n

    Returns:
        indicador_erro_geral (float): retorna o valor do indicador geral
        indicador_erro_pessimista (float): retorna o valor do indicador pessimista
        indicador_erro_otimista (float): retorna o valor do indicador otimista
    """

    df_filtrado = df_tipologia[['INDICADORES', variavel]]

    ind_geral, ind_pessimista, ind_otimista = separa_erros(df_filtrado, variavel, meses)

    df = pd.DataFrame(None)

    df['Indicadores'] = meses
    df['Indicadores geral'] = np.array(ind_geral) * 100
    df['Indicadores pessimista'] = np.array(ind_pessimista) * 100
    df['Indicadores otimista'] = np.array(ind_otimista) * 100

    size = len(meses)

    indicador_erro_geral = indicador_de_erros(df, 'Indicadores geral', size)
    indicador_erro_pessimista = indicador_de_erros(df, 'Indicadores pessimista', size)
    indicador_erro_otimista = indicador_de_erros(df, 'Indicadores otimista', size)

    return indicador_erro_geral, indicador_erro_pessimista, indicador_erro_otimista

def empilha_erro_asset(empreendimento, value):
    """Função que empilha os erros de um empreendimento.

    Args:
        empreendimento (string): nome do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        ativo (DataFrame): retorna o dataframe empilhado de um empreendimento
        meses (list): retorna a lista dos M_i
    """

    lista_pxr = list(erro_pxr[empreendimento]['previstos'].keys())

    ativo = erro_pxr[empreendimento]['previstos'][lista_pxr[0]] # Atribui o primeiro pxr

    for i in range(1, len(lista_pxr)):
        ativo = pd.concat([ativo, erro_pxr[empreendimento]['previstos'][lista_pxr[i]]], axis=0)

    ativo = ativo.reset_index(drop=True)

    maximo_meses = len(list(ativo['INDICADORES'].unique()))

    if value > maximo_meses:
        meses = [f'M{i}' for i in range(1, maximo_meses+1)]

    elif value <= maximo_meses:
        meses = [f'M{i}' for i in range(1, value+1)]
    
    return ativo, meses

def replace(x):
    """Função que arredonda valores.

    Args:
        x (float): valores

    Returns:
        x (float): valores arredondados
    """

    return round(x, 2)

def colunas(x, y, flag):
    """Função que cria as colunas das tabelas de acordo com a flag.

    Args:
        x (string): nome da coluna, por exemplo: empreendimentos ou tipologias
        y (string): id da coluna
        flag (bool): valor booleano que indica se é empreendimento ou tipologia

    Returns:
        colunas1 (dict): retorna o dicionário
    """

    if flag:
        colunas1 = [
            {"name": ["", x], "id": y},
            {"name": [" ", "Tipologias"], "id": "tip"},
            
            {"name": ["Vendas Acumuladas Totais", "Otimista"], "id": "vatOtimista", "type": "numeric", "format": percentage},
            {"name": ["Vendas Acumuladas Totais", "Geral"], "id": "vatGeral", "type": "numeric", "format": percentage},
            {"name": ["Vendas Acumuladas Totais", "Pessimista"], "id": "vatPessimista", "type": "numeric", "format": percentage},

            {"name": ["Cancelamento Total", "Otimista"], "id": "ctOtimista", "type": "numeric", "format": percentage},
            {"name": ["Cancelamento Total", "Geral"], "id": "ctGeral", "type": "numeric", "format": percentage},
            {"name": ["Cancelamento Total", "Pessimista"], "id": "ctPessimista", "type": "numeric", "format": percentage},

            {"name": ["VGV Líquido Acumulado", "Otimista"], "id": "vgvlaOtimista", "type": "numeric", "format": percentage},
            {"name": ["VGV Líquido Acumulado", "Geral"], "id": "vgvlaGeral", "type": "numeric", "format": percentage},
            {"name": ["VGV Líquido Acumulado", "Pessimista"], "id": "vgvlaPessimista", "type": "numeric", "format": percentage},

            {"name": ["Receita Bruta Operacional", "Otimista"], "id": "rboOtimista", "type": "numeric", "format": percentage},
            {"name": ["Receita Bruta Operacional", "Geral"], "id": "rboGeral", "type": "numeric", "format": percentage},
            {"name": ["Receita Bruta Operacional", "Pessimista"], "id": "rboPessimista", "type": "numeric", "format": percentage},

            {"name": ["Custos Operacionais", "Otimista"], "id": "coOtimista", "type": "numeric", "format": percentage},
            {"name": ["Custos Operacionais", "Geral"], "id": "coGeral", "type": "numeric", "format": percentage},
            {"name": ["Custos Operacionais", "Pessimista"], "id": "coPessimista", "type": "numeric", "format": percentage},

            {"name": ["Despesas Operacionais", "Otimista"], "id": "doOtimista", "type": "numeric", "format": percentage},
            {"name": ["Despesas Operacionais", "Geral"], "id": "doGeral", "type": "numeric", "format": percentage},
            {"name": ["Despesas Operacionais", "Pessimista"], "id": "doPessimista", "type": "numeric", "format": percentage},

            {"name": ["Fluxo de Caixa Operacional Ajustado", "Otimista"], "id": "fcoaOtimista", "type": "numeric", "format": percentage},
            {"name": ["Fluxo de Caixa Operacional Ajustado", "Geral"], "id": "fcoaGeral", "type": "numeric", "format": percentage},
            {"name": ["Fluxo de Caixa Operacional Ajustado", "Pessimista"], "id": "fcoaPessimista", "type": "numeric", "format": percentage},

            {"name": ["Fluxo de Caixa FII", "Otimista"], "id": "fcfiiOtimista", "type": "numeric", "format": percentage},
            {"name": ["Fluxo de Caixa FII", "Geral"], "id": "fcfiiGeral", "type": "numeric", "format": percentage},
            {"name": ["Fluxo de Caixa FII", "Pessimista"], "id": "fcfiiPessimista", "type": "numeric", "format": percentage},
        ]
    else:
        colunas1 = [
            {"name": ["", x], "id": y},

            {"name": ["Vendas Acumuladas Totais", "Otimista"], "id": "vatOtimista", "type": "numeric", "format": percentage},
            {"name": ["Vendas Acumuladas Totais", "Geral"], "id": "vatGeral", "type": "numeric", "format": percentage},
            {"name": ["Vendas Acumuladas Totais", "Pessimista"], "id": "vatPessimista", "type": "numeric", "format": percentage},

            {"name": ["Cancelamento Total", "Otimista"], "id": "ctOtimista", "type": "numeric", "format": percentage},
            {"name": ["Cancelamento Total", "Geral"], "id": "ctGeral", "type": "numeric", "format": percentage},
            {"name": ["Cancelamento Total", "Pessimista"], "id": "ctPessimista", "type": "numeric", "format": percentage},

            {"name": ["VGV Líquido Acumulado", "Otimista"], "id": "vgvlaOtimista", "type": "numeric", "format": percentage},
            {"name": ["VGV Líquido Acumulado", "Geral"], "id": "vgvlaGeral", "type": "numeric", "format": percentage},
            {"name": ["VGV Líquido Acumulado", "Pessimista"], "id": "vgvlaPessimista", "type": "numeric", "format": percentage},

            {"name": ["Receita Bruta Operacional", "Otimista"], "id": "rboOtimista", "type": "numeric", "format": percentage},
            {"name": ["Receita Bruta Operacional", "Geral"], "id": "rboGeral", "type": "numeric", "format": percentage},
            {"name": ["Receita Bruta Operacional", "Pessimista"], "id": "rboPessimista", "type": "numeric", "format": percentage},

            {"name": ["Custos Operacionais", "Otimista"], "id": "coOtimista", "type": "numeric", "format": percentage},
            {"name": ["Custos Operacionais", "Geral"], "id": "coGeral", "type": "numeric", "format": percentage},
            {"name": ["Custos Operacionais", "Pessimista"], "id": "coPessimista", "type": "numeric", "format": percentage},

            {"name": ["Despesas Operacionais", "Otimista"], "id": "doOtimista", "type": "numeric", "format": percentage},
            {"name": ["Despesas Operacionais", "Geral"], "id": "doGeral", "type": "numeric", "format": percentage},
            {"name": ["Despesas Operacionais", "Pessimista"], "id": "doPessimista", "type": "numeric", "format": percentage},

            {"name": ["Fluxo de Caixa Operacional Ajustado", "Otimista"], "id": "fcoaOtimista", "type": "numeric", "format": percentage},
            {"name": ["Fluxo de Caixa Operacional Ajustado", "Geral"], "id": "fcoaGeral", "type": "numeric", "format": percentage},
            {"name": ["Fluxo de Caixa Operacional Ajustado", "Pessimista"], "id": "fcoaPessimista", "type": "numeric", "format": percentage},

            {"name": ["Fluxo de Caixa FII", "Otimista"], "id": "fcfiiOtimista", "type": "numeric", "format": percentage},
            {"name": ["Fluxo de Caixa FII", "Geral"], "id": "fcfiiGeral", "type": "numeric", "format": percentage},
            {"name": ["Fluxo de Caixa FII", "Pessimista"], "id": "fcfiiPessimista", "type": "numeric", "format": percentage},
        ]

    return colunas1

def estilo_dos_dados(data, slice):
    """Função que gera o estilo dos dados, colando negrito no valor máximo.

    Args:
        data (DataFrame): tabela
        slice (int): slice que seleciona apenas algumas colunas

    Returns:
        estilo: retorna um dicionário de estilo
    """

    estilo = []

    x = {
        'if': {'row_index': 'odd'},
        'backgroundColor': '#DCE3EC'
    }

    estilo.append(x)

    for col in list(data.columns)[slice:]:
        maximo = str(data[col].max())
        x = {
                'if': {
                    'column_id': col,
                    'filter_query': f'{{{col}}} = {maximo}'
                },
                'fontWeight': 'bold'
            }
        estilo.append(x)

    return estilo

def return_table(value, tipologia):
    """Função que retorna a tabela de indicadores de erros por empreendimento.

    Args:
        value (int): input de meses (default: 5)
        tipologia (string): nome da tipologia, por exemplo: Loteamento aberto

    Returns:
        table (DataFrame): retorna a tabela configurada
    """

    dict_ativos, _ = dicionarios("empreendimentos")
    lista = []

    for emp in lista_emp:
        df, meses = empilha_erro_asset(emp, value)
        
        maximo = len(list(df['INDICADORES'].unique()))

        if value <= maximo:
            meses = [f"M{i}" for i in range(1, value+1)]
        elif value > maximo:
            meses = [f"M{i}" for i in range(1, maximo+1)]
            
            if emp in alerta.list_ativos:
                lista.append(emp)

            if value <= alerta.anterior:
                for ativo in lista:
                    if ativo in alerta.list_ativos:
                        alerta.list_ativos.remove(ativo)
            
            elif value > alerta.anterior:
                if emp not in alerta.list_ativos:
                    alerta.list_ativos.append(emp)
                    alerta.aux_ativos = emp
                    alerta.lista_aux += ', ' + emp.upper() 
                
                alerta.anterior = value-1

            continue

        tip = df['NATUREZA'].unique()[0]
        dict_ativos['empreendimentos'].append(emp)
        dict_ativos['tip'].append(tip)
        
        for i in range(len(n_cols)):
            ind1, ind2, ind3 = gera_indicador(df, n_cols[i], meses)
            ind1 = ind1/100
            ind2 = ind2/100
            ind3 = ind3/100

            if n_cols[i] == 'VENDAS ACUMULADAS TOTAIS':
                dict_ativos["vatOtimista"].append(ind3)
                dict_ativos["vatGeral"].append(ind1)
                dict_ativos["vatPessimista"].append(ind2)
            elif n_cols[i] == 'CANCELAMENTO TOTAL':
                dict_ativos["ctOtimista"].append(ind3)
                dict_ativos["ctGeral"].append(ind1)
                dict_ativos["ctPessimista"].append(ind2)
            elif n_cols[i] == 'VGV LÍQUIDO ACUMULADO':
                dict_ativos["vgvlaOtimista"].append(ind3)
                dict_ativos["vgvlaGeral"].append(ind1)
                dict_ativos["vgvlaPessimista"].append(ind2)
            elif n_cols[i] == 'FLUXO DE CAIXA FII':
                dict_ativos["fcfiiOtimista"].append(ind3)
                dict_ativos["fcfiiGeral"].append(ind1)
                dict_ativos["fcfiiPessimista"].append(ind2)
            elif n_cols[i] == '(+) RECEITA BRUTA OPERACIONAL':
                dict_ativos["rboOtimista"].append(ind3)
                dict_ativos["rboGeral"].append(ind1)
                dict_ativos["rboPessimista"].append(ind2)
            elif n_cols[i] == '(=) CUSTOS OPERACIONAIS':
                dict_ativos["coOtimista"].append(ind3)
                dict_ativos["coGeral"].append(ind1)
                dict_ativos["coPessimista"].append(ind2)
            elif n_cols[i] == '(=) DESPESAS OPERACIONAIS':
                dict_ativos["doOtimista"].append(ind3)
                dict_ativos["doGeral"].append(ind1)
                dict_ativos["doPessimista"].append(ind2)
            elif n_cols[i] == '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO':
                dict_ativos["fcoaOtimista"].append(ind3)
                dict_ativos["fcoaGeral"].append(ind1)
                dict_ativos["fcoaPessimista"].append(ind2)

    data = pd.DataFrame(dict_ativos)

    if lista != []:
        for ativo in lista:
            if ativo in list(dict_ativos['empreendimentos']):
                alerta.list_ativos.remove(ativo)

    data.fillna(0.00, inplace=True)

    if tipologia != "Todos":
        data = data[data['tip'] == tipologia]

    cols = colunas("Empreendimentos", "empreendimentos", True)

    data = data.sort_values(by='empreendimentos', ascending=True).reset_index(drop=True)
    
    estilo = estilo_dos_dados(data, 2)

    table = dt.DataTable(
        id='tbl1',
        data=data.to_dict('records'),
        columns=cols,

        style_as_list_view=False, # Habilita as linhas verticais da tabela
        
        style_table={'maxHeight': '1000px', 'maxWidth': '700px', 'minWidth': '100%', 'overflowY': 'auto', 'overflowX': 'auto', "box-shadow": "6px 6px 6px #C6CCD4"}, # Ajusta a altura da tabela e a barra de rolagem
        
        page_action='none',
        
        sort_action='native',
        
        fixed_rows={'headers': True}, # Fixa o cabeçalho
        
        fixed_columns={'headers': True, 'data': 2}, # Fixa a coluna 1
        
        style_cell={
            'textAlign': 'center',
            'font-family': 'Inter',
            'height': 'auto',
            'minWidth': '100px', 'width': '100px', 'maxWidth': '500px'
        },

        style_cell_conditional=[
            {
                'if': {'column_id': 'empreendimentos'},
                'textAlign': 'left',
                'width': '280px'
            },
            {
                'if': {'column_id': 'tip'},
                'textAlign': 'left',
                'width': 'auto'
            }
        ],

        style_data={
            'color': 'black',
            'backgroundColor': '#FFFFFF', #54698A
            'height': 'auto',
            'border': '1px solid white',
            'font-family': 'Inter'
        },
        
        style_data_conditional=estilo,

        style_header={
            'backgroundColor': '#DCE3EC',
            'color': 'black',
            'width': '80',
            'fontWeight': 'bold',
            'font-family': 'Inter',
            'border': '1px solid white',
        },

        tooltip_header={
            'vatOtimista': ['', f'Exemplo: \
                A previsão de Vendas foi de, no máximo, {data.vatOtimista.max()*100:.2f}% acima do realizado.'],
            
            'vatGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Vendas é de, no máximo, {data.vatGeral.max()*100:.2f}%.'],

            'vatPessimista': ['', f'Exemplo: \
                A previsão de Vendas foi de, no máximo, {data.vatPessimista.max()*100:.2f}% abaixo do realizado.'],
        
            'ctOtimista': ['', f'Exemplo: \
                A previsão de Cancelamentos foi de, no máximo, {data.ctOtimista.max()*100:.2f}% acima do realizado.'],
            
            'ctGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Cancelamentos é de, no máximo, {data.ctGeral.max()*100:.2f}%.'],

            'ctPessimista': ['', f'Exemplo: \
                A previsão de Cancelamentos foi de, no máximo, {data.ctPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'vgvlaOtimista': ['', f'Exemplo: \
                A previsão do VGV Líquido Acumulado foi de, no máximo, {data.vgvlaOtimista.max()*100:.2f}% acima do realizado.'],
            
            'vgvlaGeral': ['', f'Exemplo: \
                Em média, o erro de previsão do VGV Líquido Acumulado é de, no máximo, {data.vgvlaGeral.max()*100:.2f}%.'],

            'vgvlaPessimista': ['', f'Exemplo: \
                A previsão do VGV Líquido Acumulado foi de, no máximo, {data.vgvlaPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'rboOtimista': ['', f'Exemplo: \
                A previsão de Receita Bruta Operacional foi de, no máximo, {data.rboOtimista.max()*100:.2f}% acima do realizado.'],
            
            'rboGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Receita Bruta Operacional é de, no máximo, {data.rboGeral.max()*100:.2f}%.'],

            'rboPessimista': ['', f'Exemplo: \
                A previsão de Receita Bruta Operacional foi de, no máximo, {data.rboPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'coOtimista': ['', f'Exemplo: \
                A previsão de Custos Operacionais foi de, no máximo, {data.coOtimista.max()*100:.2f}% acima do realizado.'],
            
            'coGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Custos Operacionais é de, no máximo, {data.coGeral.max()*100:.2f}%.'],

            'coPessimista': ['', f'Exemplo: \
                A previsão de Custos Operacionais foi de, no máximo, {data.coPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'doOtimista': ['', f'Exemplo: \
                A previsão de Despesas Operacionals foi de, no máximo, {data.doOtimista.max()*100:.2f}% acima do realizado.'],
            
            'doGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Despesas Operacionais é de, no máximo, {data.doGeral.max()*100:.2f}%.'],

            'doPessimista': ['', f'Exemplo: \
                A previsão de Despesas Operacionais foi de, no máximo, {data.doPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'fcoaOtimista': ['', f'Exemplo: \
                A previsão do Fluxo de Caixa Operacional Ajustado foi de, no máximo, {data.fcoaOtimista.max()*100:.2f}% acima do realizado.'],
            
            'fcoaGeral': ['', f'Exemplo: \
                Em média, o erro de previsão do Fluxo de Caixa Operacional Ajustado é de, no máximo, {data.fcoaGeral.max()*100:.2f}%.'],

            'fcoaPessimista': ['', f'Exemplo: \
                A previsão do Fluxo de Caixa Operacional Ajustado foi de, no máximo, {data.fcoaPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'fcfiiOtimista': ['', f'Exemplo: \
                A previsão do Fluxo de Caixa FII foi de, no máximo, {data.fcfiiOtimista.max()*100:.2f}% acima do realizado.'],
            
            'fcfiiGeral': ['', f'Exemplo: \
                Em média, o erro de previsão do Fluxo de Caixa FII é de, no máximo, {data.fcfiiGeral.max()*100:.2f}%.'],

            'fcfiiPessimista': ['', f'Exemplo: \
                A previsão do Fluxo de Caixa FII foi de, no máximo, {data.fcfiiPessimista.max()*100:.2f}% abaixo do realizado.'],
        },

        css=[
            {
                'selector': '.dash-table-tooltip',
                'rule': 'background-color: #242e48; font-family: Inter; color: #FFFFFF; fontWeight: bold'
            }
        ],

        tooltip_delay=0,
        tooltip_duration=None,

        fill_width=False,

        merge_duplicate_headers=True
    )

    return table

def return_table_typololy(value):
    """Função que retorna a tabela de indicadores de erros por tipologia.

    Args:
        value (int): input de meses (default: 5)

    Returns:
        table (DataFrame): retorna a tabela configurada
    """

    dict_tipologia, lista_tipologias = dicionarios("tipologias")

    for tipologia in lista_tipologias:
        if tipologia == "Todos":
            df = dfs.copy()
            maximo = len(list(df['INDICADORES'].unique()))
        else:
            df = filtra_tipologia(tipologia, dfs)
            maximo = len(list(df['INDICADORES'].unique()))
        
        if value <= maximo:
            meses = [f"M{i}" for i in range(1, value+1)]
        elif value > maximo:
            meses = [f"M{i}" for i in range(1, maximo+1)]
            
            if tipologia not in alerta.list_tipologia:    
                alerta.list_tipologia.append(tipologia)
                alerta.aux_tipologia = tipologia

            continue

        dict_tipologia['tipologias'].append(tipologia)

        for i in range(len(n_cols)):
            ind1, ind2, ind3 = gera_indicador(df, n_cols[i], meses)
            ind1 = ind1/100
            ind2 = ind2/100
            ind3 = ind3/100

            if n_cols[i] == 'VENDAS ACUMULADAS TOTAIS':
                dict_tipologia["vatOtimista"].append(ind3)
                dict_tipologia["vatGeral"].append(ind1)
                dict_tipologia["vatPessimista"].append(ind2)
            elif n_cols[i] == 'CANCELAMENTO TOTAL':
                dict_tipologia["ctOtimista"].append(ind3)
                dict_tipologia["ctGeral"].append(ind1)
                dict_tipologia["ctPessimista"].append(ind2)
            elif n_cols[i] == 'VGV LÍQUIDO ACUMULADO':
                dict_tipologia["vgvlaOtimista"].append(ind3)
                dict_tipologia["vgvlaGeral"].append(ind1)
                dict_tipologia["vgvlaPessimista"].append(ind2)
            elif n_cols[i] == 'FLUXO DE CAIXA FII':
                dict_tipologia["fcfiiOtimista"].append(ind3)
                dict_tipologia["fcfiiGeral"].append(ind1)
                dict_tipologia["fcfiiPessimista"].append(ind2)
            elif n_cols[i] == '(+) RECEITA BRUTA OPERACIONAL':
                dict_tipologia["rboOtimista"].append(ind3)
                dict_tipologia["rboGeral"].append(ind1)
                dict_tipologia["rboPessimista"].append(ind2)
            elif n_cols[i] == '(=) CUSTOS OPERACIONAIS':
                dict_tipologia["coOtimista"].append(ind3)
                dict_tipologia["coGeral"].append(ind1)
                dict_tipologia["coPessimista"].append(ind2)
            elif n_cols[i] == '(=) DESPESAS OPERACIONAIS':
                dict_tipologia["doOtimista"].append(ind3)
                dict_tipologia["doGeral"].append(ind1)
                dict_tipologia["doPessimista"].append(ind2)
            elif n_cols[i] == '(=) FLUXO DE CAIXA OPERACIONAL AJUSTADO':
                dict_tipologia["fcoaOtimista"].append(ind3)
                dict_tipologia["fcoaGeral"].append(ind1)
                dict_tipologia["fcoaPessimista"].append(ind2)

    data = pd.DataFrame(dict_tipologia)

    if alerta.list_tipologia != []:
        for tip in alerta.list_tipologia:
            if tip in list(dict_tipologia['tipologias']):
                alerta.list_tipologia.remove(tip)

    data.fillna(0.00, inplace=True)

    cols = colunas("Tipologias", "tipologias", False)

    data = data.sort_values(by='tipologias', ascending=True).reset_index(drop=True)
    
    estilo = estilo_dos_dados(data, 1)
    
    table = dt.DataTable(
        id='tbl2',
        
        data=data.to_dict('records'),
        
        columns=cols,

        style_as_list_view=False, # Habilita as linhas verticais da tabela

        style_table={'maxHeight': '700px', 'maxWidth': '700px', 'minWidth': '100%', 'overflowY': 'auto', 'overflowX': 'auto', "box-shadow": "6px 6px 6px #C6CCD4"}, # Ajusta a altura da tabela e a barra de rolagem

        page_action='none',

        sort_action='native',

        #fixed_rows={'headers': True}, # Fixa o cabeçalho

        fixed_columns={'headers': True, 'data': 1}, # Fixa a coluna 1

        style_cell={
            'textAlign': 'center',
            'font-family': 'Inter',
            'height': 'auto',
            'minWidth': '110px', 'width': '110px', 'maxWidth': '260px'
        },
        style_cell_conditional=[
            {
                'if': {'column_id': 'tipologias'},
                'textAlign': 'left',
                'width': 'auto'
            }
        ],

        style_data={
            'color': 'black',
            'backgroundColor': '#FFFFFF', #54698A
            'height': 'auto',
            'border': '1px solid white',
            'font-family': 'Inter'
        },
    
        style_data_conditional=estilo,

        style_header={
            'backgroundColor': '#DCE3EC',
            'color': 'black',
            'width': '80',
            'fontWeight': 'bold',
            'font-family': 'Inter',
            'border': '1px solid white',
        },

        tooltip_header={
            'vatOtimista': ['', f'Exemplo: \
                A previsão de Vendas foi de, no máximo, {data.vatOtimista.max()*100:.2f}% acima do realizado.'],
            
            'vatGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Vendas é de, no máximo, {data.vatGeral.max()*100:.2f}%.'],

            'vatPessimista': ['', f'Exemplo: \
                A previsão de Vendas foi de, no máximo, {data.vatPessimista.max()*100:.2f}% abaixo do realizado.'],
        
            'ctOtimista': ['', f'Exemplo: \
                A previsão de Cancelamentos foi de, no máximo, {data.ctOtimista.max()*100:.2f}% acima do realizado.'],
            
            'ctGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Cancelamentos é de, no máximo, {data.ctGeral.max()*100:.2f}%.'],

            'ctPessimista': ['', f'Exemplo: \
                A previsão de Cancelamentos foi de, no máximo, {data.ctPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'vgvlaOtimista': ['', f'Exemplo: \
                A previsão do VGV Líquido Acumulado foi de, no máximo, {data.vgvlaOtimista.max()*100:.2f}% acima do realizado.'],
            
            'vgvlaGeral': ['', f'Exemplo: \
                Em média, o erro de previsão do VGV Líquido Acumulado é de, no máximo, {data.vgvlaGeral.max()*100:.2f}%.'],

            'vgvlaPessimista': ['', f'Exemplo: \
                A previsão do VGV Líquido Acumulado foi de, no máximo, {data.vgvlaPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'rboOtimista': ['', f'Exemplo: \
                A previsão de Receita Bruta Operacional foi de, no máximo, {data.rboOtimista.max()*100:.2f}% acima do realizado.'],
            
            'rboGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Receita Bruta Operacional é de, no máximo, {data.rboGeral.max()*100:.2f}%.'],

            'rboPessimista': ['', f'Exemplo: \
                A previsão de Receita Bruta Operacional foi de, no máximo, {data.rboPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'coOtimista': ['', f'Exemplo: \
                A previsão de Custos Operacionais foi de, no máximo, {data.coOtimista.max()*100:.2f}% acima do realizado.'],
            
            'coGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Custos Operacionais é de, no máximo, {data.coGeral.max()*100:.2f}%.'],

            'coPessimista': ['', f'Exemplo: \
                A previsão de Custos Operacionais foi de, no máximo, {data.coPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'doOtimista': ['', f'Exemplo: \
                A previsão de Despesas Operacionals foi de, no máximo, {data.doOtimista.max()*100:.2f}% acima do realizado.'],
            
            'doGeral': ['', f'Exemplo: \
                Em média, o erro de previsão de Despesas Operacionais é de, no máximo, {data.doGeral.max()*100:.2f}%.'],

            'doPessimista': ['', f'Exemplo: \
                A previsão de Despesas Operacionais foi de, no máximo, {data.doPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'fcoaOtimista': ['', f'Exemplo: \
                A previsão do Fluxo de Caixa Operacional Ajustado foi de, no máximo, {data.fcoaOtimista.max()*100:.2f}% acima do realizado.'],
            
            'fcoaGeral': ['', f'Exemplo: \
                Em média, o erro de previsão do Fluxo de Caixa Operacional Ajustado é de, no máximo, {data.fcoaGeral.max()*100:.2f}%.'],

            'fcoaPessimista': ['', f'Exemplo: \
                A previsão do Fluxo de Caixa Operacional Ajustado foi de, no máximo, {data.fcoaPessimista.max()*100:.2f}% abaixo do realizado.'],
            
            'fcfiiOtimista': ['', f'Exemplo: \
                A previsão do Fluxo de Caixa FII foi de, no máximo, {data.fcfiiOtimista.max()*100:.2f}% acima do realizado.'],
            
            'fcfiiGeral': ['', f'Exemplo: \
                Em média, o erro de previsão do Fluxo de Caixa FII é de, no máximo, {data.fcfiiGeral.max()*100:.2f}%.'],

            'fcfiiPessimista': ['', f'Exemplo: \
                A previsão do Fluxo de Caixa FII foi de, no máximo, {data.fcfiiPessimista.max()*100:.2f}% abaixo do realizado.'],
        },

        css=[
            {
                'selector': '.dash-table-tooltip',
                'rule': 'background-color: #242e48; font-family: Inter; color: #FFFFFF; fontWeight: bold'
            }
        ],

        tooltip_delay=0,
        tooltip_duration=None,

        fill_width=False,

        merge_duplicate_headers=True
    )

    return table