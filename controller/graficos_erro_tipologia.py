import plotly.graph_objects as go
import pandas as pd
from model.read_data import *
import numpy as np
import warnings
warnings.filterwarnings("ignore")

pd.set_option('display.max_columns', None)

dfs = query_dfs()

def erro_medio_positivo_negativo(df_filtrado, variavel, meses):
    """Função que faz as médias do erros pessimista, otimista e geral.

    Args:
        df_filtrado (DataFrame): dataframe filtrado de acordo com cada M_i
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        meses (list): lista com os indicadores de meses: M1, M2,..., M_n

    Returns:
        media (list): retorna uma lista com as médias de cada M_i
        media_pos (list): retorna uma lista com as médias otimistas de cada M_i
        media_neg (list): retorna uma lista com as médias pessimistas de cada M_i
    """

    media, media_pos, media_neg = [], [], []

    for m in meses:
        df_m = df_filtrado[df_filtrado['INDICADORES'] == m]

        if df_m.shape[0] == 1:
            media.append(abs(df_m[variavel].values[0]))
        elif df_m.shape[0] == 0:
            media.append(0)
        else:
            media.append(df_m[variavel].abs().mean())

        pos_m = df_m[df_m[variavel] > 0] # Filtrando os valores positivos
        neg_m = df_m[df_m[variavel] < 0] # Filtrando os valores negativos

        if pos_m.shape[0] == 1:
            media_pos.append(abs(pos_m[variavel].values[0]))
        elif pos_m.shape[0] == 0:
            media_pos.append(0)
        else:
            media_pos.append(pos_m[variavel].abs().mean())

        if neg_m.shape[0] == 1:
            media_neg.append(abs(neg_m[variavel].values[0]))
        elif neg_m.shape[0] == 0:
            media_neg.append(0)
        else:
            media_neg.append(neg_m[variavel].abs().mean())

    return media, media_pos, media_neg

def escala_grafico_erro(variavel):
    """Função que padroniza os nomes das variáveis.

    Args:
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais

    Returns:
        var (string): retorna a variável padronizada
    """

    if variavel == "CANCELAMENTO TOTAL" or variavel == "VENDAS ACUMULADAS TOTAIS":
        var = variavel.lower().capitalize()
    elif variavel == "VGV LÍQUIDO ACUMULADO":
        var = variavel[:3] + variavel[3:].lower()
    elif variavel == "FLUXO DE CAIXA FII":
        var = variavel[:14].capitalize() + variavel[14:]
    elif variavel == "(=) CUSTOS OPERACIONAIS":
        s = variavel.lower()
        var = s[4].upper() + s[5:]
    else:
        s = variavel.lower()
        var = s[4].upper() + s[5:]
    
    return var

def grafico_erro(df_tipologia, variavel, value):
    """Função que gera a figura do gráfico para cada variável de um empreendimento.

    Args:
        df_tipologia (DataFrame): dataframe filtrado pela tipologia
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        value (int): input de meses (default: 5)

    Returns:
        fig (object): retorna a figura do gráfico
    """

    df_filtrado = df_tipologia[['INDICADORES', variavel]]

    maximo_meses = len(list(df_filtrado.INDICADORES.unique()))

    if value > maximo_meses:
        meses = [f'M{i}' for i in range(1, maximo_meses+1)]

    elif value <= maximo_meses:
        meses = [f'M{i}' for i in range(1, value+1)]

    m, p, n = erro_medio_positivo_negativo(df_filtrado, variavel, meses)

    df = pd.DataFrame(None)

    df['Indicadores'] = meses
    df['Erro inferior'] = np.array(n) * 100
    df['Erro médio'] = np.array(m) * 100
    df['Erro superior'] = np.array(p) * 100

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=df['Indicadores'], 
        y=df['Erro superior'].round(3),
        name='Erro otimista',
        marker={'color':'#14B5BD', 'size':8, 'symbol': 'x'},
        showlegend=True
    ))

    fig.add_trace(go.Scatter(
        x=df['Indicadores'], 
        y=df['Erro médio'].round(3), 
        name='Erro médio',
        marker={'color':'#242e48', 'size':8, 'symbol': 'circle'},
        showlegend=True
    ))

    fig.add_trace(go.Scatter(
        x=df['Indicadores'], 
        y=df['Erro inferior'].round(3), 
        name='Erro pessimista',
        marker={'color':'#E54D8D', 'size':8, 'symbol': 'square'},
        showlegend=True 
    ))
    
    fig.update_yaxes(zeroline=False, zerolinewidth=1, showgrid=True, gridwidth=0.5, gridcolor='#F7F7F7', mirror=True, rangemode="tozero", color="#242e48", ticksuffix="%")
    fig.update_xaxes(zeroline=False, zerolinewidth=2, showgrid=False, gridwidth=0.5, gridcolor='#F7F7F7', mirror=True, rangemode="tozero", color="#242e48")

    var = escala_grafico_erro(variavel)

    if variavel == "CANCELAMENTO TOTAL ACUMULADO" or variavel == "VENDAS ACUMULADAS TOTAIS":
        fig.update_layout(
            title={"text":var, "font": {"color": "#242e48", "family": "Inter"}},
            font_family="Inter",
            title_font_family="Inter",
            xaxis_title="Meses",
            autosize=True,
            paper_bgcolor="#DCE3EC", #BAC8DB
            plot_bgcolor="#DCE3EC",
            legend={"font": {'color': "#242e48", "family": "Inter"}},
        )
    else:
        fig.update_layout(
            title={"text":var, "font": {"color": "#242e48", "family": "Inter"}},
            font_family="Inter",
            title_font_family="Inter",
            xaxis_title="Meses",
            autosize=True,
            paper_bgcolor="#DCE3EC", #BAC8DB
            plot_bgcolor="#DCE3EC",
            legend={"font": {'color': "#242e48", "family": "Inter"}},
        )
    
    return fig

def opcao_grafico_erro(tipologia, variavel, value):
    """Função chamada em app.py e que retorna a figura do gráfico.

    Args:
        tipologia (string): tipologia, por exemplo: Loteamento aberto
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        value (int): intput de meses (default: 5)

    Returns:
        fig (object): retorna a figura do gráfico
    """

    if tipologia != 'Todos':
        df_tipologia = dfs[dfs['NATUREZA'] == tipologia]
    else:
        df_tipologia = dfs.copy()

    return grafico_erro(df_tipologia, variavel, value)

def alerta_tipologia(tipologia, value):
    """Função que gera o alerta caso a tipologia não tenha a quantidade de meses informado.

    Args:
        tipologia (string): tipologia, por exemplo: Loteamento aberto
        value (int): intput de meses (default: 5)

    Returns:
        alerta (bool): retorna um valor booleano indicando o alerta
    """

    if tipologia != 'Todos':
        df_tipologia = dfs[dfs['NATUREZA'] == tipologia]
    else:
        df_tipologia = dfs.copy()

    maximo_meses = len(list(df_tipologia.INDICADORES.unique()))

    if value > maximo_meses:
        alerta = True
    elif value <= maximo_meses:
        alerta = False

    return alerta
