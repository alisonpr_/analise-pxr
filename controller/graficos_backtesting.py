import plotly.graph_objects as go
import pandas as pd
from model.read_data import *
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import summary_table
import warnings
warnings.filterwarnings("ignore")

pd.set_option('display.max_columns', None)

def empilha_erros(empreendimento):
    """Função que empilha os erros calculados de um empreendimento.

    Args:
        empreendimento (string): nome do empreendimento

    Returns:
        dfs_erro_por_empreendimento (DataFrame): retorna um novo dataframe com os dados empilhados
    """

    lista_pxr = list(erro_pxr[empreendimento]['previstos'].keys())

    dfs_erro_por_empreendimento = erro_pxr[empreendimento]['previstos'][lista_pxr[0]] # Atribui o primeiro pxr

    for i in range(1, len(lista_pxr)):
        dfs_erro_por_empreendimento = pd.concat([dfs_erro_por_empreendimento, erro_pxr[empreendimento]['previstos'][lista_pxr[i]]], axis=0)

    dfs_erro_por_empreendimento = dfs_erro_por_empreendimento.reset_index(drop=True)

    return dfs_erro_por_empreendimento

def media_ms(df):
    """Função que faz as médias para M1, M2,..., M_n.

    Args:
        df (DataFrame): DataFrame com os dados empilhados

    Returns:
        df_aux (DataFrame): retorna um novo DataFrame com as médias calculadas
    """

    # Tranformas os indicadores M1, M2,..., M_n para 1, 2,..., 3
    df.INDICADORES = df.INDICADORES.apply(lambda x: int(x[1:]))

    # Cria o novo dataframe e instancia os indicadores de meses
    df_aux = pd.DataFrame()
    df_aux['INDICADORES'] = list(df.INDICADORES.unique())

    # Percorre cada coluna fazendo as médias
    for var in n_cols:
        media = []

        # Faz o filtro por cada M_i
        for m in list(df.INDICADORES.unique()):
            dfm = df[df.INDICADORES == m]

            if dfm.shape[0] == 1:
                media.append(round(abs(dfm[var].values[0]), 4))
            else:
                media.append(dfm[var].abs().mean().round(4))

        df_aux[var] = media

    return df_aux

def replace_meses(preditos):
    """Função que retorna os indicadores para M1, M2,..., M_n.

    Args:
        preditos (DataFrame): DataFrame da regressão linear

    Returns:
        preditos: retorna o mesmo dataframe, mas com os indicadores trocados
    """

    dict_replace = {}
    
    for i in range(preditos.shape[0]):
        dict_replace[i+1] = f"M{i+1}"

    preditos['INDICADORES'].replace(dict_replace, inplace=True)

    return preditos

def regressao(df, var, confianca):
    """Função que gera um dataframe com a regressão linear para cada variável de um empreendimento.

    Args:
        df (DataFrame): dataframe com as médias de cada M1, M2,..., M_n 
        var (string): variável analisada, por exemplo: Vendas Acumuladas Totais
        confianca (float): intervalo de confiança indicado

    Returns:
        preds (Dataframe): retorna o dataframe com os erros preditos pela regressão
    """

    num_meses = df.shape[0]
    preditos = pd.DataFrame()
    preditos['INDICADORES'] = [i for i in range(1, num_meses+1)]

    X = df['INDICADORES'].values.reshape(-1,1) 
    y = df[var].abs().values

    X = sm.add_constant(X)
    res = sm.OLS(y, X).fit()
    
    st, data, ss2 = summary_table(res, alpha=1-confianca)
    
    preds = pd.DataFrame.from_records(data, columns=[s.replace('\n', ' ') for s in ss2]).sort_values(by='Obs')

    preds.rename(columns={"Dep Var Population": var}, inplace=True)

    return preds

def grafico_backtest(variavel, primeiro_previsto, realizado_aux, preds, confianca):
    """Função que retorna a figura do gráfico para cada variável de um empreendimento.

    Args:
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        primeiro_previsto (DataFrame): dataframe do primeiro PxR de um empreendimento
        realizado_aux (DataFrame): dataframe do último PxR de um empreendimento
        preds (DataFrame): dataframe dos erros preditos pela regressão linear
        confianca (float): intervalo de confiança indicado pelo usuário

    Returns:
        fig (object): retorna uma figura do gráfico
    """
    
    df = pd.DataFrame(None)
    
    # Sem intervalo de confiança
    if confianca == True:
        df['Data'] = primeiro_previsto['DATA']
        df['Indicadores'] = primeiro_previsto['INDICADORES']
        df['Erro'] = abs(preds['Predicted Value'].round(4))
        erro = df['Erro']
        erro[0] = 0.0010
        df['Erro'] = erro
        df[variavel + ' previsto'] = abs(primeiro_previsto[variavel])
        df['Lim_Inf'] = abs(primeiro_previsto[variavel]) - (abs(primeiro_previsto[variavel]) * df['Erro'])
        df['Lim_Sup'] = abs(primeiro_previsto[variavel]) + (abs(primeiro_previsto[variavel]) * df['Erro'])
        df['Lim_Inf'] = df['Lim_Inf']
        df['Lim_Sup'] = df['Lim_Sup']
    
    # Com intervalo de confiança
    else:
        df['Data'] = primeiro_previsto['DATA']
        df['Indicadores'] = primeiro_previsto['INDICADORES']
        df['Erro'] = abs(preds[variavel].round(4))
        df[variavel + ' previsto'] = abs(primeiro_previsto[variavel])
        df['Lim_Inf'] = abs(primeiro_previsto[variavel]) - (abs(primeiro_previsto[variavel]) * preds['Mean ci 95% low'].abs())
        df['Lim_Sup'] = abs(primeiro_previsto[variavel]) + (abs(primeiro_previsto[variavel]) * preds['Mean ci 95% upp'].abs())

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=realizado_aux['DATA'], 
        y=abs(realizado_aux[variavel]),
        mode='lines+markers',
        name="Realizado", 
        marker={'color':'#242e48', 'size':7, 'symbol': 'circle'}
    ))
    fig.add_trace(go.Scatter(
        x=primeiro_previsto['DATA'], 
        y=abs(primeiro_previsto[variavel]),
        mode='lines+markers',
        name="Previsto", 
        marker={'color':'#E54D8D', 'size':7, 'symbol': 'diamond'}
    ))
    fig.add_trace(go.Scatter(
        x=df['Data'], 
        y=df['Lim_Inf'], 
        name='Banda Inferior',
        mode='lines',
        marker={'color':"#444"},
        line={'width':0},
        showlegend=False
    ))
    fig.add_trace(go.Scatter(
        x=df['Data'], 
        y=df['Lim_Sup'],
        name='Banda Superior', 
        marker= {'color':"#444"},
        line={'width':0},
        mode='lines',
        fillcolor='rgba(60, 78, 120, 0.3)',
        fill='tonexty',
        showlegend=False
    ))

    fig.update_yaxes(showgrid=True, zeroline=False, rangemode="tozero", color="#242e48")
    fig.update_xaxes(showgrid=False, zeroline=False, zerolinewidth=2, color="#242e48")

    if variavel == "CANCELAMENTO TOTAL" or variavel == "VENDAS ACUMULADAS TOTAIS":
        titulo_eixo_y = "Quantidade"
        var = variavel.lower().capitalize()
    elif variavel == "VGV LÍQUIDO ACUMULADO":
        titulo_eixo_y = "R$"
        var = variavel[:3] + variavel[3:].lower()
    elif variavel == "FLUXO DE CAIXA FII":
        titulo_eixo_y = "R$"
        var = variavel[:14].capitalize() + variavel[14:]
    else:
        titulo_eixo_y = "R$"
        s = variavel.lower()
        var = s[4].upper() + s[5:]

    if variavel == "CANCELAMENTO TOTAL" or variavel == "VENDAS ACUMULADAS TOTAIS":
        fig.update_layout(
            title={"text":var, "font": {"color": "#242e48", "family": "Inter"}},
            font_family="Inter",
            title_font_family="Inter",
            xaxis_title="Meses",
            yaxis_title=titulo_eixo_y,
            autosize=True,
            paper_bgcolor="#DCE3EC",
            plot_bgcolor="#DCE3EC",
            legend={"font": {'color': "#242e48", "family": "Inter"}},
        )
    else:
        fig.update_layout(
            title={"text":var, "font": {"color": "#242e48", "family": "Inter"}},
            font_family="Inter",
            title_font_family="Inter",
            xaxis_title="Meses",
            yaxis_title=titulo_eixo_y,
            autosize=True,
            paper_bgcolor="#DCE3EC", #BAC8DB
            plot_bgcolor="#DCE3EC",
            legend={"font": {'color': "#242e48", "family": "Inter"}},
        )

    return fig

def opcao_empreendimento(empreendimento, variavel, confianca=True):
    """Função que é chamada em app.py e retorna o gráfico de cada variável. 

    Args:
        empreendimento (string): nome do empreendimento
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        confianca (bool, float): intervalo de confiança (default: True)

    Returns:
        fig (object): retorna a figura do gráfico gerado
    """

    # Chama a função que empilha os erro calculados
    ativo_erro = empilha_erros(empreendimento)

    # Chama a função que faz a média de cada M_i
    df_norm = media_ms(ativo_erro)

    # Pega uma lista com a chaves do PxR previstos
    lista_pxr = list(erro_pxr_aux[empreendimento]['previstos'].keys())
    
    # Pega o primeiro PxR de um empreendimento
    primeiro_previsto = dict_tratado_aux[empreendimento]['previstos'][lista_pxr[0]]
    
    # Pega o último PxR de um empreedimento
    realizado = pxr_realizados[empreendimento] # Armazena o realizado do empreendimento onde a variável DATA  vai de janeiro até dezembro

    # Pega a chave do último PxR
    nome_pxr_realizado = list(empr[empreendimento].keys())[-1]
    
    # Pega a chave do primeiro PxR
    nome_pxr_previsto = list(empr[empreendimento].keys())[0]
    
    # Extrai ano e mês do primeiro e último PxR
    anop = int(nome_pxr_previsto.split()[0][:2])
    mesp = int(nome_pxr_previsto.split()[0][2:])
    anor = int(nome_pxr_realizado.split()[0][:2])
    mesr = int(nome_pxr_realizado.split()[0][2:])

    # Cria as datas de filtro com base na data de referência do primeiro e último PxR
    if mesp > 9:
        data = f"20{anop}-{mesp}-01"
    else:
        data = f"20{anop}-0{mesp}-01"

    if mesr > 9:
        data2 = f"20{anor}-{mesr}-01"
    else:
        data2 = f"20{anor}-0{mesr}-01"

    # Faz o filtro do primeiro e último PxR
    realizado_aux = realizado[(realizado['DATA'] >= data) & (realizado['DATA'] < data2)]
    primeiro_previsto = primeiro_previsto[primeiro_previsto['DATA'] < data2]

    # Chama a função que faz a regressão linear dos erros
    preds = regressao(df_norm, variavel, confianca)

    # Chama a função que gera a figura do gráfico
    fig = grafico_backtest(variavel, primeiro_previsto, realizado_aux, preds, confianca)

    return fig
