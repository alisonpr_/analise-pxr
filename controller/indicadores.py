from model.read_data import query_dfs, erro_pxr
import pandas as pd
import numpy as np
import math

dfs = query_dfs()

def separa_erros(df_filtrado, variavel, meses):
    """Fução que separa os dados.

    Args:
        df_filtrado (DataFrame): dataframe filtrado pela tipologia
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        meses (list): lista dos M_i

    Returns:
        ind_geral (list): retorna uma lista com as médias gerais
    """

    ind_geral = []

    for m in meses:
        df_m = df_filtrado[df_filtrado['INDICADORES'] == m]
       
        if df_m.shape[0] == 1:
            ind_geral.append(abs(df_m[variavel].values[0]))
        elif df_m.shape[0] == 0:
            ind_geral.append(0)
        else:
            ind_geral.append(df_m[variavel].abs().mean())

    return ind_geral

def indicador_de_erros(df, tipo_indicador, size):
    """Função que calcula os indicadores de erros.

    Args:
        df (DataFrame): dataframe do indicador
        tipo_indicador (string): indica se é o indicador geral, pessimista ou otimista
        size (int): quantidade de M_i

    Returns:
        indicador (float): retorna o valor do indicador calculado
    """

    pesos = []
    k = size
    dados = list(df[tipo_indicador].values)

    indicador = 0
    for i in range(k):
        pesos.append(k-i)
        indicador += dados[i] * (k-i)

    indicador = indicador / sum(pesos)

    return indicador

def gera_indicador(df_tipologia, variavel, meses):
    """Função que gera o indicador geral para o card das páginas.

    Args:
        df_tipologia (DataFrame): dataframe filtrado pela tipologia
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        meses (list): lista com os M1, M2,..., M_n

    Returns:
        indicador_erro_geral (float): retorna o valor do indicador geral
        df (DataFrame): retorna o dataframe
    """

    df_filtrado = df_tipologia[['INDICADORES', variavel]]

    # Chama a função que separa os dados pessimista, otimista e geral
    ind_geral = separa_erros(df_filtrado, variavel, meses)

    df = pd.DataFrame(None)

    df['Indicadores'] = meses
    df['Indicadores geral'] = np.array(ind_geral) * 100

    size = len(meses)

    # Chama a função que calcula os indicadores de erros
    indicador_erro_geral = indicador_de_erros(df, 'Indicadores geral', size)

    return indicador_erro_geral, df

def desvio_padrao(df):
    """Função que calcula o desvio padrão ponderado.

    Args:
        df (DataFrame): dataframe filtrado pela tipologia

    Returns:
        std (float): retorna o valor do desvio padrão
    """
    s = 0
    x = list(df['Indicadores geral'].values)
    k = len(x)
    pesos = []

    for i in range(k):
        pesos.append(k-i)
        s += (k-i) * ((x[i] - np.mean(x))**2)

    return math.sqrt(s/sum(pesos))

def filtra_tipologia(tipologia, dfs):
    """Função que retorna um dataframe filtrado pela tipologia.

    Args:
        tipologia (string): nome da tipologia, por exemplo: Loteamento aberto
        dfs (DataFrame): dataframe com todos os erros de todos empreendimentos empilhados

    Returns:
        df (DataFrame): retorna o dataframe filtrado pela tipologia
    """

    df = dfs[dfs['NATUREZA'] == tipologia]
    
    return df

def indicadores_tipologia(tipologia, variavel, value):
    """Função que retorna os indicadores de erros e desvio padrão por tipologia.

    Args:
        tipologia (string): nome da tipologia, por exemplo: Loteamento aberto
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador
        std (float): retorna o valor do desvio padrão
    """

    if tipologia == "Todos":
        df = dfs.copy()
        maximo = len(list(df['INDICADORES'].unique()))
    else:
        df = filtra_tipologia(tipologia, dfs)
        maximo = len(list(df['INDICADORES'].unique()))

    if value <= maximo:
        meses = [f"M{i}" for i in range(1, value+1)]
    elif value > maximo:
        meses = [f"M{i}" for i in range(1, maximo+1)]

    ind, df_m = gera_indicador(df, variavel, meses)
    std = desvio_padrao(df_m)

    return str(round(ind, 2)), str(round(std, 2))

def empilha_erro_asset(empreendimento, value):
    """Função que empilha os erros de um empreendimento.

    Args:
        empreendimento (string): nome do empreendimento
        value (int): input de meses (default: 5)

    Returns:
        ativo (DataFrame): retorna o dataframe empilhado
        meses (list): retorna uma lista com os M1, M2,..., M_n
    """

    lista_pxr = list(erro_pxr[empreendimento]['previstos'].keys())

    ativo = erro_pxr[empreendimento]['previstos'][lista_pxr[0]] # Atribui o primeiro pxr

    for i in range(1, len(lista_pxr)):
        ativo = pd.concat([ativo, erro_pxr[empreendimento]['previstos'][lista_pxr[i]]], axis=0)

    ativo = ativo.reset_index(drop=True)

    maximo_meses = len(list(ativo['INDICADORES'].unique()))

    if value > maximo_meses:
        meses = [f'M{i}' for i in range(1, maximo_meses+1)]

    elif value <= maximo_meses:
        meses = [f'M{i}' for i in range(1, value+1)]
    
    return ativo, meses

def indicadores_ativos(empreendimento, variavel, value):
    """Função que retorna os indicadores de erros e o desvio padrão por empreendimento.

    Args:
        empreendimento (string): nome do empreendimento
        variavel (string): nome da variável, por exemplo: Vendas Acumuladas Totais
        value (int): input de meses (default: 5)

    Returns:
        ind (float): retorna o valor do indicador
        std (float): retorna o valor do desvio padrão
    """

    df, meses = empilha_erro_asset(empreendimento, value)

    ind, df_m = gera_indicador(df, variavel, meses)
    std = desvio_padrao(df_m)

    return str(round(ind, 2)), str(round(std, 2))
