from index import dash_app
from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from utils.constantes import *

# Div do SIDEBAR
sidebar = html.Div(
    children=[
        html.H1("Cassy", className="cassy", style={'color': colors['text'], 'size': 13, 'font-family': 'Fantasy', 'align': 'center', 'margin-left': '35px'}),
        
        html.Hr(className="hrSidebar"),
    
        # Páginas dos gráficos
        dbc.Nav(
            children=[
                dbc.NavLink("Página inicial", href=home_page_location, active="exact", style={'font-family': 'Inter'}),
                dbc.NavLink("Análise de erro por tipologia", href=error_page_location, active="exact", style={'font-family': 'Inter'}),
                dbc.NavLink("Análise de erro por ativo", href=error_page_assets_location, active="exact", style={'font-family': 'Inter'}),
                dbc.NavLink("Backtesting", href=backtesting_location, active="exact", style={'font-family': 'Inter'}),
                dbc.NavLink("Realizado com bandas de erro", href=error_bands_location, active="exact", style={'font-family': 'Inter'}),
            ],
            vertical=True,
            pills=True,
        ),

        html.Hr(className="hrSidebar"),

        html.Img(id='logo-gp', src=dash_app.get_asset_url("logo_GP_negativo.png"), style=IMAGEM_SIDEBAR)
    ],
    style=SIDEBAR_STYLE,
)
