from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from utils.constantes import *

var_cols = ['Vendas Acumuladas Totais', 'Cancelamento Total', 'VGV Líquido Acumulado', 'VGV Líquido Vendido', 'Receita Bruta Operacional',
    'Custos Operacionais', 'Despesas Operacionais', 'Fluxo de Caixa Operacional Ajustado']

lista_tip_filtro = ['Todos', 'Loteamento Aberto', 'Incorporação Vertical', 'Incorporação Horizontal Fechado',
    'Multipropriedade/ Fracionado', 'Condomínio Fechado', 'Parque Aquático']

# Tabela de indicadores de erros por empreendimentos
ativos_table_row = dbc.Row(
    [
        # Coluna com o campo de filtrar tipologia e o input de meses
        dbc.Col([
            dbc.Row([
                # Coluna com o filtro da tipologia
                dbc.Col(
                    html.Div(
                        children=[
                            html.H6("Filtro por tipologia", id="tooltip-filtro-ativos", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                            
                            dbc.Tooltip("Filtre os ativos através da tipologia.", target="tooltip-filtro-ativos", style={'font-family': 'Inter'}),
                            
                            dcc.Dropdown(
                                id='tipologia3',
                                options=[{'label': i, 'value': i} for i in sorted(lista_tip_filtro, reverse=True)],
                                value='Todos',
                                clearable = False,
                                style={'color': '#242e48', 'font-family': 'Inter', 'width': '310px'}
                            )
                        ]
                    ), md=5
                ),

                # Coluna com o campo de input de meses
                dbc.Col([
                    html.Div(id='div-table-error-indicator-asset',
                    children=[
                        #html.H5("Tabela de indicadores", id="tooltip-tipologia2", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),

                        html.H6("Meses", id="tooltip-table1", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                        dbc.Tooltip("Input de meses sobre a tabela de indicadores de erros. No mínimo 4 e no máximo 13.", target="tooltip-table1", style={'font-family': 'Inter'}),

                        dcc.Input(
                            id='input-meses-table-asset',
                            className="form-control",
                            type='number',
                            value=5,
                            min=4,
                            max=13,
                            step=1,
                            style={'color': '#242e48', 'font-family': 'Inter', 'width': '80px'}
                        )
                    ])
                ], md=3)
            ])
        ]),

        # Coluna com o botão info
        dbc.Col(
            dbc.Row([
                dbc.Col(
                    children=[
                        html.Div(
                            children=[
                                # Botão info
                                dbc.Button(
                                    "Info",
                                    id="popover-table-asset",                                          # id do botão
                                    outline=True,                                                         # Adiciona contorno ao botão para melhorar o estilo
                                    color="sucess",                                                       # Alterando cor do botão
                                    style={'font-family': 'Inter', "box-shadow": "4px 4px 4px #242e48"},  # Definindo a fonte do texto
                                    size="md",                                                            # Alterando o tamanho do botão para pequeno
                                    className="btn btn-outline-primary"                                   # Alterando o tipo do botão com uma classe do bootstrap
                                ),

                                # Caixinha com as informações
                                dbc.Popover(
                                    children=[
                                        dbc.PopoverHeader("Informações adicionais", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),

                                        dbc.PopoverBody(
                                            dcc.Markdown(
                                                '''
                                                __Tabela de Indicadores__: os indicadores de erros (otimista, geral e pessimista), correspondem a uma ponderação que atribui um peso 
                                                maior aos erros mais próximos da previsão. Não importando se o erro é maior no meses mais próximos ou mais 
                                                distantes. A ideia dos indicadores gira em torno de valorizar mais os acertos no curto prazo. O Input de meses
                                                irá estabelecer o peso calculado. Segue a fórmula do indicador:
                                    
                                                $$
                                                    I_e = \\frac{\sum_{i=0}^{k-1} e_i * (k-i)}{\sum_{i=0}^{k-1}(k-i)},
                                                $$

                                                onde $e_i$ representa o erro do mês $i$ e $k$ representa o input de meses.
                                                ''',
                                                mathjax=True,
                                                style={'textAlign': 'justify', 'font-family': 'Inter'}
                                            ),
                                            style= {'overflow': 'auto', 'max-height': '500px', 'font-family': 'Inter', 'color': '#242e48'} 
                                        )
                                    ],

                                    target="popover-table-asset",   # Setando o botão de target
                                    placement="bottom-end",         # Definindo a posição que o popover deve abrir em relação ao botão
                                    is_open=False,                  # Definindo que o estado inicial do popover é fechado
                                    trigger="legacy"                # Definindo que abra ao clicar
                                ),
                            ]
                        )
                    ]
                )
            ]),
            md=4,
            width=2,
            align="right",
            style = {'paddingLeft': '28.5%', 'paddingRight': '1%', 'paddingBottom': '1%', 'paddingTop': '1%'}, # adicionando um espaçamento lateral
        )
    ],
    align="start",
)

# Estrutura da tabela de indicadores de erros por ativos
content_table = dbc.Row(
    [
        dbc.Col(
            html.Div(id='table_error'), md=12,
        ),
    ],
    align="start",
)

lista_tipologia = ['Todos', 'Loteamento aberto', 'Condomínio de chácaras', 'Built to suit', 'Incorporação vertical', 'Lazer', 'Incorporação horizontal fechado',
                   'Múltiplas tipologias', 'Multipropriedade', 'Condomínio fechado']

# Tabela de indicadores de erros por tipologia
tipologia_table_row = dbc.Row(
    [
        # Input de meses da tabela
        dbc.Col([
            dbc.Row([
                dbc.Col([
                    html.Div(id='div-table-error-indicator-typology',
                    children=[
                        html.H6("Meses", id="tooltip-table2", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                        dbc.Tooltip("Input de meses sobre a tabela de indicadores de erros. No mínimo 4 e no máximo 13.", target="tooltip-table2", style={'font-family': 'Inter'}),

                        dcc.Input(
                            id='input-meses-table-typology',
                            className="form-control",
                            type='number',
                            value=5,
                            min=4,
                            max=13,
                            step=1,
                            style={'color': '#242e48', 'font-family': 'Inter', 'width': '80px'}
                        )
                    ])
                ], md=2)
            ])
        ], md=6),

        # Botão info da tabela
        dbc.Col(
            dbc.Row([
                dbc.Col(
                    children=[
                        html.Div(
                            children=[
                                dbc.Button(
                                    "Info",
                                    id="popover-table-typology",                                          # id do botão
                                    outline=True,                                                         # Adiciona contorno ao botão para melhorar o estilo
                                    color="sucess",                                                       # Alterando cor do botão
                                    style={'font-family': 'Inter', "box-shadow": "4px 4px 4px #242e48"},  # Definindo a fonte do texto
                                    size="md",                                                            # Alterando o tamanho do botão para pequeno
                                    className="btn btn-outline-primary"                                   # Alterando o tipo do botão com uma classe do bootstrap
                                ),

                                dbc.Popover(
                                    children=[
                                        dbc.PopoverHeader("Informações adicionais", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),

                                        dbc.PopoverBody(
                                            dcc.Markdown(
                                                '''
                                                __Tabela de Indicadores__: os indicadores de erros (otimista, geral e pessimista), correspondem a uma ponderação que atribui um peso 
                                                maior aos erros mais próximos da previsão. Não importando se o erro é maior no meses mais próximos ou mais 
                                                distantes. A ideia dos indicadores gira em torno de valorizar mais os acertos no curto prazo. O Input de meses
                                                irá estabelecer o peso calculado. Segue a fórmula do indicador:

                                                $$
                                                    I_e = \\frac{\sum_{i=0}^{k-1} e_i * (k-i)}{\sum_{i=0}^{k-1}(k-i)},
                                                $$
                                                onde $e_i$ representa o erro do mês $i$ e $k$ representa o input de meses.
                                                ''',
                                                mathjax=True,
                                                style={'textAlign': 'justify', 'font-family': 'Inter'}
                                            ),
                                            style= {'overflow': 'auto', 'max-height': '500px', 'font-family': 'Inter', 'color': '#242e48'} 
                                        )
                                    ],

                                    target="popover-table-typology",     # Setando o botão de target
                                    placement="bottom-end",     # Definindo a posição que o popover deve abrir em relação ao botão
                                    is_open=False,              # Definindo que o estado inicial do popover é fechado
                                    trigger="legacy"            # Definindo que abra ao clicar
                                ),
                            ]
                        )
                    ]
                )
            ]),
            md=6,
            width=2,
            align="right",
            style = {'paddingLeft': '45%', 'paddingRight': '1%', 'paddingBottom': '1%', 'paddingTop': '1%'}, # adicionando um espaçamento lateral
        )
    ],
    align="start",
)

# Estrutura da tabela de indicadores de erros por ativos
content_table_tipologia = dbc.Row(
    [
        dbc.Col(
            html.Div(id='table_error_typology'), md=12,
        ),
    ],
    align="start",
)
