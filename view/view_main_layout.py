from dash import dcc
from dash import html
from view.view_sidebar import sidebar
from utils.constantes import *

content = html.Div(id="page-content", style=CONTENT_STYLE)

############################################### DIV PRINCIPAL DA PÁGINA ###############################################

layout = html.Div(
    children=[dcc.Location(id="url"), sidebar, content],

    style={'backgroundColor': "#FFFFFF"}
)