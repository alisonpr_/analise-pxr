from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from utils.constantes import *
from view.view_typology import *

# Primeira linha dos cards de indicadores e desvio padrão
card_first_row_erro = dbc.Row(
    [
        # Coluna de Vendas Acumuladas Totais
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-vat-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="left",
                        style={'paddingLeft': '0.7%'}, # adicionando um espaçamento lateral
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-vat-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '0.8%', 'paddingRight': '1.6%'}, # adicionando um espaçamento lateral
                    )
                ], className='pt-1')
            ]
        ),

        # Coluna de Cancelamento Total
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-ct-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="center",
                        style={'paddingLeft': '1.8%'}, # adicionando um espaçamento lateral
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-ct-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '1.2%', 'paddingRight': '0.4%'}, # adicionando um espaçamento lateral
                    )
                ], className='pt-1')
            ]
        )
    ], className='p-2 align-items-stretch' # Ajusta o espaçado entre os cards e os gráficos
)

# Segunda linha dos cards de indicadores e desvio padrão
card_second_row_erro = dbc.Row(
    [
        # Coluna de VGV Líquido Acumulado
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-vgvla-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="left",
                        style={'paddingLeft': '0.8%'}, # adicionando um espaçamento lateral
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-vgvla-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '0.8%', 'paddingRight': '1.6%'}, # adicionando um espaçamento lateral
                    )
                ], className='pt-1')
            ]
        ),

        # Coluna de Receita Bruta Operacional
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-rbo-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="center",
                        style={'paddingLeft': '1.8%'}, # adicionando um espaçamento lateral
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-rbo-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '1.2%', 'paddingRight': '0.4%'}, # adicionando um espaçamento lateral
                    )
                ], className='pt-1')
            ]
        )
    ], className='p-2 align-items-stretch' # Ajusta o espaçado entre os cards e os gráficos
)

# Terceira linha dos cards de indicadores e desvio padrão
card_third_row_erro = dbc.Row(
    [
        # Coluna de Custos Operacionais
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-co-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="left",
                        style={'paddingLeft': '0.8%'}, # adicionando um espaçamento lateral
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-co-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '0.8%', 'paddingRight': '1.6%'}, # adicionando um espaçamento lateral
                    )
                ], className='pt-1')
            ]
        ),

        # Coluna de Despesas Operacionais
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-do-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="center",
                        style={'paddingLeft': '1.8%'}, # adicionando um espaçamento lateral
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-do-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '1.2%', 'paddingRight': '0.4%'}, # adicionando um espaçamento lateral
                    )
                ], className='pt-1')
            ]
        )
    ], className='p-2 align-items-stretch' # Ajusta o espaçado entre os cards e os gráficos
)

# Quarta linha dos cards de indicadores e desvio padrão
card_fourth_row_erro = dbc.Row(
    [
        # Coluna do Fluxo de Caixa Operacional Ajustado
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-fcoa-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="left",
                        style={'paddingLeft': '0.8%'}, # adicionando um espaçamento lateral
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-fcoa-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '0.8%', 'paddingRight': '1.6%'}, # adicionando um espaçamento lateral
                    )
                ], className='pt-1')
            ]
        ),

        # Coluna do Fluxo de Caixa FII
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-fcfii-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="center",
                        style={'paddingLeft': '1.8%'}, # adicionando um espaçamento lateral
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-fcfii-typololy", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '1.2%', 'paddingRight': '0.4%'}, # adicionando um espaçamento lateral
                    )
                ], className='pt-1')
            ]
        )
    ], className='p-2 align-items-stretch' # Ajusta o espaçado entre os cards e os gráficos
)