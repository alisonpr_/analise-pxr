from dash import dcc
from dash import html
import dash_bootstrap_components as dbc

# Primeira linha das estrutras dos gráficos
content_first_row_bandas = dbc.Row(
    [
        # Estrutura onde fica o gráfico de Vendas Acumuladas Totais
        dbc.Col(
           html.Div(dcc.Graph(id='graph_1_bandas'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
           md=6,
        ),

        # Estrutura onde fica o gráfico de Cancelamento Total
        dbc.Col(
            html.Div(dcc.Graph(id='graph_2_bandas'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6,
        )
    ],
    align="start",
)

# Segunda linha das estrutras dos gráficos
content_second_row_bandas = dbc.Row(
    [
        # Estrutura onde fica o gráfico de VGV Líquido Acumulado
        dbc.Col(
            html.Div(dcc.Graph(id='graph_3_bandas'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        ),

        # Estrutura onde fica o gráfico de Receita Bruta Operacional
        dbc.Col(
            html.Div(dcc.Graph(id='graph_4_bandas'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        )
    ],
    align="start",
)

# Terceira linha das estrutras dos gráficos
content_third_row_bandas = dbc.Row(
    [
        # Estrutura onde fica o gráfico de Custos Operacionais
        dbc.Col(
            html.Div(dcc.Graph(id='graph_5_bandas'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        ),

        # Estrutura onde fica o gráfico de Despesas Operacionais
        dbc.Col(
            html.Div(dcc.Graph(id='graph_6_bandas'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        )
    ],
    align="start",
)

# Quarta linha das estrutras dos gráficos
content_fourth_row_bandas = dbc.Row(
    [
        # Estrutura onde fica o gráfico de Fluxo de Caixa Operacional Ajustado
        dbc.Col(
            html.Div(dcc.Graph(id='graph_7_bandas'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        ),

        # Estrutura onde fica o gráfico de Fluxo de Caixa FII
        dbc.Col(
            html.Div(dcc.Graph(id='graph_8_bandas'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        )
    ],
    align="start",
)