from turtle import st
from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from utils.constantes import *
from view.view_typology import *

# Primeira linha das estrutras dos gráficos
content_first_row_erro = dbc.Row(
    [
        # Estrutura onde fica o gráfico de Vendas Acumuladas Totais
        dbc.Col(
           html.Div(dcc.Graph(id='graph_1_erro'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
           style={"border-radius": 15},
           md=6,
        ),

        # Estrutura onde fica o gráfico de Cancelamento Total
        dbc.Col(
            html.Div(dcc.Graph(id='graph_2_erro'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6,
        )
    ],
    align="start",
)

# Segunda linha das estrutras dos gráficos
content_second_row_erro = dbc.Row(
    [
        # Estrutura onde fica o gráfico de VGV Líquido Acumulado
        dbc.Col(
            html.Div(dcc.Graph(id='graph_3_erro'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6,
        ),

        # Estrutura onde fica o gráfico de Receita Bruta Operacional
        dbc.Col(
            html.Div(dcc.Graph(id='graph_4_erro'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6,
        )
    ],
    align="start",
)

# Terceira linha das estrutras dos gráficos
content_third_row_erro = dbc.Row(
    [
        # Estrutura onde fica o gráfico de Custos Operacionais
        dbc.Col(
            html.Div(dcc.Graph(id='graph_5_erro'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6,
        ),

        # Estrutura onde fica o gráfico de Despesas Operacionais
        dbc.Col(
            html.Div(dcc.Graph(id='graph_6_erro'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6,
        )
    ],
    align="start",
)

# Quarta linha das estrutras dos gráficos
content_fourth_row_erro = dbc.Row(
    [
        # Estrutura onde fica o gráfico de Fluxo de Caixa Operacional Ajustado
        dbc.Col(
            html.Div(dcc.Graph(id='graph_7_erro'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6,
        ),

        # Estrutura onde fica o gráfico de Fluxo de Caixa FII
        dbc.Col(
            html.Div(dcc.Graph(id='graph_8_erro'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6,
        )
    ],
    align="start",
)
