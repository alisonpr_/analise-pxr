# -*- coding: utf-8 -*-
import pandas as pd
from dash import dcc
from dash import html
from dash import dash_table
import dash_bootstrap_components as dbc
from utils.constantes import *
import plotly.graph_objects as go
from model.read_data import empr, lista_de_empreendimentos

tip = ['Todos', 'Loteamento Aberto', 'Condomínio Fechado', 'Incorporação Horizontal Fechado',
    'Incorporação Vertical', 'Múltiplas Tipologias', 'Multipropriedade/ Fracionado']

tip.remove('Todos')

# Texto introdutório do projeto
first_row_hp = dbc.Row(
    [
        dbc.Col(
           html.Div(
               dcc.Markdown(
                   '''A análise preditiva é importante para analisar o desempenho da empresa em prever custos, receitas e evolução de vendas dos empreendimentos. \
                    Em especial, avaliar o quanto os dados previstos se divergem dos realizados. Além disso, a análise preditiva irá auxiliar a análise prescritiva, \
                    também conhecida como análise de recomendação, que é quando unimos as análises descritiva e preditiva com o conhecimento de negócios. \
                    Essas três frentes unidas são capazes de recomendar ações tomadas de maneira automática, otimizar os processos e escalar a tomada de decisões. \
                    Assim é possível economizar tempo e alcançar melhores resultados. ''',
                    style={'font-family': 'Inter', 'color': '#242e48', 'textAlign': 'justify'}
                ),
           ), md=12,
        ),
    ],
    align="justify",
)

# Dicionário com as colunas em meses
df = {
    'Empreendimentos': [],
    '01/21': [],
    '02/21': [],
    '03/21': [],
    '04/21': [],
    '05/21': [],
    '06/21': [],
    '07/21': [],
    '08/21': [],
    '09/21': [],
    '10/21': [],
    '11/21': [],
    '12/21': [],
    '01/22': [],
    '02/22': [],
    'Total': []
}

lista = list(empr.keys())

# Verificando e contando os PxR de cada mês
count = 0
for emp in lista:
    lista_pxr = list(empr[emp].keys())
    df['Empreendimentos'].append(emp)
    
    lista21, lista22 = [], []
    for p in lista_pxr:
        mes = int(p.split(' ')[0][2:])
        ano = int(p.split(' ')[0][:2])

        if ano == 21:
            lista21.append(mes)
        else:
            lista22.append(mes)
        
        count += 1

    contador = 0
    for a in [21, 22]:
        if a == 21:
            for k in range(1, 13):
                if k in lista21:
                    if k >= 10:
                        chave = f"{k}/21"
                    else:
                        chave = f"0{k}/21"

                    df[chave].append('✓')
                    contador += 1
                else:
                    if k >= 10:
                        chave = f"{k}/21"
                    else:
                        chave = f"0{k}/21"

                    df[chave].append('')
        else:
            for k in range(1, 3):
                if k in lista22:
                    if k >= 10:
                        chave = f"{k}/22"
                    else:
                        chave = f"0{k}/22"

                    df[chave].append('✓')
                    contador += 1
                else:
                    if k >= 10:
                        chave = f"{k}/22"
                    else:
                        chave = f"0{k}/22"

                    df[chave].append('')
    
    df['Total'].append(contador)
    
df = pd.DataFrame(df).sort_values(by=['Empreendimentos'], ascending=True)

data = {
    'x': [],
    'y': [],
}

for i in range(df.shape[0]):
    lista = list(df.loc[i])
    emp = lista[0]
    data['x'].append(emp)

    k = 0
    for j in range(1, len(lista)):
        if lista[j] != '':
            k += 1
    data['y'].append(round((k/12)*100, 2))

data = pd.DataFrame(data)
data.sort_values(by=['x'], ascending=False, inplace=True)

def figura_home_page():
    """Função que retorna a figura do gráfico de empreendimentos.

    Returns:
        fig (object): retorna a figura do gráfico
    """

    fig = go.Figure(
    
    go.Bar(
        x=data['y'],
        y=data['x'],
        orientation='h',
        marker={'color': '#39425a', 'line': {'color': '#39425a', 'width': 1}},
        #text=data['y'],
        textposition='auto'
    )
)
    fig.update_traces(textposition='outside')
    fig.update_yaxes(showgrid=False, zeroline=False, rangemode="tozero", color="#242e48")
    fig.update_xaxes(showgrid=False, zeroline=False, zerolinewidth=2, color="#242e48", ticksuffix="%")
    fig.update_layout(
        font_family="Inter",
        title_font_family="Inter",
        paper_bgcolor="#FFFFFF", #9fb2cd
        plot_bgcolor="#FFFFFF",
        margin={'l': 5, 'r': 5, 't': 5, 'b': 5},
        xaxis_range=[0,100],
        width=500,
        height=800
    )

    return fig

# Tabela da home page
table_pxr = dash_table.DataTable(
    data = df.to_dict('records'),
    
    columns=[{"name": i, "id": i} for i in list(df.columns)],
    
    style_as_list_view=False, # Habilita as linhas verticais da tabela
    
    style_table={'maxHeight': '1000px', 'maxWidth': '936px', 'minWidth': '100%', 'overflowY': 'auto', "box-shadow": "6px 6px 6px #C6CCD4"}, # Ajusta a altura da tabela e a barra de rolagem

    fixed_rows={'headers': True}, # Fixa o cabeçalho

    fixed_columns={'headers': True, 'data': 1}, # Fixa a coluna 1

    page_action='none',
    
    style_cell={
        'textAlign': 'center',
        'font-family': 'Inter',
        'height': 'auto',
        'minWidth': '60.6px', 'width': '60.6px', 'maxWidth': '550px'
    },

    style_cell_conditional=[
        {
            'if': {'column_id': 'Empreendimentos'},
            'textAlign': 'left',
            'width': '300px'
        }
    ],

    style_data={
        'color': 'black',
        'backgroundColor': '#FFFFFF', #54698A
        'height': 'auto',
        'border': '1px solid white',
        'font-family': 'Inter'
    },

    style_data_conditional=[
        {
            'if': {'row_index': 'odd'},
            'backgroundColor': '#DCE3EC'
        },
        {
            'if': {'column_id': 'Total'},
            'color': 'black',
            'fontWeight': 'bold',
            'font-family': 'Inter',
        }
    ],

    style_header={
        'backgroundColor': '#DCE3EC',
        'color': 'black',
        'fontWeight': 'bold',
        'font-family': 'Inter',
        'border': '1px solid white',
    },

    fill_width=False,
)

# Linha da estrutura da tabela e os cards
second_row_hp = dbc.Row(
    [
        # Coluna da tabela
        dbc.Col(
           html.Div(
               children=[
                    html.H5("Relação de PxR's por empreendimentos", style=TEXT_SUBTITLE),
                    table_pxr
                ]
            ),
            width=10
        ),

        # Coluna dos cards da home page
        dbc.Col(
            children=[
                dbc.Row(
                    children=[
                        html.Div(
                            children=[
                                # Card do número de empreendimentos
                                dbc.Card(
                                    children=[
                                        dbc.CardHeader("Nº de empreendimentos", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'font-size': 13, 'textAlign': 'center'}),
                                        dbc.CardBody(
                                            children=[
                                                html.H2(f"{len(lista_de_empreendimentos)}", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                            ], className='text-center'
                                        )
                                    ],
                                    style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4", "margin": "0.8%"},
                                )
                            ]
                        ),

                        html.Div(
                            children=[
                                # Card do número de tipologias
                                dbc.Card(
                                    children=[
                                        dbc.CardHeader("Nº de tipologias", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'font-size': 13, 'textAlign': 'center'}),
                                        dbc.CardBody(
                                            children=[
                                                html.H2(f"{len(tip)}", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                            ], className='text-center'
                                        )
                                    ],
                                    style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                                )
                            ]
                        ),

                        html.Div(
                            children=[
                                # Card com o total de PxR
                                dbc.Card(
                                    children=[
                                        dbc.CardHeader("Total de PxRs", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'font-size': 13, 'textAlign': 'center'}),
                                        dbc.CardBody(
                                            children=[
                                                html.H2(f"{df.Total.sum()}", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                            ], className='text-center'
                                        )
                                    ],
                                    style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4", "margin": "0.8%"}
                                )
                            ]
                        )
                    ],
                    align="center"
                )
            ],
            width=2
        )
    ],
    align="center",
)