from dash import html
import dash_bootstrap_components as dbc
from utils.constantes import *
from view.view_dropdown_confidance import *

# Primeira linha dos cards de indicadores e desvio padrão
card_first_row_erro_asset = dbc.Row(
    [
        # Coluna de Vendas Acumuladas Totais
        dbc.Col(
            children=[
                dbc.Row([
                    # Indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-vat-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="left",
                        style={'paddingLeft': '0.8%'}, # adicionando um espaçamento lateral
                    ),

                    # Desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-vat-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '0.8%', 'paddingRight': '1.6%'},
                    )
                ], className='pt-1')
            ]
        ),

        # Coluna de Cancelamento Total
        dbc.Col(
            children=[
                dbc.Row([
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-ct-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="center",
                        style={'paddingLeft': '1.8%'},
                    ),

                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-ct-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '1.2%', 'paddingRight': '0.4%'},
                    )
                ], className='pt-1')
            ]
        )
    ], className='p-2 align-items-stretch' # Ajusta o espaçado entre os cards e os gráficos
)

# Segunda linha dos cards de indicadores e desvio padrão
card_second_row_erro_asset = dbc.Row(
    [
        # Coluna de VGV Líquido Acumulado
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicadores de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-vgvla-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="left",
                        style={'paddingLeft': '0.8%'},
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-vgvla-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '0.8%', 'paddingRight': '1.6%'},
                    )
                ], className='pt-1')
            ]
        ),

        # Coluna de Receita Brutal Operacional
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-rbo-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="center",
                        style={'paddingLeft': '1.8%'},
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-rbo-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '1.2%', 'paddingRight': '0.4%'},
                    )
                ], className='pt-1')
            ]
        )
    ], className='p-2 align-items-stretch' # Ajusta o espaçado entre os cards e os gráficos
)

# Terceira linha dos cards de indicadores e desvio padrão
card_third_row_erro_asset = dbc.Row(
    [
        # Coluna de Custos Operacionais
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-co-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="left",
                        style={'paddingLeft': '0.8%'},
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-co-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '0.8%', 'paddingRight': '1.6%'},
                    )
                ], className='pt-1')
            ]
        ),

        # Coluna de Custos Operacionais
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-do-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="center",
                        style={'paddingLeft': '1.8%'},
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-do-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '1.2%', 'paddingRight': '0.4%'},
                    )
                ], className='pt-1')
            ]
        )
    ], className='p-2 align-items-stretch' # Ajusta o espaçado entre os cards e os gráficos
)

# Quarta linha dos cards de indicadores e desvio padrão
card_fourth_row_erro_asset = dbc.Row(
    [
        # Coluna de Fluxo de Caixa Operacional Ajustado
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-fcoa-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="left",
                        style={'paddingLeft': '0.8%'},
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-fcoa-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '0.8%', 'paddingRight': '1.6%'},
                    )
                ], className='pt-1')
            ]
        ),

        # Coluna de Fluxo de Caixa FII
        dbc.Col(
            children=[
                dbc.Row([
                    # Coluna do indicador de erros
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Indicador de erro", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-ind-error-fcfii-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="center",
                        style={'paddingLeft': '1.8%'},
                    ),

                    # Coluna do desvio padrão
                    dbc.Col(
                        children=[
                            dbc.Card(
                                children=[
                                    dbc.CardHeader("Desvio Padrão", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter', 'textAlign': 'center'}),
                                    dbc.CardBody(
                                        children=[
                                            html.H2(id="card-std-error-fcfii-asset", style={'color': '#242e48', 'font-family': 'Inter', 'textAlign': 'center'})
                                        ], className='text-center'
                                    )
                                ],
                                style={"background-color": "#DCE3EC", "box-shadow": "6px 6px 6px #C6CCD4"}
                            )
                        ],
                        align="right",
                        style={'paddingLeft': '1.2%', 'paddingRight': '0.4%'},
                    )
                ], className='pt-1')
            ]
        )
    ], className='p-2 align-items-stretch' # Ajusta o espaçado entre os cards e os gráficos
)