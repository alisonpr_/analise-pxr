from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from utils.constantes import *

lista_tipologia = ['Todos', 'Loteamento Aberto', 'Parque Aquático', 'Incorporação Vertical', 'Incorporação Horizontal Fechado',
    'Multipropriedade/ Fracionado', 'Condomínio Fechado']

# Dropdown para escolha da tipologia
tipologia_row = dbc.Row(
    [
        # Coluna com o campo de escolha de tipologia e input de meses
        dbc.Col([
            dbc.Row(
                children=[
                    # Campo com escolha da tipologia
                    dbc.Col(
                        html.Div(
                            children=[
                                html.H6("Tipologias", id="tooltip-tipologia", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                                
                                dbc.Tooltip("Os gráficos são representados por todas tipologias, como também pode ser analisado por tipologias específicas!", target="tooltip-tipologia", style={'font-family': 'Inter'}),
                                
                                dcc.Dropdown(
                                    id='tipologia',
                                    options=[{'label': i, 'value': i} for i in lista_tipologia],
                                    value='Todos',
                                    clearable = False,
                                    style={'color': '#242e48', 'font-family': 'Inter', 'width': '300px'}
                                )
                            ]
                        ), md=4,
                    ),

                    # Campo com o input de meses
                    dbc.Col(
                        children=[
                            html.H6("Meses", id="tooltip-realizado2", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                            dbc.Tooltip("Input de meses. No mínimo 4 e no máximo 13.", target="tooltip-realizado2", style={'font-family': 'Inter'}),

                            dcc.Input(
                                id='input-meses',
                                className="form-control",
                                type='number',
                                value=5,
                                min=4,
                                max=13,
                                step=1,
                                style={'color': '#242e48', 'font-family': 'Inter', 'width': '80px'}
                            )
                        ]
                    )
                ]
            )
        ]),

        # Coluna com o botão info
        dbc.Col(
            html.Div(
                children=[
                    # Botão info
                    dbc.Button(
                        "Info",
                        id="popover-tipologia",             # id do botão
                        outline=True,                       # Adiciona contorno ao botão para melhorar o estilo
                        color="#242e48",                    # Alterando cor do botão
                        style={'font-family': 'Inter', "box-shadow": "4px 4px 4px #242e48"},     # Definindo a fonte do texto
                        size="md",                          # Alterando o tamanho do botão para pequeno
                        className="btn btn-outline-primary" # Alterando o tipo do botão com uma classe do bootstrap
                    ),

                    # Caixinha com as informações
                    dbc.Popover(
                        children=[
                            dbc.PopoverHeader("Informações adicionais", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),

                            dbc.PopoverBody(
                                dcc.Markdown(
                                    '''
                                    __Análise Gráfica__: esta análise consiste em calcular o erro entre o previsto e o realizado mês a mês. O erro pessimista representa o erro negativo,
                                    quando o realizado superou o que foi projeto. O erro otimista representa o erro positivo, quando o realizado não alcançou o que foi projeto. Por fim,
                                    o erro médio corresponde aos erros otimista e pessimista em absoluto.

                                    __Indicador de erro__: o indicador de erro é uma ponderação que atribui um peso maior aos erros mais próximos da previsão.
                                    Não importando se o erro é maior no meses mais próximos ou mais distantes. A ideia dos indicadores gira em torno de valorizar mais os acertos no curto prazo.
                                    Segue a fórmula do indicador:
                                    
                                    $$
                                        I_e = \\frac{\sum_{i=0}^{k-1} e_i * (k-i)}{\sum_{i=0}^{k-1}(k-i)},
                                    $$

                                    onde $e_i$ representa o erro do mês $i$ e $k$ representa o input de meses.

                                    __Desvio Padrão__: o desvio padrão também é um indicador, com objetivo de comparar ao indicador de erro cálculado.

                                    __Obs__.: é válido ressaltar que ambos indicadores são apresentados graficamente somente com base no erro médio.

                                    __Formatação numérica__: a formatação numérica segue o padrão de ponto, por exemplo, 1545.19% é o mesmo que 1.545,19%. Esse padrão é necessário devido a ferramenta do dashboard.
                                    ''',
                                    mathjax=True,
                                    style={'textAlign': 'justify', 'font-family': 'Inter'}
                                ),
                                style= {'overflow': 'auto', 'max-height': '500px', 'font-family': 'Inter', 'color': '#242e48'} 
                            )
                        ],

                        target="popover-tipologia", # Setando o botão de target
                        placement="bottom-end",     # Definindo a posição que o popover deve abrir em relação ao botão
                        is_open=False,              # Definindo que o estado inicial do popover é fechado
                        trigger="legacy"            # Definindo que abra ao clicar
                    ),
                ]
            ),
            width=2,
            align="right",
            style = {'paddingLeft': '12%', 'paddingRight': '1%', 'paddingBottom': '1%', 'paddingTop': '1%'}, # adicionando um espaçamento lateral
            )
    ],
    justify="between"
)