from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from utils.constantes import *
from model.read_data import lista_emp

# Dropdown do gráfico do realizado com bandas de erro
dropdown_confianca_row = dbc.Row(
    [
        # Coluna onde tem o campo para escolher o empreendimento e o input de meses
        dbc.Col(
            children=[
                dbc.Row(
                    children=[
                        # Escolha do empreendimento
                        dbc.Col(
                            children=[
                                html.H6("Empreendimentos", id="tooltip-realizado-ativos", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                                
                                dbc.Tooltip("Os gráficos são representados por ativos!", target="tooltip-realizado-ativos", style={'font-family': 'Inter'}),
                                
                                dcc.Dropdown(
                                    id='empreendimento',
                                    options=[{'label': i, 'value': i} for i in lista_emp],
                                    value=lista_emp[0],
                                    clearable = False,
                                    style={'color': '#242e48', 'font-family': 'Inter', 'width': '280px'}
                                )
                            ]
                        ),

                        # Input de meses
                        dbc.Col(
                            children=[
                                html.H6("Meses", id="tooltip-realizado-ativos2", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                                dbc.Tooltip("Input de meses.", target="tooltip-realizado-ativos2", style={'font-family': 'Inter'}),

                                dcc.Input(
                                    id='input-data',
                                    className="form-control",
                                    value=10,
                                    type='number',
                                    min=3,
                                    max=360,
                                    step=1,
                                    style={'color': '#242e48', 'font-family': 'Inter', 'width': '80px'}
                                )
                            ]
                        )
                    ]
                )
            ],
            md=4
        ),

        # Coluna onde fica o botão info
        dbc.Col([
            dbc.Row(
                children=[
                    dbc.Col(
                        html.Div(
                            children=[
                                # Botão
                                dbc.Button(
                                    "Info",
                                    id="popover-realizadoBandas",      # id do botão
                                    outline=True,                      # Adiciona contorno ao botão para melhorar o estilo
                                    color="sucess",                    # Alterando cor do botão
                                    style={'font-family': 'Inter', "box-shadow": "4px 4px 4px #242e48"},    # Definindo a fonte do texto
                                    size="md",                         # Alterando o tamanho do botão para pequeno
                                    className="btn btn-outline-primary"        # Alterando o tipo do botão com uma classe do bootstrap
                                ),

                                # Caixinha com o texto de informações
                                dbc.Popover(
                                    children=[
                                        dbc.PopoverHeader("Informações adicionais", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),

                                        dbc.PopoverBody(
                                            dcc.Markdown(
                                                '''
                                                __Input de Meses__: esse input indica o quanto se deseja visualizar, por exemplo, ao informar 5, significa que
                                                o gráfico irá mostrar 5 meses após a data do realizado. Sendo o mínimo 3 meses, o máximo ainda não está estabelecido.
                                                
                                                __Análise Gráfica__: a análise do realizado mostra os dados acumulados desde a data inicial até a data de referência do PxR
                                                (linha azul é o realizado até a data de referência, a linha rosa é o que foi projetado). A penumbra do gráfico chama-se _bandas de erro_,
                                                ela indica uma margem aceitável de erro, com isso pode-se analisar uma margem para o Projetado, mostrando onde ele poderá estar conforme os meses e,
                                                quanto mais longe analisar, maior será a faixa de erro possível.

                                                __Formatação numérica__: a formatação numérica segue o padrão de ponto, por exemplo, 1545.19% é o mesmo que 1.545,19%. Esse padrão é necessário devido a ferramenta do dashboard.
                                                ''',
                                                style={'textAlign': 'justify', 'font-family': 'Inter'}
                                            ),
                                            style= {'overflow': 'auto', 'max-height': '500px', 'font-family': 'Inter', 'color': '#242e48'} 
                                        )
                                    ],

                                    target="popover-realizadoBandas",   # Setando o botão de target
                                    placement="bottom-end",             # Definindo a posição que o popover deve abrir em relação ao botão
                                    is_open=False,                      # Definindo que o estado inicial do popover é fechado
                                    trigger="legacy"                     # Definindo que abra ao clicar
                                ),
                            ]
                        ),
                        width=2,
                        align="right",
                        style = {'paddingLeft': '92%', 'paddingRight': '1%', 'paddingBottom': '1%', 'paddingTop': '1%'}, # adicionando um espaçamento lateral
                    )
                ]
            )
        ])
    ],
    justify="beetwen",
)

# Dropdown do gráfico de backtesting sem intervalo de confiança
dropdown_confianca_row2SI = dbc.Row(
    [
        # Coluna onde fica o campo para escolher o empreendimento
        dbc.Col(
            html.Div(
                children=[
                    html.H6("Empreendimentos", id="tooltip-backtesting-ativos", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                    
                    dbc.Tooltip("Os gráficos são representados por ativos!", target="tooltip-backtesting-ativos", style={'font-family': 'Inter'}),
                    
                    dcc.Dropdown(
                        id='empreendimento2',
                        options=[{'label': i, 'value': i} for i in lista_emp],
                        value=lista_emp[0],
                        clearable = False,
                        style={'color': '#242e48', 'font-family': 'Inter', 'width': '300px'}
                    )
                ]
            ), md=3
        ),

        # Coluna onde fica o campo do botão info
        dbc.Col(
            html.Div(
                children=[
                    # Botão info
                    dbc.Button(
                        "Info",
                        id="popover-backtesting",          # id do botão
                        outline=True,                      # Adiciona contorno ao botão para melhorar o estilo
                        color="sucess",                    # Alterando cor do botão
                        style={'font-family': 'Inter', "box-shadow": "4px 4px 4px #242e48", "align":"center"},    # Definindo a fonte do texto
                        size="md",                         # Alterando o tamanho do botão para pequeno
                        className="btn btn-outline-primary"        # Alterando o tipo do botão com uma classe do bootstrap
                    ),

                    # Caixinha com as informações
                    dbc.Popover(
                        children=[
                            dbc.PopoverHeader("Informações adicionais", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),

                            dbc.PopoverBody(
                                dcc.Markdown(
                                    '''
                                    __Backtesting S.I__: esta página apresenta as análises de backtesting sem intervalo de confiança. Isso significa que as bandas projetadas são calculadas
                                    através da soma e subtração do previsto com a taxa de erro.
                                    
                                    __Análise Gráfica__: a análise de backtest tem como objetivo testar a confiabilidade do modelo construído, olhando para os dados do passado.
                                    Compreende-se do primeiro PxR (Previsto) e o último PxR (Realizado), ambos do ano vigente. A penumbra do gráfico chama-se _bandas de erro_,
                                    ela indica uma margem aceitável de erro, espera-se que o Realizado esteja dentro desta margem, caso contrário, indica que há um desvio seja pra cima ou para
                                    baixo do que foi Previsto.
                                    
                                    __Formatação numérica__: a formatação numérica segue o padrão de ponto, por exemplo, 1545.19% é o mesmo que 1.545,19%. Esse padrão é necessário devido a ferramenta do dashboard.
                                    ''',
                                    style={'textAlign': 'justify', 'font-family': 'Inter'}
                                ),
                                style= {'overflow': 'auto', 'max-height': '500px', 'font-family': 'Inter', 'color': '#242e48'} 
                            )
                        ],

                        target="popover-backtesting",   # Setando o botão de target
                        placement="bottom-end",         # Definindo a posição que o popover deve abrir em relação ao botão
                        is_open=False,                  # Definindo que o estado inicial do popover é fechado
                        trigger="legacy"                 # Definindo que abra ao clicar
                    ),
                ]
            ),
            width=2,
            align="center",
            style = {'paddingLeft': '70%', 'paddingRight': '1%', 'paddingBottom': '1%', 'paddingTop': '1%'}, # adicionando um espaçamento lateral
        )
    ],
    justify="beetwen",
)

# Dropdown do gráfico de backtesting com intervalo de confiança
dropdown_confianca_row2CI = dbc.Row(
    [
        # Coluna onde fica os campos de escolher o empreendimento e o input do intervalo de confiança
        dbc.Col(
            children=[
                dbc.Row(
                    children=[
                        # Campo para escolha do empreendimento
                        dbc.Col(
                            children=[
                                html.H6("Empreendimentos", id="tooltip-backtesting-ativos2", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                                
                                dbc.Tooltip("Os gráficos são representados por ativos!", target="tooltip-backtesting-ativos2", style={'font-family': 'Inter'}),
                                
                                dcc.Dropdown(
                                    id='empreendimento4',
                                    options=[{'label': i, 'value': i} for i in lista_emp],
                                    value=lista_emp[0],
                                    clearable = False,
                                    style={'color': '#242e48', 'font-family': 'Inter', 'width': '300px'}
                                )
                            ]
                        ),
                        
                        # Campo do input do intervalo de confiança
                        dbc.Col(
                            children=[
                                html.H6("I.C", id="tooltip-backtesting-ativos3", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                                dbc.Tooltip("Intervalo de Confiança.", target="tooltip-backtesting-ativos3", style={'font-family': 'Inter'}),

                                dcc.Input(
                                    id='intervalo-confianca',
                                    className="form-control",
                                    value=0.95,
                                    type='number',
                                    min=0,
                                    max=1,
                                    step=0.05,
                                    style={'color': '#242e48', 'font-family': 'Inter', 'width': '80px'}
                                )
                            ]
                        )
                    ]
                )
            ],
            md=5
        ),

        # Coluna onde fica o botão info
        dbc.Col([
            dbc.Row(
                children=[
                    dbc.Col(
                        children=[
                            html.Div(
                                children=[
                                    # Botão info
                                    dbc.Button(
                                        "Info",
                                        id="popover-backtesting2",          # id do botão
                                        outline=True,                      # Adiciona contorno ao botão para melhorar o estilo
                                        color="sucess",                    # Alterando cor do botão
                                        style={'font-family': 'Inter', "box-shadow": "4px 4px 4px #242e48", "align":"center"},    # Definindo a fonte do texto
                                        size="md",                         # Alterando o tamanho do botão para pequeno
                                        className="btn btn-outline-primary"        # Alterando o tipo do botão com uma classe do bootstrap
                                    ),

                                    # Caixinha com as informações
                                    dbc.Popover(
                                        children=[
                                            dbc.PopoverHeader("Informações adicionais", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),

                                            dbc.PopoverBody(
                                                dcc.Markdown(
                                                    '''
                                                    __Backtesting C.I__: esta página apresenta as análises de backtesting com intervalo de confiança. Isso significa que as bandas projetadas são calculadas
                                                    através do nível de confiança que pode ser informado ao lado. O método que calcula o intervalo de confiança retorna o limite inferior e superior.
                                                    O propósito dessa abordagem é indicar, por exemplo, que há uma confiança de 95% dos erros estarem dentro do intervalo inferior e superior.
                                                    É através do intervalo que calculamos a soma e subtração do previsto para formar as bandas.
                                                    
                                                    __Análise Gráfica__: a análise de backtest tem como objetivo testar a confiabilidade do modelo construído, olhando para os dados do passado.
                                                    Compreende-se do primeiro PxR (Previsto) e o último PxR (Realizado), ambos do ano vigente. A penumbra do gráfico chama-se _bandas de erro_,
                                                    ela indica uma margem aceitável de erro, espera-se que o Realizado esteja dentro desta margem, caso contrário, indica que há um desvio seja pra cima ou para
                                                    baixo do que foi Previsto.
                                                    
                                                    __Formatação numérica__: a formatação numérica segue o padrão de ponto, por exemplo, 1545.19% é o mesmo que 1.545,19%. Esse padrão é necessário devido a ferramenta do dashboard.
                                                    ''',
                                                    style={'textAlign': 'justify', 'font-family': 'Inter'}
                                                ),
                                                style= {'overflow': 'auto', 'max-height': '500px', 'font-family': 'Inter', 'color': '#242e48'} 
                                            )
                                        ],

                                        target="popover-backtesting2",   # Setando o botão de target
                                        placement="bottom-end",         # Definindo a posição que o popover deve abrir em relação ao botão
                                        is_open=False,                  # Definindo que o estado inicial do popover é fechado
                                        trigger="legacy"                 # Definindo que abra ao clicar
                                    ),
                                ]
                            )
                        ],
                        width=2,
                        align="center",
                        style = {'paddingLeft': '92%', 'paddingRight': '1%', 'paddingBottom': '1%', 'paddingTop': '2%'} # adicionando um espaçamento lateral
                    ),
                ]
            )
        ])
    ],
    justify="beetwen",
)

# Dropdown do gráfico de erros por ativos
dropdown_confianca_row3 = dbc.Row(
    [
        # Coluna onde fica o campo de escolha de empreendimentos e o input de meses
        dbc.Col([
            dbc.Row(
                children=[
                    # Coluna da escolha do empreendimento
                    dbc.Col(
                        html.Div(
                            children=[
                                html.H6("Empreendimentos", id="tooltip-erro-ativos", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                                
                                dbc.Tooltip("Os gráficos são representados por ativos!", target="tooltip-erro-ativos", style={'font-family': 'Inter'}),
                                
                                dcc.Dropdown(
                                    id='empreendimento3',
                                    options=[{'label': i, 'value': i} for i in lista_emp],
                                    value=lista_emp[0],
                                    clearable = False,
                                    style={'color': '#242e48', 'font-family': 'Inter', 'width': '300px'}
                                )
                            ]
                        ), md=5
                    ),

                    # Coluna onde fica o input dos meses
                    dbc.Col(
                        children=[
                            html.H6("Meses", id="tooltip-ativos2", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),
                            dbc.Tooltip("Input de meses. No mínimo 4 e no máximo 13.", target="tooltip-ativos2", style={'font-family': 'Inter'}),

                            dcc.Input(
                                id='input-meses-ativos',
                                className="form-control",
                                type='number',
                                value=5,
                                min=4,
                                max=13,
                                step=1,
                                style={'color': '#242e48', 'font-family': 'Inter', 'width': '80px'}
                            )
                        ], md=3
                    )
                ]
            )
        ], md=8),

        # Coluna do botão info
        dbc.Col(
            dbc.Row([
                dbc.Col(
                    children=[
                        html.Div(
                            children=[
                                # Botão info
                                dbc.Button(
                                    "Info",
                                    id="popover-asset",                                                   # id do botão
                                    outline=True,                                                         # Adiciona contorno ao botão para melhorar o estilo
                                    color="sucess",                                                       # Alterando cor do botão
                                    style={'font-family': 'Inter', "box-shadow": "4px 4px 4px #242e48"},  # Definindo a fonte do texto
                                    size="md",                                                            # Alterando o tamanho do botão para pequeno
                                    className="btn btn-outline-primary"                                   # Alterando o tipo do botão com uma classe do bootstrap
                                ),

                                # Caixinha com as informações
                                dbc.Popover(
                                    children=[
                                        dbc.PopoverHeader("Informações adicionais", style={'color': '#242e48', 'fontWeight': 'bold', 'font-family': 'Inter'}),

                                        dbc.PopoverBody(
                                            dcc.Markdown(
                                                '''
                                                __Análise Gráfica__: esta análise consiste em calcular o erro entre o previsto e o realizado mês a mês. O erro pessimista representa o erro negativo,
                                                quando o realizado superou o que foi projeto. O erro otimista representa o erro positivo, quando o realizado não alcançou o que foi projeto. Por fim,
                                                o erro médio corresponde aos erros otimista e pessimista em absoluto.

                                                __Indicador de erro__: o indicador de erro é uma ponderação que atribui um peso maior aos erros mais próximos da previsão.
                                                Não importando se o erro é maior no meses mais próximos ou mais distantes. A ideia dos indicadores gira em torno de valorizar mais os acertos no curto prazo.
                                                Segue a fórmula do indicador:
                                    
                                                $$
                                                    I_e = \\frac{\sum_{i=0}^{k-1} e_i * (k-i)}{\sum_{i=0}^{k-1}(k-i)},
                                                $$

                                                onde $e_i$ representa o erro do mês $i$ e $k$ representa o input de meses.

                                                __Desvio Padrão__: o desvio padrão também é um indicador, com objetivo de comparar ao indicador de erro cálculado.

                                                __Obs__.: é válido ressaltar que ambos indicadores são apresentados graficamente somente com base no erro médio.
                                                
                                                __Formatação numérica__: a formatação numérica segue o padrão de ponto, por exemplo, 1545.19% é o mesmo que 1.545,19%. Esse padrão é necessário devido a ferramenta do dashboard.
                                                ''',
                                                mathjax=True,
                                                style={'textAlign': 'justify', 'font-family': 'Inter'}
                                            ),
                                            style= {'overflow': 'auto', 'max-height': '500px', 'font-family': 'Inter', 'color': '#242e48'} 
                                        )
                                    ],

                                    target="popover-asset",     # Setando o botão de target
                                    placement="bottom-end",     # Definindo a posição que o popover deve abrir em relação ao botão
                                    is_open=False,              # Definindo que o estado inicial do popover é fechado
                                    trigger="legacy"            # Definindo que abra ao clicar
                                ),
                            ]
                        )
                    ]
                )
            ]),
            md=4,
            width=2,
            align="right",
            style = {'paddingLeft': '28.5%', 'paddingRight': '1%', 'paddingBottom': '1%', 'paddingTop': '1%'}, # adicionando um espaçamento lateral
        )
    ],
    justify="beetwen"
)