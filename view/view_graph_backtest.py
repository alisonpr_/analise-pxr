from dash import dcc
from dash import html
import dash_bootstrap_components as dbc

# Primeira linha onde fica as estruturas dos gráficos
content_first_row = dbc.Row(
    [
        # Estrutura onde fica o gráfico de Vendas Acumuladas Totais
        dbc.Col(
           html.Div(dcc.Graph(id='graph_1'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
           md=6,
        ),

        # Estrutura onde fica o gráfico de Cancelamento Total
        dbc.Col(
            html.Div(dcc.Graph(id='graph_2'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6,
        )
    ],
    align="start",
)

# Segunda linha onde fica as estruturas dos gráficos
content_second_row = dbc.Row(
    [
        # Estrutura onde fica o gráfico de VGV Líquido Operacional
        dbc.Col(
            html.Div(dcc.Graph(id='graph_3'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        ),

        # Estrutura onde fica o gráfico de Receita Bruta Operacional
        dbc.Col(
            html.Div(dcc.Graph(id='graph_4'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        )
    ],
    align="start",
)

# Terceira linha onde fica as estrutura dos gráficos
content_third_row = dbc.Row(
    [
        # Estrutura onde fica o gráfico de Custos Operacionais
        dbc.Col(
            html.Div(dcc.Graph(id='graph_5'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        ),

        # Estrutura onde fica o gráfico de Despesas Operacionais
        dbc.Col(
            html.Div(dcc.Graph(id='graph_6'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        )
    ],
    align="start",
)

# Quarta linha onde fica as estruturas dos gráficos
content_fourth_row = dbc.Row(
    [
        # Estrutura onde fica o gráfico de Fluxo de Caixa Operacional Ajustado
        dbc.Col(
            html.Div(dcc.Graph(id='graph_7'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        ),

        # Estrutura onde fica o gráfico de Fluxo de Caixa FII
        dbc.Col(
            html.Div(dcc.Graph(id='graph_8'), style={"box-shadow": "6px 6px 6px #C6CCD4", "border-radius": 15}),
            md=6
        )
    ],
    align="start",
)
